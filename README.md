# Libft

libft is a project to recode some function from the libc in `C`

each subfolder you see at `/srcs` are considerate as module
each module have is documentation in `__doc__`

## Module

- [conv](__doc__/conv/conv.md) module for convertion operation
- [input](__doc__/input/input.md) module for input operation
- [list](__doc__/list/list.md) module for basic linked list operation
- [mem](__doc__/mem/mem.md) module for byte array operation
- [num](__doc__/num/num.md) module for int operation
- [output](__doc__/output/output.md) module for output operation
- [printf](__doc__/printf/printf.md) module for `printf`
- [string](__doc__/string/string.md) module for string operation
- [type](__doc__/type/type.md) module for type operation
