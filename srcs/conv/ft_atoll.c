/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoll.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/06 10:53:47 by fbenneto          #+#    #+#             */
/*   Updated: 2019/11/25 11:59:14 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/conv.h"
#include "libft/type.h"
#include <stdbool.h>

long long	ft_atoll(const char *s)
{
	bool				sign;
	unsigned long long	res;

	while (ft_isspace(*s))
		s++;
	while ((*s == '-' || *s == '+') && !ft_isdigit(s[1]))
		s++;
	sign = false;
	if (*s == '-' || *s == '+')
	{
		if (*s == '-')
			sign = true;
		s++;
	}
	res = 0;
	while (ft_isdigit(*s))
	{
		res = res * 10 + *s - '0';
		s++;
	}
	if (sign)
		return (-res);
	return (res);
}
