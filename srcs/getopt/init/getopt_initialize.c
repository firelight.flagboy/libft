/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   getopt_initialize.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <f.benneto@student.42.fr>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/20 13:03:18 by fbenneto          #+#    #+#             */
/*   Updated: 2019/11/20 20:29:56 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/getopt/init.h"

const char	*ft_getopt_initialize(const char *optstring, t_getopt_data *data)
{
	if (data->optind == 0)
		data->optind = 1;
	data->first_nonopt = data->optind;
	data->last_nonopt = data->optind;
	data->next = 0;
	data->is_initialized = 1;
	if (optstring[0] == '-')
	{
		data->ordering = RETURN_IN_ORDER;
		optstring++;
	}
	else if (optstring[0] == '+')
	{
		data->ordering = REQUIRE_ORDER;
		optstring++;
	}
	else
		data->ordering = PERMUTE;
	data->err_opt = optstring[0] == ':' ? ':' : '?';
	return (optstring);
}
