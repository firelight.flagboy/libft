/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   getopt_long_only.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/20 11:31:43 by fbenneto          #+#    #+#             */
/*   Updated: 2019/11/25 15:57:01 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/getopt.h"
#include "libft/getopt/internal.h"

int	ft_getopt_long_only(
	t_arg *arg, const char *shortopts,
	const t_option *longopts, int *indexptr)
{
	t_getopt_option opts;

	opts.longopts = longopts;
	opts.optstring = shortopts;
	opts.longind = indexptr;
	opts.long_only = 1;
	return (ft_getopt_internal(arg, &opts));
}
