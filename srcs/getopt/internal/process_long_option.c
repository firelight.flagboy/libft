/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   process_long_option.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/20 15:44:08 by fbenneto          #+#    #+#             */
/*   Updated: 2019/11/25 11:58:49 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/getopt/internal.h"
#include "libft/getopt/utils.h"
#include "libft/string.h"
#include "libft/printf.h"

#define EXTRA_ARG "%s: option '%s%s' doesn't allow an argument\n"
#define MISS_ARG "%s: option '%s%s' requires an argument\n"

static int			process_long_option_not_found(
	t_getopt_data *d, const char *prefix)
{
	if (d->long_only || d->argv[d->optind][1] == '-'
		|| ft_strchr(d->optstring, *d->next) == NULL)
	{
		if (d->print_error)
		{
			ft_fprintf(stderr, "%s: unreconized option '%s%s'\n",
				d->argv[0], prefix, d->next);
		}
		d->next = 0;
		d->optind++;
		d->optopt = 0;
		return ('?');
	}
	return (-1);
}

static int			bad_arg(
	const t_option *option, t_getopt_data *d,
	const char *fmt, const char *prefix)
{
	if (d->print_error)
	{
		ft_fprintf(stderr, fmt, d->argv[0], prefix, option->name);
	}
	d->optopt = option->val;
	return (d->err_opt);
}

static inline int	process_flag(const t_option *option)
{
	*option->flag = option->val;
	return (0);
}

static int			process_long_option_next(
	const t_option *option, t_getopt_data *d, const char *prefix)
{
	char	*namemed;

	namemed = d->next;
	d->optind++;
	d->next = NULL;
	if (*namemed)
	{
		if (option->has_arg)
			d->optarg = namemed + 1;
		else
			return (bad_arg(option, d, EXTRA_ARG, prefix));
	}
	else if (option->has_arg)
	{
		if (d->optind < d->argc)
			d->optarg = d->argv[d->optind++];
		else
			return (bad_arg(option, d, MISS_ARG, prefix));
	}
	if (d->longind)
		*d->longind = option - d->longopts;
	if (option->flag)
		return (process_flag(option));
	return (option->val);
}

int					process_long_option(t_getopt_data *d, const char *prefix)
{
	const t_option	*option;
	size_t			len;

	len = ft_strcspn(d->next, "=");
	option = find_long_option(d, len);
	if (option == NULL)
		return (process_long_option_not_found(d, prefix));
	d->next += len;
	return (process_long_option_next(option, d, prefix));
}
