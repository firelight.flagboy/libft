/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   next_short.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/22 19:42:24 by fbenneto          #+#    #+#             */
/*   Updated: 2019/11/25 12:37:10 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/getopt/internal.h"
#include "libft/printf.h"
#include "libft/string.h"

int		next_short_option_long(t_getopt_data *d, char c)
{
	if (*d->next != 0)
		d->optarg = d->next;
	else if (d->optind == d->argc)
	{
		if (d->print_error)
			ft_fprintf(stderr, GETOPT_ERR_MISS_ARG, d->argv[0], c);
		d->optopt = c;
		return (d->err_opt);
	}
	else
		d->optarg = d->argv[d->optind];
	d->next = d->optarg;
	d->optarg = NULL;
	return (process_long_option(d, "-W "));
}

void	next_short_optional_arg(t_getopt_data *d)
{
	if (*d->next != 0)
	{
		d->optarg = d->next;
		d->optind++;
	}
	else
		d->optarg = NULL;
	d->next = NULL;
}

void	next_short_require_arg(t_getopt_data *d, char *c)
{
	if (*d->next != 0)
	{
		d->optarg = d->next;
		d->optind++;
	}
	else if (d->optind == d->argc)
	{
		if (d->print_error)
			ft_fprintf(stderr, GETOPT_ERR_MISS_ARG, d->argv[0], *c);
		d->optopt = *c;
		*c = d->err_opt;
	}
	else
		d->optarg = d->argv[d->optind++];
	d->next = NULL;
}

int		next_short_option(const char *optstring, t_getopt_data *d)
{
	char		c;
	const char	*tmp;

	c = *d->next++;
	tmp = ft_strchr(optstring, c);
	if (*d->next == 0)
		++d->optind;
	if (tmp == NULL || c == ':' || c == ';')
	{
		if (d->print_error)
			ft_fprintf(stderr, "%s: invalid option -- '%c'\n", d->argv[0], c);
		d->optopt = c;
		return ('?');
	}
	if (tmp[0] == 'W' && tmp[1] == ';' && d->longopts)
		return (next_short_option_long(d, c));
	if (tmp[1] == ':')
	{
		if (tmp[2] == ':')
			next_short_optional_arg(d);
		else
			next_short_require_arg(d, &c);
	}
	return (c);
}
