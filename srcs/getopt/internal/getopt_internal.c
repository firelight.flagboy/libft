/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   getopt_internal.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/20 12:38:27 by fbenneto          #+#    #+#             */
/*   Updated: 2019/11/25 11:59:00 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/getopt/internal.h"
#include "libft/getopt/global.h"

int	ft_getopt_internal(t_arg *arg, t_getopt_option *options)
{
	static t_getopt_data	data;
	int						result;

	data.optind = g_optind;
	data.opterr = g_opterr;
	data.argc = arg->argc;
	data.argv = arg->argv;
	data.optstring = options->optstring;
	data.longopts = options->longopts;
	data.longind = options->longind;
	data.long_only = options->long_only;
	result = ft_getopt_internal_r(&data);
	g_optind = data.optind;
	g_optarg = data.optarg;
	g_optopt = data.optopt;
	return (result);
}
