/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   getopt_internal_r.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/20 12:38:27 by fbenneto          #+#    #+#             */
/*   Updated: 2019/11/25 12:44:34 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/getopt/utils.h"
#include "libft/getopt/internal.h"
#include "libft/getopt/global.h"
#include "libft/getopt/init.h"
#include "libft/string.h"

void	skip_special_arg(t_getopt_data *d)
{
	d->optind++;
	if (d->first_nonopt != d->last_nonopt && d->last_nonopt != d->optind)
		ft_getopt_exchange((char**)d->argv, d);
	else if (d->first_nonopt == d->last_nonopt)
		d->first_nonopt = d->optind;
	d->last_nonopt = d->argc;
	d->optind = d->argc;
}

int		next_arg_long(t_getopt_data *d)
{
	int code;

	if (d->argv[d->optind][1] == '-')
	{
		d->next = d->argv[d->optind] + 2;
		return (process_long_option(d, "--"));
	}
	if (d->long_only
		&& (d->argv[d->optind][2]
			|| !ft_strchr(d->modified_optstring, d->argv[d->optind][1])))
	{
		d->next = d->argv[d->optind] + 1;
		code = process_long_option(d, "-");
		if (code != -1)
			return (code);
	}
	return (NEXT_ARG_OK);
}

void	next_arg_prepare(t_getopt_data *d)
{
	if (d->last_nonopt > d->optind)
		d->last_nonopt = d->optind;
	if (d->first_nonopt > d->optind)
		d->first_nonopt = d->optind;
	if (d->ordering == PERMUTE)
		ft_getopt_permute(d);
	if (d->optind < d->argc && ft_strcmp(d->argv[d->optind], "--") == 0)
		skip_special_arg(d);
}

int		next_arg(t_getopt_data *d)
{
	int res;

	next_arg_prepare(d);
	res = NEXT_ARG_OK;
	if (d->optind == d->argc)
	{
		if (d->first_nonopt != d->last_nonopt)
			d->optind = d->first_nonopt;
		return (-1);
	}
	if (d->argv[d->optind][0] != '-' || d->argv[d->optind][1] == 0)
	{
		if (d->ordering == REQUIRE_ORDER)
			return (-1);
		d->optarg = d->argv[d->optind++];
		return (1);
	}
	if (d->longopts)
		res = next_arg_long(d);
	if (res != NEXT_ARG_OK)
		return (res);
	d->next = d->argv[d->optind] + 1;
	return (NEXT_ARG_OK);
}

int		ft_getopt_internal_r(t_getopt_data *d)
{
	int res;

	res = NEXT_ARG_OK;
	d->modified_optstring = d->optstring;
	d->print_error = d->opterr;
	if (d->optind == 0 || d->is_initialized == 0)
		d->modified_optstring = ft_getopt_initialize(d->modified_optstring, d);
	else if (d->modified_optstring[0] == '+' || d->modified_optstring[0] == '-')
		d->modified_optstring++;
	if (d->modified_optstring[0] == ':')
		d->print_error = 0;
	if (d->next == 0 || *d->next == 0)
		res = next_arg(d);
	if (res != NEXT_ARG_OK)
		return (res);
	return (next_short_option(d->modified_optstring, d));
}
