/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exchange.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/21 20:08:06 by fbenneto          #+#    #+#             */
/*   Updated: 2019/11/25 12:43:49 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/getopt/utils.h"
#include <sys/types.h>

static inline void	swap(void *p1, void *p2)
{
	size_t s1;
	size_t s2;

	s1 = *(size_t*)p1;
	s2 = *(size_t*)p2;
	s1 = s1 ^ s2;
	s2 = s1 ^ s2;
	s1 = s1 ^ s2;
	*(size_t*)p1 = s1;
	*(size_t*)p2 = s2;
}

static inline void	swap_part(
	char **argv, int *ptop, int *pmiddle, int *pbottom)
{
	int		i;
	int		len;

	i = 0;
	if (*ptop - *pmiddle > *pmiddle - *pbottom)
	{
		len = *pmiddle - *pbottom;
		while (i < len)
		{
			swap(argv + *pbottom + i, argv + *ptop - (*pmiddle - *pbottom) + i);
			i++;
		}
		*ptop = *ptop - len;
	}
	else
	{
		len = *ptop - *pmiddle;
		while (i < len)
		{
			swap(argv + *pbottom + i, argv + *pmiddle + i);
			i++;
		}
		*pbottom = *pbottom + len;
	}
}

void				ft_getopt_exchange(char **argv, t_getopt_data *d)
{
	int		bottom;
	int		middle;
	int		top;

	bottom = d->first_nonopt;
	middle = d->last_nonopt;
	top = d->optind;
	while (top > middle && middle > bottom)
	{
		swap_part(argv, &top, &middle, &bottom);
	}
	d->first_nonopt += d->optind - d->last_nonopt;
	d->last_nonopt = d->optind;
}
