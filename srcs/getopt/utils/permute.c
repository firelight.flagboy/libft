/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   permute.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/25 12:41:35 by fbenneto          #+#    #+#             */
/*   Updated: 2019/11/25 12:41:49 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/getopt/utils.h"

void	ft_getopt_permute(t_getopt_data *d)
{
	int			argc;
	char *const	*argv;

	argc = d->argc;
	argv = d->argv;
	if (d->first_nonopt != d->last_nonopt && d->last_nonopt != d->optind)
		ft_getopt_exchange((char**)d->argv, d);
	else if (d->last_nonopt != d->optind)
		d->first_nonopt = d->optind;
	while (d->optind < argc
		&& (argv[d->optind][0] != '-' || argv[d->optind][1] == 0))
	{
		d->optind++;
	}
	d->last_nonopt = d->optind;
}
