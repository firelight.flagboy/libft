/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   find_option.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/25 11:55:41 by fbenneto          #+#    #+#             */
/*   Updated: 2019/11/25 12:44:07 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/getopt/utils.h"
#include "libft/string.h"
#include <unistd.h>

const t_option	*find_long_option(t_getopt_data *d, size_t option_len)
{
	const t_option *node;

	node = d->longopts;
	while (node->name)
	{
		if (ft_strncmp(node->name, d->next, option_len) == 0)
			return (node);
		node++;
	}
	return (NULL);
}
