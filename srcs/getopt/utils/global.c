/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   global.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/16 18:07:20 by fbenneto          #+#    #+#             */
/*   Updated: 2019/11/20 10:28:01 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/getopt.h"
#include <stdlib.h>

int g_opterr = 1;
int g_optopt = '?';
int g_optind = 1;
char *g_optarg = NULL;
