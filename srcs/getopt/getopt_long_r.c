/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   getopt_long_r.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <f.benneto@student.42.fr>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/20 11:31:43 by fbenneto          #+#    #+#             */
/*   Updated: 2019/11/20 20:49:28 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/getopt.h"
#include "libft/getopt/internal.h"

int	ft_getopt_long_r(t_getopt_data *data)
{
	data->long_only = 0;
	return (ft_getopt_internal_r(data));
}
