/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   getopt.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/16 18:05:43 by fbenneto          #+#    #+#             */
/*   Updated: 2019/11/25 11:59:22 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/getopt.h"
#include "libft/mem.h"
#include "libft/getopt/internal.h"

int	ft_getopt(int argc, char *const *argv, const char *options)
{
	t_arg			arg;
	t_getopt_option	opts;

	ft_bzero(&opts, sizeof(t_getopt_option));
	opts.optstring = options;
	arg.argc = argc;
	arg.argv = argv;
	return (ft_getopt_internal(&arg, &opts));
}
