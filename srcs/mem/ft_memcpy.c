/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/08 13:36:45 by fbenneto          #+#    #+#             */
/*   Updated: 2019/11/14 10:26:36 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/mem.h"

static inline void	big_copy_forward(size_t *dest, const size_t *src, size_t n)
{
	while (n > 0)
	{
		*dest = *src;
		dest++;
		src++;
		n--;
	}
}

void				*ft_memcpy(void *dest, const void *src, size_t n)
{
	size_t			i;
	uint8_t			*sdest;
	const uint8_t	*ssrc;

	i = n / sizeof(size_t);
	big_copy_forward(dest, src, i);
	i *= sizeof(size_t);
	n -= i;
	sdest = dest + i;
	ssrc = src + i;
	while (n > 0)
	{
		*sdest = *ssrc;
		sdest++;
		ssrc++;
		n--;
	}
	return (dest);
}
