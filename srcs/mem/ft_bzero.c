/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_bzero.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/08 12:49:25 by fbenneto          #+#    #+#             */
/*   Updated: 2019/06/11 15:25:41 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/mem.h"

void	ft_bzero(void *s, size_t n)
{
	size_t	*bptr;
	size_t	bsize;
	size_t	i;
	char	*tmp;

	bsize = n / sizeof(size_t);
	i = 0;
	bptr = s;
	tmp = s;
	while (i < bsize)
	{
		bptr[i] = 0;
		i++;
	}
	i *= sizeof(size_t);
	while (i < n)
	{
		tmp[i] = 0;
		i++;
	}
}
