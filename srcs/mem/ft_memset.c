/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memset.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/08 11:19:28 by fbenneto          #+#    #+#             */
/*   Updated: 2019/05/27 11:17:10 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/mem.h"

void	*ft_memset(void *str, int c, size_t n)
{
	char unsigned *tmp;

	tmp = str;
	while (n)
	{
		*tmp++ = c;
		n--;
	}
	return (str);
}
