/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/08 14:18:49 by fbenneto          #+#    #+#             */
/*   Updated: 2019/11/14 10:43:15 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/mem.h"

void	*ft_memmove(void *dest, const void *src, size_t n)
{
	const char unsigned	*s;
	char unsigned		*dst;

	if (n == 0 || dest == src)
		return (dest);
	s = src;
	dst = dest;
	if (dest < (void *)src)
		while (n--)
			*dst++ = *s++;
	else
		while (n--)
			*(dst + n) = *(s + n);
	return (dest);
}
