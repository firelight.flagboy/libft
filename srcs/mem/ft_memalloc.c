/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memalloc.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/09 08:23:47 by fbenneto          #+#    #+#             */
/*   Updated: 2019/05/27 11:17:51 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/mem.h"

void	*ft_memalloc(size_t size)
{
	void *res;

	if (!(res = (void*)malloc(size)))
		return (NULL);
	ft_memset(res, 0, size);
	return (res);
}
