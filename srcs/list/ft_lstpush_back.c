/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstpush_back.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/03 13:48:27 by fbenneto          #+#    #+#             */
/*   Updated: 2019/06/03 14:56:21 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/list.h"

void	ft_lstpush_back(t_list **head_ref, t_list *new)
{
	t_list *last;

	if (*head_ref == NULL)
		*head_ref = new;
	else if ((last = ft_lstlast(*head_ref)))
		last->next = new;
}
