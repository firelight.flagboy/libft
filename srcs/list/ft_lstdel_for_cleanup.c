/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstdel_for_cleanup.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/11 13:06:45 by fbenneto          #+#    #+#             */
/*   Updated: 2019/11/15 15:26:51 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/list.h"

void	ft_lstdel_for_cleanup(t_list **ptr_to_alst)
{
	if (*ptr_to_alst)
	{
		ft_lstdel(ptr_to_alst, ft_lstfree_content);
	}
}
