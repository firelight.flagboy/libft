/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/10 17:48:32 by fbenneto          #+#    #+#             */
/*   Updated: 2019/11/15 15:40:39 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/list.h"

t_list	*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem))
{
	t_list *head;
	t_list *current;

	if (!lst)
		return (NULL);
	head = f(lst);
	if (head == NULL)
		return (NULL);
	current = head;
	lst = lst->next;
	while (lst)
	{
		current->next = f(lst);
		if (!current->next)
			return (NULL);
		current = current->next;
		lst = lst->next;
	}
	return (head);
}
