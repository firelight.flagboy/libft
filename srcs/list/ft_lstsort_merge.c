/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstsort_merge.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/03 12:43:25 by fbenneto          #+#    #+#             */
/*   Updated: 2019/06/11 15:26:05 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/list.h"

static void			ft_lstsort_merge_create_halves(
	t_list *head, t_list **left_ref, t_list **right_ref)
{
	size_t	middle;
	size_t	i;
	t_list	*node;
	t_list	*tmp;

	middle = ft_lstlen(head) / 2 - 1;
	node = head;
	*left_ref = node;
	i = 0;
	while (i < middle)
	{
		node = node->next;
		i++;
	}
	tmp = node;
	node = node->next;
	tmp->next = NULL;
	*right_ref = node;
}

static inline void	ft_lstsort_merge_routine_append(
	t_list **current, t_list **res)
{
	t_list *node;

	node = *current;
	*current = node->next;
	node->next = NULL;
	ft_lstpush_back(res, node);
}

static t_list		*ft_lstsort_merge_join(
	t_list *left, t_list *right, int (*cmp)(t_list *a, t_list *b))
{
	t_list *res;

	if (left == NULL)
		return (right);
	if (right == NULL)
		return (left);
	res = NULL;
	while (left && right)
	{
		if (cmp(left, right))
			ft_lstsort_merge_routine_append(&left, &res);
		else
			ft_lstsort_merge_routine_append(&right, &res);
	}
	while (left)
		ft_lstsort_merge_routine_append(&left, &res);
	while (right)
		ft_lstsort_merge_routine_append(&right, &res);
	return (res);
}

void				ft_lstsort_merge(
	t_list **head_ref, int (*cmp)(t_list *a, t_list *b))
{
	t_list *head;
	t_list *a;
	t_list *b;

	head = *head_ref;
	if (head == NULL || head->next == NULL || cmp == NULL)
		return ;
	ft_lstsort_merge_create_halves(head, &a, &b);
	ft_lstsort_merge(&a, cmp);
	ft_lstsort_merge(&b, cmp);
	*head_ref = ft_lstsort_merge_join(a, b, cmp);
}
