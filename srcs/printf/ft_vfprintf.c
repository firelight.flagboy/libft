/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_vfprintf.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/17 14:20:17 by fbenneto          #+#    #+#             */
/*   Updated: 2019/11/29 15:20:20 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/printf.h"
#include "libft/printf/struct.h"
#include "libft/printf/utils.h"

#ifdef __linux__

int		ft_vfprintf(FILE *stream, const char *format, va_list ap)
{
	va_list node;
	t_buff	*buff;

	if (stream->_fileno < 0 || stream->_fileno > OPEN_MAX || !format)
		return (-1);
	buff = get_buff();
	buff->index = 0;
	buff->res = 0;
	buff->fd = stream->_fileno;
	buff->put = ft_putbuffer;
	va_copy(node, ap);
	if (ft_fill_buffer(format, node) != -1)
		buff->put(buff);
	else
		return (-1);
	va_end(node);
	return (buff->res);
}

#else

int		ft_vfprintf(FILE *stream, const char *format, va_list ap)
{
	va_list node;
	t_buff	*buff;

	if (stream->_file < 0 || stream->_file > OPEN_MAX || !format)
		return (-1);
	buff = get_buff();
	buff->index = 0;
	buff->res = 0;
	buff->fd = stream->_file;
	buff->put = ft_putbuffer;
	va_copy(node, ap);
	if (ft_fill_buffer(format, node) != -1)
		buff->put(buff);
	else
		return (-1);
	va_end(node);
	return (buff->res);
}

#endif
