/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_backward.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/22 15:55:09 by fbenneto          #+#    #+#             */
/*   Updated: 2019/11/15 08:49:45 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/printf/utils.h"

int		ft_fillbackward(t_flags *f, char neg, int len)
{
	int	l;

	if ((neg || ((f->flags & (HI_ADD | HI_ESCAPE)) && !neg)))
		f->buff_size--;
	if (f->buff_size < len)
		return (0);
	l = f->buff_size - len;
	ft_add_nchar_to_buff(' ', l);
	return (l);
}
