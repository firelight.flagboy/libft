/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_dprintf.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/17 12:02:24 by fbenneto          #+#    #+#             */
/*   Updated: 2019/11/29 15:21:18 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/printf.h"

int		ft_dprintf(int fd, const char *format, ...)
{
	va_list	ap;
	int		rt;

	if (!format)
		return (-1);
	va_start(ap, format);
	rt = ft_vdprintf(fd, format, ap);
	va_end(ap);
	return (rt);
}
