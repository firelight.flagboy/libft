/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sprintf.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/06 11:47:47 by fbenneto          #+#    #+#             */
/*   Updated: 2019/11/29 15:20:41 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/printf.h"

int			ft_sprintf(char *s, const char *format, ...)
{
	va_list ap;
	int		rt;

	if (!s || !format)
		return (-1);
	va_start(ap, format);
	rt = ft_vsprintf(s, format, ap);
	va_end(ap);
	return (rt);
}
