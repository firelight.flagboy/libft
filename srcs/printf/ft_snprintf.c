/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_snprintf.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/06 11:47:47 by fbenneto          #+#    #+#             */
/*   Updated: 2019/11/29 15:20:48 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/printf.h"

int			ft_snprintf(char *s, size_t n, const char *format, ...)
{
	va_list ap;
	int		rt;

	if (!s || !format)
		return (-1);
	va_start(ap, format);
	rt = ft_vsnprintf(s, n, format, ap);
	va_end(ap);
	return (rt);
}
