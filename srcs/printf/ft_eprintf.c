/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_eprintf.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/17 12:02:24 by fbenneto          #+#    #+#             */
/*   Updated: 2019/11/29 15:21:05 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "libft/printf.h"

int		ft_eprintf(const char *format, ...)
{
	va_list ap;
	int		rt;

	if (!format)
		return (-1);
	va_start(ap, format);
	rt = ft_vdprintf(STDERR_FILENO, format, ap);
	va_end(ap);
	return (rt);
}
