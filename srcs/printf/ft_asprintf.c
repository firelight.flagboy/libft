/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_asprintf.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/06 11:47:47 by fbenneto          #+#    #+#             */
/*   Updated: 2019/11/29 15:21:13 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/printf.h"

int			ft_asprintf(char **as, const char *format, ...)
{
	va_list ap;
	int		rt;

	if (!as || !format)
		return (-1);
	va_start(ap, format);
	rt = ft_vasprintf(as, format, ap);
	va_end(ap);
	return (rt);
}
