/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_fprintf.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/17 12:02:24 by fbenneto          #+#    #+#             */
/*   Updated: 2019/11/29 15:27:33 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/printf.h"

int		ft_fprintf(FILE *stream, const char *format, ...)
{
	va_list	ap;
	int		rt;

	if (!format)
		return (-1);
	va_start(ap, format);
	rt = ft_vfprintf(stream, format, ap);
	va_end(ap);
	return (rt);
}
