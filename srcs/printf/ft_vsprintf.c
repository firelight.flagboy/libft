/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_vsprintf.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/17 14:17:22 by fbenneto          #+#    #+#             */
/*   Updated: 2019/11/29 15:20:00 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/printf.h"
#include "libft/printf/utils.h"

int			ft_vsprintf(char *s, const char *format, va_list ap)
{
	va_list	node;
	t_buff	*buff;

	buff = get_buff();
	buff->index = 0;
	buff->res = 0;
	buff->s = s;
	buff->put = ft_putbuffer_s;
	va_copy(node, ap);
	if (ft_fill_buffer(format, node) != -1)
		buff->put(buff);
	else
		return (-1);
	va_end(node);
	return (buff->res);
}
