/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_unicode.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/23 11:29:42 by fbenneto          #+#    #+#             */
/*   Updated: 2019/11/15 09:00:36 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/printf/utils.h"

static char	*ft_get_char(int len, wchar_t c, char s[])
{
	if (len == 4)
	{
		s[0] = c / 0x100 + 0xf0;
		s[1] = (c / 0x80) % 0x40 + 0x80;
		s[2] = (c / 0x40) % 0x40 + 0x80;
		s[3] = c % 0x40 + 0x80;
	}
	else if (len == 3)
	{
		s[0] = c / 0x80 + 0xe0;
		s[1] = (c / 0x40) % 0x40 + 0x80;
		s[2] = c % 0x40 + 0x80;
	}
	else if (len == 2)
	{
		s[0] = c / 0x40 + 0xc0;
		s[1] = c % 0x40 + 0x80;
	}
	else
		s[0] = c;
	return (s);
}

int			ft_unicode(wchar_t c)
{
	char	s[5];
	int		l;

	l = printf_len_uni_char(c);
	if (l == -1)
		return (-1);
	if (l == 1)
	{
		ft_add_char_to_buff(c);
		return (1);
	}
	s[l] = 0;
	ft_get_char(l, c, s);
	ft_add_str_to_buff(s);
	return (l);
}
