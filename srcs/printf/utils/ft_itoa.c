/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/22 13:04:33 by fbenneto          #+#    #+#             */
/*   Updated: 2019/11/15 08:51:48 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/num.h"
#include "libft/string.h"
#include "libft/printf/utils.h"

int			printf_itoa(uintmax_t n)
{
	char	s[20];
	int		lennb;

	lennb = ft_len_uint(n, 10);
	s[lennb] = 0;
	lennb--;
	while (lennb >= 0)
	{
		s[lennb] = (n % 10) + '0';
		n /= 10;
		lennb--;
	}
	return (ft_add_str_to_buff(s));
}

int			printf_itoa_base(uintmax_t n, char *base)
{
	char		s[65];
	int			len_base;
	int			lennb;

	len_base = ft_strlen(base);
	lennb = ft_len_uint(n, len_base);
	s[lennb] = 0;
	lennb--;
	while (lennb >= 0)
	{
		s[lennb] = base[n % len_base];
		n /= len_base;
		lennb--;
	}
	return (ft_add_str_to_buff(s));
}
