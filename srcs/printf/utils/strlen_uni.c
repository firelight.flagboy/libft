/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlen_printf.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/22 12:36:09 by fbenneto          #+#    #+#             */
/*   Updated: 2019/11/15 09:00:49 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft/printf/utils.h"

int			printf_len_uni_char(wchar_t n)
{
	int	l;

	l = -1;
	if (n >= 0 && n < 0x80)
		l = 1;
	else if (n < 0x800)
		l = 2;
	else if (n < 0x10000)
		l = 3;
	else if (n < 0x200000)
		l = 4;
	if (l <= (int)MB_CUR_MAX)
		return (l);
	return (-1);
}

int			printf_len_uni_str(wchar_t *s, t_flags *f)
{
	int len;
	int l;

	len = 0;
	while (*s)
	{
		l = printf_len_uni_char(*s);
		if (l == -1)
			return (-1);
		if (f->flags & HI_PRECISION && len + l > f->precision)
			break ;
		len += l;
		s++;
	}
	return (len);
}
