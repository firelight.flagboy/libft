/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   string_copy.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/06 12:52:00 by fbenneto          #+#    #+#             */
/*   Updated: 2019/11/15 09:04:31 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/printf/utils.h"

static inline size_t	ft_n_process(size_t l, size_t n)
{
	if (l < n)
		return (l);
	else
		return (n);
}

char					*printf_string_cpy(char *dest, char const *s, size_t n,
	size_t l)
{
	size_t	i;
	size_t	*bdest;
	size_t	*bs;
	char	*r;

	r = dest;
	bdest = (size_t*)dest;
	bs = (size_t*)s;
	n = ft_n_process(l, n);
	i = 0;
	while (i < n / sizeof(size_t))
	{
		*bdest++ = *bs++;
		i++;
	}
	i = 0;
	dest = dest + n - n % sizeof(size_t);
	s = s + n - n % sizeof(size_t);
	while (i < n % sizeof(size_t))
	{
		*dest++ = *s++;
		i++;
	}
	*dest = 0;
	return (r);
}
