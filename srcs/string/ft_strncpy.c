/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/07 16:05:32 by fbenneto          #+#    #+#             */
/*   Updated: 2019/11/14 11:31:23 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/string.h"

char	*ft_strncpy(char *dst, const char *src, size_t len)
{
	size_t	len_src;
	char	*d;

	len_src = ft_strnlen(src, len);
	len -= len_src;
	d = dst;
	while (len_src > 0)
	{
		*d++ = *src++;
		len_src--;
	}
	while (len > 0)
	{
		*d++ = 0;
		len--;
	}
	return (dst);
}
