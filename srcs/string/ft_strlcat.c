/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/10 12:15:35 by fbenneto          #+#    #+#             */
/*   Updated: 2019/11/25 11:03:22 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/string.h"

size_t	ft_strlcat(char *dst, const char *src, size_t size)
{
	size_t	n;
	size_t	res;
	char	*d;

	n = ft_strnlen(dst, size);
	res = ft_strlen(src) + n;
	if (n == size)
		return (res);
	d = dst + n;
	n = size - n - 1;
	while (n && *src)
	{
		*d++ = *src++;
		n--;
	}
	*d = 0;
	return (res);
}
