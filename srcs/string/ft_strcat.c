/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcat.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/07 16:13:52 by fbenneto          #+#    #+#             */
/*   Updated: 2019/11/29 15:21:47 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/string.h"

char	*ft_strcat(char *s1, const char *s2)
{
	size_t	len;
	char	*dup;

	dup = s1;
	len = ft_strlen(s1);
	s1 += len;
	while (*s2)
		*s1++ = *s2++;
	*s1 = '\0';
	return (dup);
}
