/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/09 14:24:22 by fbenneto          #+#    #+#             */
/*   Updated: 2019/11/14 15:38:19 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/string.h"

char	*ft_strmap(char const *s, char (*f)(char))
{
	char	*res;
	char	*r;

	if (!(res = (char*)malloc((ft_strlen(s) + 1) * sizeof(char))))
		return (NULL);
	r = res;
	while (*s)
		*r++ = f(*s++);
	*r = '\0';
	return (res);
}
