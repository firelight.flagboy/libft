/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/09 16:06:54 by fbenneto          #+#    #+#             */
/*   Updated: 2019/10/17 09:52:35 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/string.h"

char	*ft_strjoin(char const *s1, char const *s2)
{
	char	*res;
	size_t	i;
	size_t	j;

	i = ft_strlen(s1);
	j = ft_strlen(s2);
	if (!(res = (char*)malloc((i + j + 1) * sizeof(char))))
		return (NULL);
	ft_memcpy(res, s1, i);
	ft_memcpy(res + i, s2, j);
	res[i + j] = 0;
	return (res);
}
