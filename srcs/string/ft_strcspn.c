/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcspn.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/14 11:05:22 by fbenneto          #+#    #+#             */
/*   Updated: 2019/11/25 11:03:49 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/string.h"

size_t	ft_strcspn(const char *s, const char *charset)
{
	const char	*dup;

	dup = s;
	while (*s && ft_strchr(charset, *s) == NULL)
	{
		s++;
	}
	return (s - dup);
}
