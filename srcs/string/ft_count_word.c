/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_count_word.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/14 16:12:40 by fbenneto          #+#    #+#             */
/*   Updated: 2019/10/17 09:45:30 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/string.h"
#include "libft/type.h"

size_t	ft_count_word(char *str)
{
	int		inw;
	size_t	count;

	inw = 0;
	count = 0;
	while (*str)
	{
		if (!ft_isspace(*str) && !inw)
		{
			count++;
			inw = 1;
		}
		else
		{
			if (ft_isspace(*str))
				inw = 0;
		}
		str++;
	}
	return (count);
}
