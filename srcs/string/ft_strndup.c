/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strndup.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/14 15:57:39 by fbenneto          #+#    #+#             */
/*   Updated: 2019/11/14 11:18:49 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/string.h"

char	*ft_strndup(char const *str, size_t n)
{
	char	*res;
	size_t	len;

	len = ft_strnlen(str, n);
	if (!(res = ft_strnew(len)))
		return (NULL);
	ft_strncpy(res, str, len);
	return (res);
}
