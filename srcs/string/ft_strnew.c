/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/09 08:29:29 by fbenneto          #+#    #+#             */
/*   Updated: 2019/06/06 10:10:28 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/string.h"

char	*ft_strnew(size_t size)
{
	char *res;

	if (!(res = (char*)malloc((size + 1) * sizeof(char))))
		return (NULL);
	ft_memset(res, 0, size + 1);
	return (res);
}
