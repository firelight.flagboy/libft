/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_realloc_str.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/13 17:52:25 by fbenneto          #+#    #+#             */
/*   Updated: 2019/11/25 14:11:00 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/string.h"

char	*ft_realloc_str(char **astr, size_t len)
{
	char	*res;
	char	*src;

	if (!astr)
		return (NULL);
	if (!*astr)
		return (NULL);
	src = *astr;
	if (!(res = ft_strnew(len)))
		return (NULL);
	ft_memcpy(res, src, len);
	free(src);
	*astr = res;
	return (res);
}
