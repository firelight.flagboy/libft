/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr_fd.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/10 08:40:03 by fbenneto          #+#    #+#             */
/*   Updated: 2019/06/11 15:26:29 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/output.h"

#define MAX_LEN_DIGIT_INT 12
#define FT_PUTNBR_RADIX 10

void	ft_putnbr_fd(int n, int fd)
{
	unsigned int	nb;
	char			s[MAX_LEN_DIGIT_INT];
	size_t			i;

	nb = n;
	i = 0;
	if (n < 0)
	{
		nb = -n;
		s[i++] = '-';
	}
	while (nb)
	{
		s[i++] = nb % FT_PUTNBR_RADIX + '0';
		nb /= FT_PUTNBR_RADIX;
	}
	s[i] = 0;
	return (ft_putstr_fd(s, fd));
}
