/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_len_uint.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/06 10:50:42 by fbenneto          #+#    #+#             */
/*   Updated: 2019/10/24 11:25:20 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/num.h"

ssize_t	ft_len_uint(size_t num, uint radix)
{
	ssize_t i;

	i = 0;
	if (num == 0)
		return (1);
	if (radix == 0)
		return (-1);
	while (num)
	{
		num /= radix;
		i++;
	}
	return (i);
}
