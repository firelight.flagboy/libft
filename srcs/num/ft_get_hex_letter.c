/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_get_hex_letter.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/07 14:01:30 by fbenneto          #+#    #+#             */
/*   Updated: 2019/10/17 09:45:03 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/num.h"

char	ft_get_hex_letter(int val)
{
	return (HEX_CHARSET[((unsigned char)val) & 0xf]);
}
