/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lendigit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/06 10:42:12 by fbenneto          #+#    #+#             */
/*   Updated: 2019/11/15 12:46:19 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/num.h"
#include "libft/type.h"

size_t	ft_lendigit(const char *str)
{
	const char *save;

	save = str;
	while (ft_isdigit(*str))
	{
		str++;
	}
	return (str - save);
}
