/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_len_int.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/06 10:49:44 by fbenneto          #+#    #+#             */
/*   Updated: 2019/10/24 11:25:17 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/num.h"

ssize_t	ft_len_int(ssize_t num, uint radix)
{
	if (num < 0)
		return (ft_len_uint(-num, radix));
	return (ft_len_uint(num, radix));
}
