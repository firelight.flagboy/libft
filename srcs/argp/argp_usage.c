/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   argp_usage.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/18 15:02:58 by fbenneto          #+#    #+#             */
/*   Updated: 2019/11/27 13:00:31 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/argp.h"

void	ft_argp_usage(const t_argp_state *state)
{
	ft_argp_state_help(state, stderr, FT_ARGP_HELP_STD_USAGE);
}
