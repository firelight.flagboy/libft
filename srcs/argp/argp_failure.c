/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   argp_failure.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/18 15:02:58 by fbenneto          #+#    #+#             */
/*   Updated: 2019/11/29 15:33:01 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/argp.h"
#include "libft/printf.h"

#include <stdlib.h>
#include <string.h>

void	ft_argp_failure(
	const t_argp_state *state, int status, int errnum, const char *fmt, ...)
{
	va_list			ap;
	char			*s;
	FILE			*stream;
	char			buff[200];

	s = NULL;
	if (state && (state->flags & FT_ARGP_NO_ERRS))
		return ;
	stream = state ? state->err_stream : stderr;
	ft_fprintf(stream, "%s",
		state ? state->name : g_argp_program_invocation_name);
	va_start(ap, fmt);
	if (fmt && ft_vasprintf(&s, fmt, ap) != -1)
		ft_fprintf(stream, ": %s", s);
	va_end(ap);
	free(s);
	if (errnum)
	{
		strerror_r(errnum, buff, sizeof(buff));
		ft_fprintf(stream, ": %s", buff);
	}
	ft_fprintf(stream, "\n");
	if (status && (!state && !(state->flags & FT_ARGP_NO_EXIT)))
		exit(status);
}
