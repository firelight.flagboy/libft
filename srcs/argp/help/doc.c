/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   doc.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <f.benneto@student.42.fr>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/18 15:42:54 by fbenneto          #+#    #+#             */
/*   Updated: 2019/12/05 09:37:13 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/printf.h"
#include "libft/string.h"

#include "libft/argp/help.h"
#include "libft/argp/internal/help_internal.h"

#define POST 0
#define PRE_BLANK 1
#define FIRST_ONLY 2

static int	print_doc(t_fmt *fmt, const char *doc, int *values)
{
	unsigned max;

	if (values[POST])
	{
		doc += 1;
		max = ft_strlen(doc);
	}
	else
		max = ft_strcspn(doc, "\v");
	if (values[PRE_BLANK])
		fmt_new_line(fmt);
	fmt_nputsnl(fmt, doc, max);
	return (1);
}

int			ft_argp_help_doc(t_fmt *fmt, const t_argp *argp,
	t_help_state *state, int *v)
{
	const char			*start = NULL;
	const t_argp_child	*child = argp->children;
	unsigned			as_print;

	as_print = 0;
	if (argp->doc)
	{
		if (v[POST])
			start = ft_strchr(argp->doc, '\v');
		else
			start = argp->doc;
	}
	fmt_set_left_margin(fmt, 0);
	if (start)
		as_print |= print_doc(fmt, start, v);
	if (child)
		while (child->argp && !(v[FIRST_ONLY] && as_print))
			as_print |= ft_argp_help_doc(fmt, (child++)->argp, state,
			(int[]){v[POST], v[PRE_BLANK] || as_print, v[FIRST_ONLY]});
	return (as_print);
}
