/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   usage.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/18 15:42:54 by fbenneto          #+#    #+#             */
/*   Updated: 2020/03/10 09:58:20 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/printf.h"
#include "libft/string.h"
#include "libft/type.h"

#include "libft/argp/help.h"
#include "libft/argp/define.h"
#include "libft/argp/internal/usage_util.h"

static void		print_long_usage_long_opt(t_fmt *fmt,
	const t_help_option_alias *alias, const t_help_option *option,
	int long_only)
{
	const char *prefix;

	prefix = (long_only ? "-" : "--");
	if (option->metavar)
	{
		if (option->flags & FT_ARGP_OPTION_ARG_OPTIONAL)
			fmt_printf(fmt, " [%s%s [%s]]", prefix, alias->name,
				option->metavar);
		else
			fmt_printf(fmt, " [%s%s %s]", prefix, alias->name, option->metavar);
	}
	else
	{
		if (option->flags & FT_ARGP_OPTION_ARG_OPTIONAL)
			fmt_printf(fmt, " [%s%s]", prefix, alias->name);
		else
			fmt_printf(fmt, " [%s%s]", prefix, alias->name);
	}
}

static void		print_long_usage_long_opt_loop(t_fmt *fmt, t_help_state *state)
{
	t_help_option_group	*group;
	t_help_option		*options;
	t_help_option_alias	*alias;

	group = state->group_head;
	while (group)
	{
		options = group->options;
		while (options)
		{
			alias = options->alias;
			while (alias)
			{
				if (!(alias->flags &
					(FT_ARGP_OPTION_NO_USAGE | FT_ARGP_OPTION_HIDDEN))
					&& alias->name)
					print_long_usage_long_opt(fmt, alias, options,
						state->flags & FT_ARGP_LONG_ONLY);
				alias = alias->next;
			}
			options = options->next;
		}
		group = group->next;
	}
}

static void		print_long_usage(t_fmt *fmt, t_help_state *state)
{
	fmt_set_left_margin(fmt, USAGE_OPT_COL);
	print_long_usage_no_arg_loop(fmt, state);
	print_long_usage_short_opt_loop(fmt, state);
	print_long_usage_long_opt_loop(fmt, state);
}

static void		ft_argp_help_usage_internal(t_fmt *fmt,
	t_help_usage_internal *intrn, t_help_state *state, char **current_args_doc)
{
	const char		*s;

	fmt_set_left_margin(fmt, 0);
	print_start_usage(fmt, intrn->name, intrn->first_pattern);
	if (intrn->long_usage)
		print_long_usage(fmt, state);
	else if (group_have_option(state->group_head))
		fmt_puts(fmt, " [OPTIONS ...]");
	s = get_next_args_doc(state->argp, current_args_doc,
		&intrn->current_level, intrn->level);
	if (s)
		fmt_printf(fmt, " %.*s", (int)ft_strcspn(s, "\n"), s);
	fmt_new_line(fmt);
}

int				ft_argp_help_usage(t_fmt *fmt, t_help_state *state,
	const char *name, int long_usage)
{
	const size_t			nb_levels = usage_nb_level(state->argp);
	char					*current_args_doc;
	t_help_usage_internal	state_internal;

	state_internal.level = 0;
	state_internal.first_pattern = 1;
	state_internal.long_usage = long_usage;
	state_internal.name = name;
	current_args_doc = NULL;
	while (state_internal.level < nb_levels)
	{
		state_internal.current_level = 0;
		ft_argp_help_usage_internal(fmt, &state_internal, state,
			&current_args_doc);
		state_internal.long_usage = 0;
		state_internal.first_pattern = 0;
		state_internal.level++;
	}
	return (1);
}
