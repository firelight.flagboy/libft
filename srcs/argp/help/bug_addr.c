/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   bug_addr.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <f.benneto@student.42.fr>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/18 15:42:54 by fbenneto          #+#    #+#             */
/*   Updated: 2019/12/05 09:37:32 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/printf.h"

#include "libft/argp/help.h"
#include "libft/argp/global.h"

int	ft_argp_help_bug_addr(t_fmt *fmt, t_help_state *state)
{
	fmt_set_left_margin(fmt, 0);
	if (g_argp_program_bug_address)
	{
		if (state->as_print)
			fmt_new_line(fmt);
		fmt_printfnl(fmt, "Report bugs to %s.",
			g_argp_program_bug_address);
		return (1);
	}
	return (0);
}
