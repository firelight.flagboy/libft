/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_margin.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <f.benneto@student.42.fr>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/04 15:18:35 by fbenneto          #+#    #+#             */
/*   Updated: 2019/12/05 08:38:03 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/mem.h"

#include "libft/argp/internal/help_format.h"

int	fmt_check_margin(t_fmt *fmt)
{
	unsigned size;

	if (fmt->cursor >= fmt->left_margin)
		return (0);
	size = fmt->left_margin - fmt->cursor;
	ft_memset(fmt->p, ' ', size);
	fmt->p += size;
	fmt->cursor = fmt->left_margin;
	return (size);
}
