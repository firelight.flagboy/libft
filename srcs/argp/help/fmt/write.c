/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   write.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <f.benneto@student.42.fr>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/04 13:50:28 by fbenneto          #+#    #+#             */
/*   Updated: 2019/12/05 08:39:54 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/mem.h"

#include "libft/argp/internal/help_format.h"

int	fmt_write(t_fmt *fmt, const void *s, size_t len)
{
	ssize_t l;
	ssize_t t;

	l = len;
	while (l > 0)
	{
		t = l;
		if (t > fmt->end - fmt->p)
		{
			t = fmt->end - fmt->p;
			ft_memcpy(fmt->p, s, t);
			fmt_flush(fmt);
		}
		else
			ft_memcpy(fmt->p, s, t);
		s += t;
		fmt->p += t;
		fmt->cursor += t;
		l -= t;
	}
	return (len);
}
