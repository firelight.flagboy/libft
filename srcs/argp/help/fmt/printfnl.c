/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   printfnl.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <f.benneto@student.42.fr>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/05 09:31:22 by fbenneto          #+#    #+#             */
/*   Updated: 2019/12/05 09:35:47 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdarg.h>
#include <stdlib.h>
#include "libft/printf.h"

#include "libft/argp/internal/help_format.h"

int	fmt_printfnl(t_fmt *fmt, const char *format, ...)
{
	va_list	ap;
	char	*s;
	int		res;

	va_start(ap, format);
	if (ft_vasprintf(&s, format, ap) == -1)
	{
		va_end(ap);
		return (-1);
	}
	va_end(ap);
	res = fmt_putsnl(fmt, s);
	free(s);
	return (res);
}
