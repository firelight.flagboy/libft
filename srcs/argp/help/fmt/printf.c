/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   printf.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <f.benneto@student.42.fr>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/04 14:34:23 by fbenneto          #+#    #+#             */
/*   Updated: 2019/12/05 08:53:31 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/printf.h"
#include <stdarg.h>
#include <stdlib.h>

#include "libft/argp/internal/help_format.h"

int	fmt_printf(t_fmt *fmt, const char *format, ...)
{
	va_list	ap;
	char	*s;
	int		res;

	va_start(ap, format);
	if (ft_vasprintf(&s, format, ap) == -1)
	{
		va_end(ap);
		return (-1);
	}
	va_end(ap);
	fmt_check_margin(fmt);
	res = fmt_puts(fmt, s);
	free(s);
	return (res);
}
