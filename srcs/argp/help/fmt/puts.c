/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   puts.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <f.benneto@student.42.fr>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/04 10:57:03 by fbenneto          #+#    #+#             */
/*   Updated: 2019/12/05 08:53:04 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/string.h"

#include "libft/argp/internal/help_format.h"

static int	fmt_puts_internal(t_fmt *fmt, const char *s, size_t len)
{
	size_t max;
	size_t l;

	l = len;
	fmt_check_margin(fmt);
	while (l > 0)
	{
		max = fmt_ensure(fmt, l);
		fmt_write(fmt, s, max);
		s += max;
		l -= max;
	}
	return (len);
}

int			fmt_nputs(t_fmt *fmt, const char *s, size_t n)
{
	return (fmt_puts_internal(fmt, s, ft_strnlen(s, n)));
}

int			fmt_puts(t_fmt *fmt, const char *s)
{
	return (fmt_puts_internal(fmt, s, ft_strlen(s)));
}
