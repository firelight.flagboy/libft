/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   flush.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <f.benneto@student.42.fr>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/04 10:57:03 by fbenneto          #+#    #+#             */
/*   Updated: 2019/12/05 08:38:47 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/printf.h"

#include "libft/argp/internal/help_format.h"

void	fmt_flush(t_fmt *fmt)
{
	ft_fprintf(fmt->stream, "%.*s", (int)(fmt->p - fmt->buffer), fmt->buffer);
	fmt->p = fmt->buffer;
}
