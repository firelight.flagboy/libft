/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ensure.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <f.benneto@student.42.fr>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/04 14:42:22 by fbenneto          #+#    #+#             */
/*   Updated: 2019/12/05 08:58:34 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/argp/internal/help_format.h"

int	fmt_ensure(t_fmt *fmt, size_t size)
{
	size_t diff;

	if (fmt->cursor >= fmt->right_margin)
	{
		fmt_new_line(fmt);
		fmt_check_margin(fmt);
	}
	else if (fmt->cursor + size >= fmt->right_margin
		&& fmt->right_margin - fmt->left_margin > size)
	{
		fmt_new_line(fmt);
		fmt_check_margin(fmt);
	}
	diff = fmt->right_margin - fmt->cursor;
	return (diff > size ? size : diff);
}
