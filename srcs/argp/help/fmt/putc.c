/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   putc.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <f.benneto@student.42.fr>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/04 10:57:03 by fbenneto          #+#    #+#             */
/*   Updated: 2019/12/05 08:41:07 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/argp/internal/help_format.h"

int	fmt_putc(t_fmt *fmt, char c)
{
	fmt_ensure(fmt, 1);
	fmt->cursor++;
	return (*fmt->p++ = c);
}
