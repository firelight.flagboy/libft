/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/04 10:57:03 by fbenneto          #+#    #+#             */
/*   Updated: 2019/12/04 15:55:11 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft/mem.h"

#include "libft/argp/internal/help_format.h"

char	*fmt_init(t_fmt *fmt, unsigned right_marging, FILE *stream)
{
	ft_bzero(fmt, sizeof(t_fmt));
	fmt->stream = stream;
	fmt->buffer = (char*)malloc(FMT_BUFF_SIZE);
	fmt->end = fmt->buffer + FMT_BUFF_SIZE;
	fmt->p = fmt->buffer;
	fmt->right_margin = right_marging;
	return (fmt->buffer);
}
