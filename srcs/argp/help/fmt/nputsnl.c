/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   nputsnl.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <f.benneto@student.42.fr>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/05 09:31:22 by fbenneto          #+#    #+#             */
/*   Updated: 2019/12/05 09:35:36 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/argp/internal/help_format.h"

int	fmt_nputsnl(t_fmt *fmt, const char *s, size_t n)
{
	int res;

	res = fmt_nputs(fmt, s, n);
	fmt_new_line(fmt);
	return (res);
}
