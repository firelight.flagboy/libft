/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   clean.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <f.benneto@student.42.fr>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/04 10:57:03 by fbenneto          #+#    #+#             */
/*   Updated: 2019/12/05 08:38:15 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

#include "libft/argp/internal/help_format.h"

void	fmt_clean(t_fmt *fmt)
{
	if (fmt->p != fmt->buffer)
		fmt_flush(fmt);
	free(fmt->buffer);
}
