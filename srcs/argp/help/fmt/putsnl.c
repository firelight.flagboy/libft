/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   putsnl.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <f.benneto@student.42.fr>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/05 09:31:22 by fbenneto          #+#    #+#             */
/*   Updated: 2019/12/05 09:32:31 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/argp/internal/help_format.h"

int	fmt_putsnl(t_fmt *fmt, const char *s)
{
	int res;

	res = fmt_puts(fmt, s);
	fmt_new_line(fmt);
	return (res);
}
