/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   set_left_margin.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/04 10:57:03 by fbenneto          #+#    #+#             */
/*   Updated: 2019/12/04 12:04:23 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/argp/internal/help_format.h"

unsigned	fmt_set_left_margin(t_fmt *fmt, unsigned left_margin)
{
	unsigned old;

	old = fmt->left_margin;
	fmt->left_margin = left_margin;
	if (fmt->left_margin >= fmt->right_margin)
	{
		fmt->left_margin = 0;
	}
	return (old);
}
