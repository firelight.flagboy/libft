/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   new_line.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <f.benneto@student.42.fr>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/04 12:02:50 by fbenneto          #+#    #+#             */
/*   Updated: 2019/12/05 08:54:25 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/mem.h"

#include "libft/argp/internal/help_format.h"

void	fmt_new_line(t_fmt *fmt)
{
	if (fmt->p + 1 >= fmt->end)
		fmt_flush(fmt);
	*fmt->p++ = '\n';
	fmt->cursor = 0;
}
