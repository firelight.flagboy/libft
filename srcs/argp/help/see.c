/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   see.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <f.benneto@student.42.fr>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/18 15:42:54 by fbenneto          #+#    #+#             */
/*   Updated: 2019/12/05 09:37:45 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/argp/help.h"
#include "libft/printf.h"

int	ft_argp_help_see(t_fmt *fmt, const char *name)
{
	fmt_set_left_margin(fmt, 0);
	fmt_printfnl(fmt,
		"Try `%s --help' or `%s --usage' for more information.", name, name);
	return (1);
}
