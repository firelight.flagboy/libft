/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   internal.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/29 11:24:47 by fbenneto          #+#    #+#             */
/*   Updated: 2020/03/10 09:53:00 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/string.h"
#include "libft/printf.h"
#include <errno.h>

#include "libft/argp.h"
#include "libft/argp/help.h"
#include "libft/argp/internal/help_internal.h"
#include "libft/argp/internal/help_format.h"
#include "libft/argp/utils.h"

static void	init_help_state(const t_argp *argp, t_help_state *state,
	int current_group)
{
	const t_argp_child	*child = argp->children;
	const t_argp_option	*opt = argp->options;

	if (opt)
	{
		while (!option_is_end(opt))
		{
			insert_argp_opt(opt, state, &current_group);
			opt++;
		}
	}
	if (child)
	{
		while (child->argp)
		{
			init_help_state(child->argp, state, child->group == 0 ?
				current_group : child->group);
			child++;
		}
	}
}

static void	print_opts(t_help_option *opts)
{
	t_help_option_alias	*ali;

	while (opts)
	{
		ft_printf("  opt { metavar: '%s', flags: %x }\n",
			opts->metavar, opts->flags);
		ali = opts->alias;
		while (ali)
		{
			ft_printf("    alias { key: %x, name: '%s', flags: %x }\n",
				ali->key, ali->name, ali->flags);
			ali = ali->next;
		}
		opts = opts->next;
	}
}

void		debug_help_state(t_help_state *state)
{
	t_help_option_group	*group;
	t_help_doc			*doc;

	group = state->group_head;
	while (group)
	{
		ft_printf("group { id: %d, header: '%s' }\n", group->id, group->header);
		print_opts(group->options);
		doc = group->docs;
		while (doc)
		{
			ft_printf("  doc { name: '%s', content: '%s' }\n",
				doc->name, doc->content);
			doc = doc->next;
		}
		group = group->next;
	}
	doc = state->docs;
	while (doc)
	{
		ft_printf("doc { name: '%s', content: '%s' }\n",
			doc->name, doc->content);
		doc = doc->next;
	}
}

static void	dispatch_help_message(t_help_data *data, const t_argp *argp,
	t_help_state *st)
{
	if ((data->flags & (FT_ARGP_HELP_USAGE | FT_ARGP_HELP_SHORT_USAGE)))
		st->as_print |= ft_argp_help_usage(&st->fmt, st, data->name,
			(data->flags & FT_ARGP_HELP_USAGE));
	if ((data->flags & FT_ARGP_HELP_PRE_DOC))
		st->as_print |= ft_argp_help_doc(&st->fmt, argp, st,
			(int[]){ 0, 0, 1 });
	if ((data->flags & FT_ARGP_HELP_SEE))
		st->as_print |= ft_argp_help_see(&st->fmt, data->name);
	if ((data->flags & FT_ARGP_HELP_LONG))
		st->as_print |= ft_argp_help_long(&st->fmt, st,
			(data->flags & FT_ARGP_LONG_ONLY));
	if ((data->flags & FT_ARGP_HELP_POST_DOC))
		st->as_print |= ft_argp_help_doc(&st->fmt, argp, st,
			(int[]){ 1, st->as_print, 0});
	if ((data->flags & FT_ARGP_HELP_BUG_ADDR))
		ft_argp_help_bug_addr(&st->fmt, st);
}

void		ft_argp_help_internal(const t_argp *argp, const t_argp_state *state,
	FILE *stream, t_help_data *data)
{
	t_help_state st;

	ft_bzero(&st, sizeof(t_help_state));
	st.state = state;
	st.argp = argp;
	st.name = data->name;
	st.flags = data->flags;
	fmt_init(&st.fmt, RIGHT_MARGIN, stream);
	init_help_state(argp, &st, 0);
	dispatch_help_message(data, argp, &st);
	fmt_clean(&st.fmt);
}
