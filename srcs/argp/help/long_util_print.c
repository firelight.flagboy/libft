/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   long_util_print.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/10 09:28:49 by fbenneto          #+#    #+#             */
/*   Updated: 2020/03/10 09:29:20 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/argp/define.h"
#include "libft/argp/internal/help_long_utils.h"

int	print_docs(t_fmt *fmt, t_help_doc *docs)
{
	if (docs == NULL)
		return (0);
	while (docs)
	{
		fmt_set_left_margin(fmt, DOC_OPT_COL);
		fmt_puts(fmt, docs->name);
		fmt_set_left_margin(fmt, OPT_DOC_COL);
		fmt_putsnl(fmt, docs->content);
		docs = docs->next;
	}
	return (1);
}

int	print_header(t_fmt *fmt, const char *header)
{
	fmt_set_left_margin(fmt, GROUP_HEADER);
	return (fmt_putsnl(fmt, header));
}

int	print_metavar(t_fmt *fmt, const char *metavar,
	unsigned flags, int is_short)
{
	if (is_short)
	{
		if (flags & FT_ARGP_OPTION_ARG_OPTIONAL)
			return (fmt_printf(fmt, " [%s]", metavar));
		return (fmt_printf(fmt, " %s", metavar));
	}
	if (flags & FT_ARGP_OPTION_ARG_OPTIONAL)
		return (fmt_printf(fmt, "[=%s]", metavar));
	return (fmt_printf(fmt, "=%s", metavar));
}

int	print_option(t_fmt *fmt, const t_help_option *option, int long_only)
{
	const unsigned	print_long_opt = next_alias_is_print(option->alias, 0);
	unsigned		as_print;

	as_print = print_alias_short_option(fmt, option->alias,
		print_long_opt, option);
	if (print_long_opt)
		as_print |= print_alias_long_option(fmt, option->alias, option,
			long_only);
	if (option->doc)
	{
		if (fmt->cursor > OPT_DOC_COL)
			fmt_new_line(fmt);
		fmt_set_left_margin(fmt, OPT_DOC_COL);
		as_print |= fmt_puts(fmt, option->doc);
	}
	if (as_print)
		fmt_new_line(fmt);
	return (as_print);
}
