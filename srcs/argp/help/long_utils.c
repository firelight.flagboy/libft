/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   long_utils.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/10 09:04:56 by fbenneto          #+#    #+#             */
/*   Updated: 2020/03/10 09:54:09 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/type.h"
#include "libft/string.h"

#include "libft/argp/define.h"
#include "libft/argp/internal/help_long_utils.h"

int	print_metavar(t_fmt *fmt, const char *metavar,
	unsigned flags, int is_short)
{
	if (is_short)
	{
		if (flags & FT_ARGP_OPTION_ARG_OPTIONAL)
			return (fmt_printf(fmt, " [%s]", metavar));
		return (fmt_printf(fmt, " %s", metavar));
	}
	if (flags & FT_ARGP_OPTION_ARG_OPTIONAL)
		return (fmt_printf(fmt, "[=%s]", metavar));
	return (fmt_printf(fmt, "=%s", metavar));
}

int	next_alias_is_print(const t_help_option_alias *a, int is_short)
{
	if (is_short)
	{
		while (a)
		{
			if (!(a->flags & FT_ARGP_OPTION_HIDDEN) && ft_isprint(a->key))
				return (1);
			a = a->next;
		}
	}
	else
	{
		while (a)
		{
			if (!(a->flags & FT_ARGP_OPTION_HIDDEN) && a->name)
				return (1);
			a = a->next;
		}
	}
	return (0);
}

int	print_alias_short_option(t_fmt *fmt,
	const t_help_option_alias *alias, int print_long_opt,
	const t_help_option *option)
{
	int as_print;

	as_print = 0;
	fmt_set_left_margin(fmt, SHORT_OPT_COL);
	while (alias)
	{
		if (!(alias->flags & FT_ARGP_OPTION_HIDDEN) && ft_isprint(alias->key))
		{
			as_print |= fmt_printf(fmt, "-%c", alias->key);
			if (!print_long_opt && option->metavar)
				print_metavar(fmt, option->metavar, option->flags, 1);
			if (print_long_opt || next_alias_is_print(alias->next, 1))
				fmt_puts(fmt, ", ");
		}
		alias = alias->next;
	}
	return (as_print);
}
