/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   long.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/18 15:42:54 by fbenneto          #+#    #+#             */
/*   Updated: 2020/03/10 10:05:46 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/printf.h"
#include "libft/string.h"
#include "libft/type.h"

#include "libft/argp/help.h"
#include "libft/argp/define.h"
#include "libft/argp/internal/help_long_utils.h"

int			print_alias_long_option(t_fmt *fmt,
	const t_help_option_alias *alias,
	const t_help_option *option, int long_only)
{
	int as_print;

	as_print = 0;
	if (fmt->cursor > LONG_OPT_COL)
		fmt_set_left_margin(fmt, LONG_OPT_COL);
	else
		fmt_set_left_margin(fmt, SHORT_OPT_COL);
	while (alias)
	{
		if (!(alias->flags & FT_ARGP_OPTION_HIDDEN) && alias->name)
		{
			if (long_only)
				as_print |= fmt_printf(fmt, "-%s", alias->name);
			else
				as_print |= fmt_printf(fmt, "--%s", alias->name);
			if (option->metavar)
				print_metavar(fmt, option->metavar, option->flags, 0);
			if (next_alias_is_print(alias->next, 0))
				fmt_puts(fmt, ", ");
		}
		alias = alias->next;
	}
	return (as_print);
}

static int	print_option(t_fmt *fmt, const t_help_option *option, int long_only)
{
	const unsigned	print_long_opt = next_alias_is_print(option->alias, 0);
	unsigned		as_print;

	as_print = print_alias_short_option(fmt, option->alias,
		print_long_opt, option);
	if (print_long_opt)
		as_print |= print_alias_long_option(fmt, option->alias, option,
			long_only);
	if (option->doc)
	{
		if (fmt->cursor > OPT_DOC_COL)
			fmt_new_line(fmt);
		fmt_set_left_margin(fmt, OPT_DOC_COL);
		as_print |= fmt_puts(fmt, option->doc);
	}
	if (as_print)
		fmt_new_line(fmt);
	return (as_print);
}

static int	print_options(t_fmt *fmt, const t_help_option_group *group,
	int long_only)
{
	unsigned			as_print;
	const t_help_option	*options = group->options;

	as_print = 0;
	while (options)
	{
		as_print |= print_option(fmt, options, long_only);
		options = options->next;
	}
	return (as_print);
}

static int	print_group(t_fmt *fmt, const t_help_option_group *group,
	int long_only)
{
	unsigned as_print;

	as_print = 0;
	fmt_new_line(fmt);
	if (group->header)
		as_print |= print_header(fmt, group->header);
	as_print |= print_options(fmt, group, long_only);
	as_print |= print_docs(fmt, group->docs);
	return (as_print);
}

int			ft_argp_help_long(t_fmt *fmt, t_help_state *state, int long_only)
{
	unsigned					as_print;
	const t_help_option_group	*group;

	as_print = 0;
	if (state->as_print)
		fmt_new_line(fmt);
	group = state->group_head;
	state->hook = &as_print;
	while (group)
	{
		as_print |= print_group(fmt, group, long_only);
		group = group->next;
	}
	if (as_print)
		fmt_new_line(fmt);
	print_docs(fmt, state->docs);
	return (as_print);
}
