/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   usage_util_print.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/10 09:18:27 by fbenneto          #+#    #+#             */
/*   Updated: 2020/03/10 10:11:40 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/type.h"

#include "libft/argp/internal/usage_util.h"

int		print_long_usage_no_arg(t_fmt *fmt,
	const t_help_option_alias *alias, char *buff, int *idx)
{
	int as_print;

	as_print = 0;
	while (alias)
	{
		if (should_print_long_usage(alias))
		{
			buff[idx[0]] = alias->key;
			idx[0] = idx[0] + 1;
			if (*idx >= (int)sizeof(buff))
			{
				if (idx[1])
				{
					idx[1] = 0;
					as_print = fmt_printf(fmt, " [-%.*s", idx[0], buff);
				}
				else
					as_print = fmt_printf(fmt, "%.*s", idx[0], buff);
				idx[0] = 0;
			}
		}
		alias = alias->next;
	}
	return (as_print);
}

int		print_options_long_usage_no_arg(t_help_option *options,
	t_fmt *fmt, char *buff, int *arr)
{
	int as_print;

	as_print = 0;
	while (options)
	{
		if (options->metavar == NULL)
			as_print |= print_long_usage_no_arg(fmt, options->alias, buff,
				arr);
		options = options->next;
	}
	return (as_print);
}

void	print_long_usage_no_arg_loop(t_fmt *fmt, t_help_state *state)
{
	int					arr[2];
	char				buff[64];
	t_help_option_group	*group;
	int					as_print;

	arr[0] = 0;
	arr[1] = 1;
	as_print = 0;
	group = state->group_head;
	while (group)
	{
		as_print |= print_options_long_usage_no_arg(group->options, fmt, buff,
			arr);
		group = group->next;
	}
	if (as_print == 0 && arr[0] > 0)
		fmt_printf(fmt, " [-%.*s]", arr[0], buff);
	else if (as_print > 0)
		fmt_printf(fmt, "%.*s]", arr[0], buff);
}

void	print_long_usage_short(t_fmt *fmt,
	const t_help_option_alias *alias, unsigned flags, const char *metavar)
{
	while (alias)
	{
		if (!(alias->flags & (FT_ARGP_OPTION_NO_USAGE | FT_ARGP_OPTION_HIDDEN))
			&& ft_isprint(alias->key))
		{
			if (flags & FT_ARGP_OPTION_ARG_OPTIONAL)
				fmt_printf(fmt, " [-%c [%s]]", alias->key, metavar);
			else
				fmt_printf(fmt, " [-%c %s]", alias->key, metavar);
		}
		alias = alias->next;
	}
}

void	print_long_usage_short_opt_loop(t_fmt *fmt, t_help_state *state)
{
	t_help_option_group	*group;
	t_help_option		*options;

	group = state->group_head;
	while (group)
	{
		options = group->options;
		while (options)
		{
			if (options->metavar)
				print_long_usage_short(fmt, options->alias, options->flags,
					options->metavar);
			options = options->next;
		}
		group = group->next;
	}
}
