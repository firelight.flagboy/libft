/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   help_option_alias.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/02 15:05:38 by fbenneto          #+#    #+#             */
/*   Updated: 2019/12/03 13:47:23 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft/mem.h"

#include "libft/argp/internal/help_internal.h"
#include "libft/argp/define.h"

t_help_option_alias	*new_option_alias(const t_argp_option *opt)
{
	t_help_option_alias *new;

	if ((new =
		(t_help_option_alias*)malloc(sizeof(t_help_option_alias))) == NULL)
		return (NULL);
	ft_bzero(new, sizeof(t_help_option_alias));
	new->key = opt->key;
	new->name = opt->name;
	new->flags = opt->flags & (FT_ARGP_OPTION_NO_USAGE | FT_ARGP_OPTION_HIDDEN);
	return (new);
}

void				insert_help_option_alias(t_help_option_alias **head,
	t_help_option_alias *to_insert)
{
	t_help_option_alias *node;

	if (*head == NULL)
	{
		*head = to_insert;
		return ;
	}
	node = *head;
	while (node->next)
		node = node->next;
	node->next = to_insert;
}
