/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   insert_opt_in_group.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/02 15:11:36 by fbenneto          #+#    #+#             */
/*   Updated: 2019/12/02 15:11:49 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/argp/define.h"
#include "libft/argp/internal/help_internal.h"

t_help_option	*insert_opt_in_group(t_help_option_group *group,
	const t_argp_option *option)
{
	t_help_option		*opt_group;
	t_help_option_alias	*opt;
	t_help_doc			*doc;

	if ((option->flags & FT_ARGP_OPTION_DOC))
	{
		if (!(doc = new_help_doc(option)))
			return (NULL);
		insert_help_doc(&group->docs, doc);
		return ((t_help_option*)1);
	}
	else if ((opt = new_option_alias(option)) == NULL)
		return (NULL);
	if ((option->flags & FT_ARGP_OPTION_ALIAS))
	{
		if ((opt_group = get_last_options(group->options)) == NULL)
			return (NULL);
	}
	else if ((opt_group = new_option_group(group, option)) == NULL)
		return (NULL);
	insert_help_option_alias(&opt_group->alias, opt);
	return (opt_group);
}
