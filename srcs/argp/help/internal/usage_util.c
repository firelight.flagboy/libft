/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   usage_util.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/10 09:17:11 by fbenneto          #+#    #+#             */
/*   Updated: 2020/03/10 10:05:30 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/string.h"
#include "libft/type.h"

#include "libft/argp/internal/usage_util.h"

size_t	usage_nb_level(const t_argp *argp)
{
	size_t				levels;
	const t_argp_child	*child;
	const char			*s;

	child = argp->children;
	levels = 0;
	if (argp->args_doc)
	{
		levels++;
		s = argp->args_doc;
		while ((s = ft_strchr(s, '\n')))
		{
			s += ft_strspn(s, "\n ");
			if (*s == 0)
				break ;
			levels++;
		}
	}
	if (child)
		while (child->argp)
			levels += usage_nb_level((child++)->argp);
	return (levels);
}

char	*get_next_args_doc(const t_argp *argp, char **saved_arg_doc,
	size_t *curr_level, size_t level)
{
	const t_argp_child	*child;
	const char			*s;

	if (*saved_arg_doc)
	{
		s = *saved_arg_doc;
		s += ft_strspn(s, "\n ");
		*saved_arg_doc = ft_strchr(s, '\n');
		return ((char*)s);
	}
	if (argp->args_doc && (s = print_doc(argp->args_doc, curr_level, level,
		saved_arg_doc)))
		return ((char*)s);
	child = argp->children;
	if (child)
		while (child->argp)
		{
			s = get_next_args_doc((child++)->argp, saved_arg_doc,
				curr_level, level);
			if (*curr_level > level)
				return ((char*)s);
		}
	return (NULL);
}

int		should_print_long_usage(const t_help_option_alias *alias)
{
	return (!(alias->flags & (FT_ARGP_OPTION_NO_USAGE | FT_ARGP_OPTION_HIDDEN))
			&& ft_isprint(alias->key));
}

int		group_have_option(const t_help_option_group *group)
{
	while (group)
	{
		if (group->options)
			return (1);
		group = group->next;
	}
	return (0);
}
