/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   usage_utils_print.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/10 09:22:14 by fbenneto          #+#    #+#             */
/*   Updated: 2020/03/10 10:00:49 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/string.h"

#include "libft/argp/internal/usage_util.h"

void	print_start_usage(t_fmt *fmt, const char *name,
	int first_pattern)
{
	if (first_pattern)
		fmt_printf(fmt, "Usage: %s", name);
	else
		fmt_printf(fmt, "   or: %s", name);
}

char	*print_usage_docs(const char *s, size_t *curr_level, size_t level,
	char **saved_arg_doc)
{
	*curr_level = *curr_level + 1;
	if (*curr_level > level)
	{
		*saved_arg_doc = ft_strchr(s, '\n');
		return ((char*)s);
	}
	while ((s = ft_strchr(s, '\n')))
	{
		s += ft_strspn(s, "\n ");
		*curr_level = *curr_level + 1;
		if (*curr_level > level)
		{
			*saved_arg_doc = ft_strchr(s, '\n');
			return ((char*)s);
		}
	}
	return (0);
}
