/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   help_option.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/02 15:05:38 by fbenneto          #+#    #+#             */
/*   Updated: 2019/12/02 15:13:18 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft/mem.h"

#include "libft/argp/internal/help_internal.h"

t_help_option	*get_last_options(t_help_option *list)
{
	if (list == NULL)
		return (NULL);
	while (list->next)
		list = list->next;
	return (list);
}

t_help_option	*new_option_group(t_help_option_group *group,
	const t_argp_option *option)
{
	t_help_option *new;

	if ((new = (t_help_option*)malloc(sizeof(t_help_option))) == NULL)
		return (NULL);
	ft_bzero(new, sizeof(t_help_option));
	new->doc = option->doc;
	new->metavar = option->metavar;
	new->flags = option->flags;
	insert_help_option(&group->options, new);
	return (new);
}

void			insert_help_option(t_help_option **head,
	t_help_option *to_insert)
{
	t_help_option *node;

	if (*head == NULL)
	{
		*head = to_insert;
		return ;
	}
	node = *head;
	while (node->next)
		node = node->next;
	node->next = to_insert;
}
