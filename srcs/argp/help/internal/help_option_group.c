/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   help_option_group.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/02 15:05:38 by fbenneto          #+#    #+#             */
/*   Updated: 2019/12/02 15:14:32 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft/mem.h"

#include "libft/argp/internal/help_internal.h"

t_help_option_group	*new_help_group(t_help_state *state, int group)
{
	t_help_option_group *new;

	if ((new =
		(t_help_option_group*)malloc(sizeof(t_help_option_group))) == NULL)
		return (NULL);
	ft_bzero(new, sizeof(t_help_option_group));
	new->id = group;
	insert_new_group(&state->group_head, new);
	return (new);
}

void				insert_new_group(t_help_option_group **head,
	t_help_option_group *to_insert)
{
	t_help_option_group *node;

	if (*head == NULL)
	{
		*head = to_insert;
		return ;
	}
	node = *head;
	while (node->next)
		node = node->next;
	node->next = to_insert;
	to_insert->parent = node;
}

t_help_option_group	*find_help_group(t_help_option_group *list, int id)
{
	while (list)
	{
		if (list->id == id)
			return (list);
		list = list->next;
	}
	return (NULL);
}
