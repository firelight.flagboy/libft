/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   insert_argp_option.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/02 15:03:27 by fbenneto          #+#    #+#             */
/*   Updated: 2019/12/03 13:35:29 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <errno.h>

#include "libft/argp.h"
#include "libft/argp/define.h"
#include "libft/argp/utils.h"
#include "libft/argp/internal/help_internal.h"

static void	insert_doc_in_state(const t_argp_option *opt, t_help_state *state)
{
	t_help_doc *doc;

	if (!(doc = new_help_doc(opt)))
		ft_argp_failure(state->state, 1, errno, "can't create doc");
	insert_help_doc(&state->docs, doc);
}

void		insert_argp_opt(const t_argp_option *o, t_help_state *st,
	int *curr_grp)
{
	t_help_option_group *grp;

	if (o->group)
		*curr_grp = o->group;
	if (option_is_group_header(o) && (grp = new_help_group(st, *curr_grp + 1)))
	{
		*curr_grp = *curr_grp + 1;
		grp->header = o->doc;
		return ;
	}
	else if ((o->flags & FT_ARGP_OPTION_DOC) && o->name && o->name[0] == '-')
		return (insert_doc_in_state(o, st));
	else if ((grp = find_help_group(st->group_head, *curr_grp)) == NULL
		&& (grp = new_help_group(st, *curr_grp)) == NULL)
		ft_argp_failure(st->state, 1, errno, "can't create new group");
	if (insert_opt_in_group(grp, o) == NULL)
		ft_argp_failure(st->state, 1, errno, "can't insert option in group");
}
