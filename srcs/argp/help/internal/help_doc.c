/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   help_doc.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/02 15:05:38 by fbenneto          #+#    #+#             */
/*   Updated: 2019/12/02 15:14:46 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft/mem.h"
#include "libft/string.h"

#include "libft/argp/internal/help_internal.h"

void		insert_help_doc(t_help_doc **head, t_help_doc *to_insert)
{
	t_help_doc *node;

	if (*head == NULL)
	{
		*head = to_insert;
		return ;
	}
	node = *head;
	while (node->next)
		node = node->next;
	node->next = to_insert;
}

t_help_doc	*new_help_doc(const t_argp_option *doc)
{
	t_help_doc *new;

	if (!(new = (t_help_doc*)malloc(sizeof(t_help_doc))))
		return (NULL);
	ft_bzero(new, sizeof(t_help_doc));
	new->name = doc->name + ft_strspn(doc->name, "-");
	new->content = doc->doc;
	return (new);
}
