/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_opt.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/28 13:22:16 by fbenneto          #+#    #+#             */
/*   Updated: 2019/12/02 09:18:43 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/string.h"

#include "libft/argp/next.h"
#include "libft/argp/define.h"
#include "libft/argp/internal/define.h"
#include "libft/argp/utils.h"
#include "libft/argp.h"

#define NOT_RECONIZED "Option should have been recognized!?"

static int	parse_short_opt(t_parser *parser, int opt)
{
	t_group	*group;
	char	*short_index;

	group = parser->groups;
	if ((short_index = ft_strchr(parser->short_opts, opt)))
	{
		while (group < parser->end_group)
		{
			if (group->short_end > short_index)
			{
				return (ft_argp_group_parse(group, &parser->state, opt,
					parser->opt_data.optarg));
			}
			group++;
		}
	}
	return (EBADKEY);
}

int			ft_argp_parser_parse_opt(t_parser *parser, int opt, const char *arg)
{
	int				group_key;
	int				err;
	const t_option	*option;

	(void)arg;
	err = EBADKEY;
	if ((group_key = opt >> 24) == 0)
		err = parse_short_opt(parser, opt);
	else
		err = ft_argp_group_parse(parser->groups + group_key - 1,
			&parser->state, opt & 0x00ffffff, parser->opt_data.optarg);
	if (err == EBADKEY)
	{
		if (group_key == 0)
			ft_argp_error(&parser->state, "-%c: %s", opt, NOT_RECONIZED);
		else
		{
			option = parser->long_opts;
			while (option->val != opt && option->name)
				option++;
			ft_argp_error(&parser->state, "--%s: %s",
				(option->name ? option->name : "???"), NOT_RECONIZED);
		}
	}
	return (err);
}
