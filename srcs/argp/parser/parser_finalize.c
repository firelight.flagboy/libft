/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser_finalize.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/28 14:48:33 by fbenneto          #+#    #+#             */
/*   Updated: 2019/11/29 15:52:48 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <errno.h>

#include "libft/printf.h"

#include "libft/argp.h"
#include "libft/argp/finalize.h"
#include "libft/argp/internal/define.h"
#include "libft/argp/utils.h"

static int	handle_end_of_arg(t_parser *parser, int *arg_index)
{
	t_group	*group;
	int		err;

	err = 0;
	group = parser->groups;
	while (group < parser->end_group && (!err || err == EBADKEY))
	{
		if (group->args_processed == 0)
			err = ft_argp_group_parse(group, &parser->state,
				FT_ARGP_KEY_NO_ARGS, 0);
		group++;
	}
	group = parser->end_group - 1;
	while (group >= parser->groups && (!err || err == EBADKEY))
		err = ft_argp_group_parse(group--, &parser->state, FT_ARGP_KEY_END, 0);
	if (err == EBADKEY)
		err = 0;
	if (arg_index)
		*arg_index = parser->state.next;
	return (err);
}

static int	parser_finalize_arg(t_parser *parser, int *arg_index)
{
	int		err;

	err = 0;
	if (parser->state.next == parser->state.argc)
		err = handle_end_of_arg(parser, arg_index);
	else if (arg_index)
		*arg_index = parser->state.next;
	else
	{
		if (!(parser->state.flags & FT_ARGP_NO_ERRS)
			&& parser->state.err_stream)
			ft_fprintf(parser->state.err_stream,
				"%s: Too many arguments\n", parser->state.name);
		err = EBADKEY;
	}
	return (err);
}

static void	handle_err(t_parser *parser, int err)
{
	t_group *group;

	group = parser->end_group - 1;
	if (err == EBADKEY)
		ft_argp_state_help(&parser->state, parser->state.err_stream,
			FT_ARGP_HELP_STD_HELP);
	while (group >= parser->groups)
		ft_argp_group_parse(group--, &parser->state, FT_ARGP_KEY_ERROR, 0);
}

int			ft_argp_parser_finalize(
	t_parser *parser, int err, int arg_badkey, int *arg_index)
{
	t_group *group;

	if (err == EBADKEY && arg_badkey)
		err = 0;
	if (!err)
		err = parser_finalize_arg(parser, arg_index);
	if (err)
		handle_err(parser, err);
	else
	{
		group = parser->end_group - 1;
		while (group >= parser->groups)
			err = ft_argp_group_parse(group--, &parser->state,
				FT_ARGP_KEY_SUCCESS, 0);
		if (err == EBADKEY)
			err = 0;
	}
	group = parser->end_group - 1;
	while (group >= parser->groups)
		ft_argp_group_parse(group--, &parser->state, FT_ARGP_KEY_FINI, 0);
	if (err == EBADKEY)
		err = EINVAL;
	free(parser->storage);
	return (err);
}
