/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_next.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/28 11:02:20 by fbenneto          #+#    #+#             */
/*   Updated: 2019/11/29 15:45:15 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/getopt.h"
#include "libft/string.h"

#include "libft/argp/next.h"
#include "libft/argp/define.h"
#include "libft/argp/internal/define.h"

#define KEY_END -1
#define KEY_ARG 1
#define KEY_ERR '?'
#define QUOTE "--"

static int	handle_end_opt(t_parser *parser, int *opt, int *arg_badkey)
{
	if (parser->state.next >= parser->state.argc
		|| (parser->state.flags & FT_ARGP_NO_ARGS))
	{
		*arg_badkey = 1;
		return (EBADKEY);
	}
	*opt = KEY_ARG;
	parser->opt_data.optarg = parser->state.argv[parser->state.next++];
	return (0);
}

static int	handle_getopt(t_parser *parser, int *opt, int *arg_badkey)
{
	parser->opt_data.optind = parser->state.next;
	parser->opt_data.optopt = KEY_END;
	if (parser->state.flags & FT_ARGP_LONG_ONLY)
		*opt = ft_getopt_long_only_r(&parser->opt_data);
	else
		*opt = ft_getopt_long_r(&parser->opt_data);
	parser->state.next = parser->opt_data.optind;
	if (*opt == KEY_END)
	{
		parser->try_getopt = 0;
		if (parser->state.next > 1 && ft_strcmp(
			parser->state.argv[parser->state.next - 1], QUOTE) == 0)
			parser->state.quoted = parser->state.next;
	}
	else if (*opt == KEY_ERR && parser->opt_data.optopt != KEY_END)
	{
		*arg_badkey = 0;
		return (EBADKEY);
	}
	return (0);
}

int			ft_argp_parse_next(t_parser *parser, int *arg_badkey)
{
	int opt;
	int err;

	err = 0;
	if (parser->state.quoted && parser->state.next < parser->state.quoted)
		parser->state.quoted = 0;
	if (parser->try_getopt && !parser->state.quoted)
	{
		if ((err = handle_getopt(parser, &opt, arg_badkey))
			== EBADKEY)
			return (EBADKEY);
	}
	else
		opt = KEY_END;
	if (opt == KEY_END && (err = handle_end_opt(parser, &opt, arg_badkey))
		== EBADKEY)
		return (EBADKEY);
	if (opt == KEY_ARG)
		err = ft_argp_parser_parse_arg(parser, parser->opt_data.optarg);
	else
		err = ft_argp_parser_parse_opt(parser, opt, parser->opt_data.optarg);
	if (err == EBADKEY)
		*arg_badkey = (opt == KEY_END || opt == KEY_ARG);
	return (err);
}
