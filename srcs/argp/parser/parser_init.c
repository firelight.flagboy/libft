/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser_init.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/27 10:31:03 by fbenneto          #+#    #+#             */
/*   Updated: 2020/03/10 09:13:10 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <errno.h>

#include "libft/getopt.h"
#include "libft/mem.h"
#include "libft/string.h"

#include "libft/argp/init.h"
#include "libft/argp/global.h"
#include "libft/argp/define.h"
#include "libft/argp/internal/define.h"
#include "libft/argp/utils.h"

static void		parser_convert(
	t_parser *parser, const t_argp *argp, unsigned flags)
{
	t_parser_convert_state state;

	state.parser = parser;
	state.short_end = parser->short_opts;
	state.long_end = (t_option*)parser->long_opts;
	state.child_inputs_end = parser->child_inputs;
	state.parent_index = 0;
	if (flags & FT_ARGP_IN_ORDER)
		*state.short_end++ = '-';
	else if (flags & FT_ARGP_NO_ARGS)
		*state.short_end++ = '+';
	*state.short_end = 0;
	state.long_end->name = NULL;
	parser->argp = argp;
	if (argp)
		parser->end_group = convert_options(argp, NULL, parser->groups, &state);
	else
		parser->end_group = parser->groups;
}

static void		init_parser_group(t_parser *parser, int *err)
{
	t_group *group;

	group = parser->groups;
	while (group < parser->end_group && (!*err || *err == EBADKEY))
	{
		if (group->parent)
			group->input = group->parent->child_inputs[group->parent_index];
		if (!group->parser
			&& group->argp->children && group->argp->children->argp)
			group->child_inputs[0] = group->input;
		*err = ft_argp_group_parse(group, &parser->state, FT_ARGP_KEY_INIT, 0);
		group++;
	}
}

static void		parser_setup_state(
	t_parser *parser, char *const *argv, unsigned flags)
{
	char	*short_name;

	(void)flags;
	if (parser->state.flags & FT_ARGP_NO_ERRS)
	{
		parser->opt_data.opterr = 0;
		if (parser->state.flags & FT_ARGP_PARSE_ARGV0)
		{
			parser->state.argv--;
			parser->state.argc++;
		}
	}
	else
		parser->opt_data.opterr = 1;
	if (parser->state.argv == argv && argv[0])
	{
		short_name = ft_strrchr(argv[0], '/');
		parser->state.name = short_name ? short_name + 1 : argv[0];
	}
	else
		parser->state.name = g_argp_program_invocation_name;
	if (parser->state.name == NULL)
		parser->state.name = FT_ARGP_MISS_INVOCATION_NAME;
}

static void		parser_setup_getopt_data(t_parser *parser, t_arg *arg,
	t_parser_data *data)
{
	(void)arg;
	(void)data;
	parser->opt_data.argc = parser->state.argc;
	parser->opt_data.argv = parser->state.argv;
	parser->opt_data.optstring = parser->short_opts;
	parser->opt_data.longopts = parser->long_opts;
}

int				ft_argp_parser_init(
	t_parser *parser, const t_argp *argp, t_arg *arg, t_parser_data *data)
{
	int		err;

	err = 0;
	if ((parser->storage = parser_init_struct(argp, data, parser))
		== NULL)
		return (ENOMEM);
	parser_convert(parser, argp, data->flags);
	parser->state.root_argp = parser->argp;
	parser->state.argc = arg->argc;
	parser->state.argv = arg->argv;
	parser->state.flags = data->flags;
	parser->state.err_stream = stderr;
	parser->state.out_stream = stdout;
	parser->state.next = 0;
	parser->state.pstate = parser;
	parser->try_getopt = 1;
	if (parser->groups < parser->end_group)
		parser->groups->input = data->input;
	init_parser_group(parser, &err);
	if (err && err != EBADKEY)
		return (err);
	parser_setup_state(parser, arg->argv, data->flags);
	parser_setup_getopt_data(parser, arg, data);
	return (0);
}
