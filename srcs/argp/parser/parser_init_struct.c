/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser_init_struct.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/10 09:13:21 by fbenneto          #+#    #+#             */
/*   Updated: 2020/03/10 09:14:57 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <errno.h>

#include "libft/getopt.h"
#include "libft/mem.h"
#include "libft/string.h"

#include "libft/argp/init.h"
#include "libft/argp/global.h"
#include "libft/argp/define.h"
#include "libft/argp/internal/define.h"
#include "libft/argp/utils.h"

void		*parser_init_struct(
	const t_argp *argp, t_parser_data *data, t_parser *parser)
{
	t_parser_sizes	sizes;

	ft_memcpy(&parser->opt_data,
		&(t_getopt_data){ .optind = 1, .opterr = 1 }, sizeof(t_getopt_data));
	ft_bzero(&sizes, sizeof(t_parser_sizes));
	sizes.short_opt_len = (data->flags & FT_ARGP_NO_ARGS) ? 0 : 1;
	if (argp)
		ft_argp_calc_sizes(argp, &sizes);
	if ((parser->storage = malloc((sizes.num_groups + 1) * sizeof(t_group)
		+ sizes.num_child_inputs * sizeof(void *) + (sizes.long_opt_len + 1)
		* sizeof(t_option) + (sizes.short_opt_len + 1))) == NULL)
		return (NULL);
	parser->groups = parser->storage;
	parser->child_inputs = (void*)parser->groups
		+ (sizes.num_groups + 1) * sizeof(t_group);
	parser->long_opts = (void*)parser->child_inputs
		+ (sizes.num_child_inputs) * sizeof(void *);
	parser->short_opts = (void*)parser->long_opts
		+ (sizes.long_opt_len + 1) * sizeof(t_option);
	ft_bzero(parser->child_inputs, sizes.num_child_inputs * sizeof(void*));
	ft_bzero(&parser->state, sizeof(t_argp_state));
	return (parser->storage);
}
