/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_arg.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/28 12:43:07 by fbenneto          #+#    #+#             */
/*   Updated: 2019/11/29 15:44:44 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/argp/next.h"
#include "libft/argp/define.h"
#include "libft/argp/internal/define.h"
#include "libft/argp/utils.h"

static t_group	*parse_arg_loop(
	t_parser *parser, int *key, int *err, const char *arg)
{
	t_group *group;

	group = parser->groups;
	*key = 0;
	while (group < parser->end_group && *err == EBADKEY)
	{
		parser->state.next++;
		*key = FT_ARGP_KEY_ARG;
		*err = ft_argp_group_parse(group, &parser->state, *key, arg);
		if (*err == EBADKEY)
		{
			parser->state.next--;
			*key = FT_ARGP_KEY_ARGS;
			*err = ft_argp_group_parse(group, &parser->state, *key, 0);
		}
		group++;
	}
	return (group);
}

int				ft_argp_parser_parse_arg(t_parser *parser, const char *arg)
{
	int		index;
	int		err;
	t_group	*group;
	int		key;

	index = --parser->state.next;
	err = EBADKEY;
	group = parse_arg_loop(parser, &key, &err, arg);
	if (!err)
	{
		if (key == FT_ARGP_KEY_ARGS)
			parser->state.next = parser->state.argc;
		if (parser->state.next > index)
			(--group)->args_processed += (parser->state.next - index);
		else
			parser->try_getopt = 1;
	}
	return (err);
}
