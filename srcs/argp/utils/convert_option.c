/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   convert_option.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/27 16:00:17 by fbenneto          #+#    #+#             */
/*   Updated: 2019/12/02 08:32:30 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/argp/define.h"
#include "libft/argp/struct.h"
#include "libft/argp/internal/struct.h"
#include "libft/argp/utils.h"

static void		convert_argp_long_options(t_parser_convert_state *state,
	const t_argp_option *opt, const t_argp_option *real, t_group *group)
{
	state->long_end->name = opt->name;
	state->long_end->has_arg = no_argument;
	if (real->metavar)
	{
		state->long_end->has_arg = required_argument;
		if (real->flags & FT_ARGP_OPTION_ARG_OPTIONAL)
			state->long_end->has_arg = optional_argument;
	}
	state->long_end->flag = 0;
	state->long_end->val = \
		((opt->key ? opt->key : real->key) & 0x00ffffff)
		+ (((group - state->parser->groups) + 1) << 24);
	(++state->long_end)->name = NULL;
}

static void		convert_argp_options(
	const t_argp_option *real,
	t_parser_convert_state *state, t_group *group)
{
	const t_argp_option	*o = real;

	while (!option_is_end(o))
	{
		if (!(o->flags & FT_ARGP_OPTION_ALIAS))
			real = o;
		if (!(real->flags & FT_ARGP_OPTION_DOC))
		{
			if (option_is_short(o))
			{
				*state->short_end++ = o->key;
				if (real->metavar)
				{
					*state->short_end++ = ':';
					if (real->flags & FT_ARGP_OPTION_ARG_OPTIONAL)
						*state->short_end++ = ':';
				}
				*state->short_end = 0;
			}
			if (o->name
				&& argp_find_long_option(state->parser->long_opts, o->name) < 0)
				convert_argp_long_options(state, o, real, group);
		}
		o++;
	}
}

static void		convert_init_group(
	t_group *group, const t_argp *argp,
	t_group *parent, const t_parser_convert_state *state)
{
	group->parser = argp->parser;
	group->argp = argp;
	group->short_end = state->short_end;
	group->args_processed = 0;
	group->parent = parent;
	group->parent_index = state->parent_index;
	group->input = 0;
	group->hook = 0;
	group->child_inputs = 0;
}

static void		convert_prepare_child_inputs(t_group *group,
	const t_argp_child *child, t_parser_convert_state *state)
{
	unsigned			num_children;

	num_children = 0;
	while (child[num_children].argp)
		num_children++;
	group->child_inputs = state->child_inputs_end;
	state->child_inputs_end += num_children;
}

t_group			*convert_options(
	const t_argp *argp, t_group *parent, t_group *group,
	t_parser_convert_state *state)
{
	const t_argp_child	*child = argp->children;
	unsigned			i;

	if (argp->options || argp->parser)
	{
		if (argp->options)
			convert_argp_options(argp->options, state, group);
		convert_init_group(group, argp, parent, state);
		if (child)
			convert_prepare_child_inputs(group, child, state);
		parent = group++;
	}
	else
		parent = NULL;
	if (child)
	{
		i = 0;
		while (child->argp)
		{
			state->parent_index = i++;
			group = convert_options(child++->argp, parent, group, state);
		}
	}
	return (group);
}
