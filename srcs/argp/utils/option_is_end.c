/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   option_is_end.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/27 11:43:15 by fbenneto          #+#    #+#             */
/*   Updated: 2019/12/02 08:28:41 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/argp/utils.h"

int	option_is_end(const t_argp_option *opt)
{
	return (!opt->key && !opt->name && !opt->doc && !opt->group);
}
