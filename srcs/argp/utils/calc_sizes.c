/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   calc_sizes.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/27 11:14:36 by fbenneto          #+#    #+#             */
/*   Updated: 2019/12/02 08:34:20 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/argp/internal/struct.h"
#include "libft/argp/utils.h"

static void	calc_sizes_opt(const t_argp_option *opt, t_parser_sizes *sizes)
{
	int	num_opts;

	num_opts = 0;
	while (!option_is_end(opt++))
		num_opts++;
	sizes->short_opt_len += 3 * num_opts;
	sizes->long_opt_len += num_opts;
}

void		ft_argp_calc_sizes(const t_argp *argp, t_parser_sizes *sizes)
{
	const t_argp_child	*child;
	const t_argp_option	*opt;

	opt = argp->options;
	child = argp->children;
	if (opt || argp->parser)
	{
		sizes->num_groups++;
		if (opt)
			calc_sizes_opt(opt, sizes);
	}
	if (child)
		while (child->argp)
		{
			ft_argp_calc_sizes((child++)->argp, sizes);
			sizes->num_child_inputs++;
		}
}
