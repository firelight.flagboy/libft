/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   option_is_group_header.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/02 10:22:02 by fbenneto          #+#    #+#             */
/*   Updated: 2019/12/02 10:22:42 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/argp/utils.h"

int	option_is_group_header(const t_argp_option *opt)
{
	return (opt->name == 0 && opt->key == 0);
}
