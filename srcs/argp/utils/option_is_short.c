/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   option_is_short.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/27 11:46:02 by fbenneto          #+#    #+#             */
/*   Updated: 2019/11/29 14:44:39 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/type.h"

#include "libft/argp/define.h"
#include "libft/argp/utils.h"

int		option_is_short(const t_argp_option *opt)
{
	if (opt->flags & FT_ARGP_OPTION_DOC)
		return (0);
	return (ft_isprint(opt->key));
}
