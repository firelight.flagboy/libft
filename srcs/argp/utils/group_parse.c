/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   group_parse.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/28 13:11:38 by fbenneto          #+#    #+#             */
/*   Updated: 2019/11/29 15:42:39 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/argp/utils.h"
#include "libft/argp/internal/define.h"
#include "libft/argp/define.h"

int	ft_argp_group_parse(t_group *group, t_argp_state *state, int key,
	const char *arg)
{
	int err;

	if (group->parser)
	{
		state->hook = group->hook;
		state->input = group->input;
		state->child_inputs = group->child_inputs;
		state->arg_num = group->args_processed;
		err = group->parser(key, arg, state);
		group->hook = state->hook;
		return (err);
	}
	return (EBADKEY);
}
