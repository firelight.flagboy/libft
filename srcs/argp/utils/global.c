/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   global.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/19 09:59:11 by fbenneto          #+#    #+#             */
/*   Updated: 2019/11/29 14:39:37 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/argp/global.h"
#include "libft/argp/define.h"
#include <sysexits.h>

int __attribute__((weak))			g_argp_err_exit_status = EX_USAGE;
const char __attribute__((weak))	*g_argp_program_version = 0;
const char __attribute__((weak))	*g_argp_program_bug_address = 0;
const char __attribute__((weak))	*g_argp_program_invocation_name = \
FT_ARGP_MISS_INVOCATION_NAME;

t_program_version_hook __attribute__((weak))	g_argp_program_version_hook = 0;
