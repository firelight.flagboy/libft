/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   find_long_option.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/27 15:08:36 by fbenneto          #+#    #+#             */
/*   Updated: 2019/12/02 08:33:06 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/string.h"
#include "libft/argp/utils.h"

int	argp_find_long_option(const t_option *options, const char *name)
{
	const t_option *opt = options;

	while (opt->name != NULL)
	{
		if (ft_strcmp(opt->name, name) == 0)
			return (opt - options);
		opt++;
	}
	return (-1);
}
