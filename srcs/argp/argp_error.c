/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   argp_error.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/18 15:02:58 by fbenneto          #+#    #+#             */
/*   Updated: 2019/11/29 14:39:19 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/argp.h"
#include "libft/printf.h"
#include <stdarg.h>
#include <stdlib.h>
#include <errno.h>

void	ft_argp_error(const t_argp_state *state, const char *fmt, ...)
{
	va_list	ap;
	FILE	*stream;
	char	*s;

	if (state && (state->flags & FT_ARGP_NO_ERRS))
		return ;
	stream = state ? state->err_stream : stderr;
	if (stream)
	{
		va_start(ap, fmt);
		if (ft_vasprintf(&s, fmt, ap) == -1)
		{
			va_end(ap);
			return (ft_argp_failure(state, 1, errno,
				"argp_error: vasprintf failed\n"));
		}
		va_end(ap);
		ft_fprintf(state->err_stream, "%s: %s\n",
			state ? state->name : g_argp_program_invocation_name, s);
		free(s);
		ft_argp_state_help(state, stream, FT_ARGP_HELP_STD_ERR);
	}
}
