/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   argp_parse.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/18 15:02:58 by fbenneto          #+#    #+#             */
/*   Updated: 2019/12/03 15:54:34 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/mem.h"
#include "libft/printf.h"

#include "libft/argp.h"
#include "libft/argp/define.h"
#include "libft/argp/internal/define.h"
#include "libft/argp/internal/struct.h"

#include "libft/argp/init.h"
#include "libft/argp/next.h"
#include "libft/argp/finalize.h"

#define FT_ARGP_HELP_OPT_KEY 0xabcd
#define FT_ARGP_VERSION_OPT_KEY 0xbcde
#define FT_ARGP_USAGE_OPT_KEY 0xcdef

static const t_argp_option	g_argp_default_option[] = {
	{ "help", FT_ARGP_HELP_OPT_KEY, 0, 0, "Show long usage message", -1 },
	{ "usage", FT_ARGP_USAGE_OPT_KEY, 0, 0, "Show short usage message", 0 },
	{ 0 }
};

static const t_argp_option	g_argp_version_option[] = {
	{ "version", FT_ARGP_VERSION_OPT_KEY, 0, 0, "Print program version", -1 },
	{ 0 }
};

static int		default_parser(int key, const char *arg, t_argp_state *state)
{
	(void)arg;
	if (key == FT_ARGP_HELP_OPT_KEY)
		ft_argp_state_help(state, state->out_stream, FT_ARGP_HELP_STD_HELP);
	else if (key == FT_ARGP_USAGE_OPT_KEY)
		ft_argp_state_help(state, state->out_stream,
			FT_ARGP_HELP_USAGE | FT_ARGP_HELP_EXIT_OK);
	else
		return (EBADKEY);
	return (0);
}

static int		version_parser(int key, const char *arg, t_argp_state *state)
{
	(void)arg;
	if (key == FT_ARGP_VERSION_OPT_KEY)
	{
		if (g_argp_program_version_hook)
			g_argp_program_version_hook(state->out_stream, state);
		else if (g_argp_program_version)
			ft_fprintf(state->out_stream, "%s\n", g_argp_program_version);
		else
			ft_argp_error(state, "No version know !?");
		if (!(state->flags & FT_ARGP_NO_EXIT))
			exit(0);
		return (0);
	}
	return (EBADKEY);
}

static const t_argp g_argp_default_parser = {
	g_argp_default_option, &default_parser, 0, 0, 0, 0 };
static const t_argp g_argp_version_parser = {
	g_argp_version_option, &version_parser, 0, 0, 0, 0 };

static t_argp	*init_default_parser(t_argp *top_argp,
	t_argp_child *children, const t_argp *argp)
{
	ft_bzero(top_argp, sizeof(t_argp));
	ft_bzero(children, 4 * sizeof(t_argp_child));
	top_argp->children = children;
	if (argp)
		(children++)->argp = argp;
	(children++)->argp = &g_argp_default_parser;
	if (g_argp_program_version || g_argp_program_version_hook)
		(children++)->argp = &g_argp_version_parser;
	children->argp = 0;
	return (top_argp);
}

int				ft_argp_parse(
	const t_argp *argp, t_arg *arg, unsigned flags, t_argp_data *data)
{
	int					err;
	t_argp				top_argp;
	t_argp_child		child[4];
	int					arg_badkey;
	t_parser			parser;

	if ((flags & FT_ARGP_NO_HELP) == 0)
	{
		argp = init_default_parser(&top_argp, child, argp);
	}
	err = ft_argp_parser_init(&parser, argp, arg,
		&(t_parser_data){ flags, data->arg_index, data->input });
	if (err == 0)
	{
		while (err == 0)
			err = ft_argp_parse_next(&parser, &arg_badkey);
		err = ft_argp_parser_finalize(
			&parser, err, arg_badkey, data->arg_index);
	}
	return (err);
}
