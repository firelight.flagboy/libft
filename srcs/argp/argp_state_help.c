/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   argp_state_help.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/18 15:02:58 by fbenneto          #+#    #+#             */
/*   Updated: 2019/11/29 14:39:19 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/argp.h"
#include "libft/argp/help.h"
#include <stdlib.h>

void	ft_argp_state_help(
	const t_argp_state *state, FILE *stream, unsigned flags)
{
	if ((state && (state->flags & FT_ARGP_NO_ERRS)) || stream == NULL)
		return ;
	if (state && state->flags & FT_ARGP_LONG_ONLY)
		flags |= FT_ARGP_HELP_LONG_ONLY;
	ft_argp_help_internal(state ? state->root_argp : 0, state, stream,
		&(t_help_data){flags,
		state ? state->name : g_argp_program_invocation_name });
	if (!state || (state->flags & FT_ARGP_NO_EXIT) == 0)
	{
		if ((flags & FT_ARGP_HELP_EXIT_ERR))
			exit(g_argp_err_exit_status);
		if ((flags & FT_ARGP_HELP_EXIT_OK))
			exit(0);
	}
}
