/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   argp_help.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/18 15:02:58 by fbenneto          #+#    #+#             */
/*   Updated: 2019/11/29 11:12:21 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/argp.h"
#include "libft/argp/help.h"

void	ft_argp_help(
	const t_argp *argp, FILE *stream, unsigned flags, char *name)
{
	ft_argp_help_internal(argp, 0, stream, &(t_help_data){ flags, name });
}
