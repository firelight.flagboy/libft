MAKELIB_PATH := makelib

NAME := libft.a

C_SRCS := \
	conv/ft_atoi \
	conv/ft_atol \
	conv/ft_atoll \
	conv/ft_itoa \
	\
	input/ft_read_file \
	\
	list/ft_lstadd \
	list/ft_lstdel_for_cleanup \
	list/ft_lstdel \
	list/ft_lstdelone \
	list/ft_lstfree_content \
	list/ft_lstiter \
	list/ft_lstlast \
	list/ft_lstlen \
	list/ft_lstmap \
	list/ft_lstnew_dup \
	list/ft_lstnew \
	list/ft_lstpush_back \
	list/ft_lstpush_front \
	list/ft_lstsort_merge \
	\
	mem/ft_bzero \
	mem/ft_memalloc \
	mem/ft_memccpy \
	mem/ft_memchr \
	mem/ft_memcmp \
	mem/ft_memcpy \
	mem/ft_memdel \
	mem/ft_memmove \
	mem/ft_memset \
	\
	num/ft_len_int \
	num/ft_len_uint \
	num/ft_lendigit \
	num/ft_get_hex_letter \
	\
	output/ft_putchar \
	output/ft_putchar_fd \
	output/ft_putendl \
	output/ft_putendl_fd \
	output/ft_putnbr \
	output/ft_putnbr_fd \
	output/ft_putstr \
	output/ft_putstr_fd \
	\
	string/ft_count_word \
	string/ft_realloc_str \
	string/ft_str_skip_char \
	string/ft_str_skip_char_f \
	string/ft_strcat \
	string/ft_strchr \
	string/ft_strclr \
	string/ft_strcmp \
	string/ft_strcpy \
	string/ft_strdel \
	string/ft_strdup \
	string/ft_strequ \
	string/ft_striter \
	string/ft_striteri \
	string/ft_strjoin \
	string/ft_strlcat \
	string/ft_strlen \
	string/ft_strmap \
	string/ft_strmapi \
	string/ft_strncat \
	string/ft_strncmp \
	string/ft_strncpy \
	string/ft_strndup \
	string/ft_strnequ \
	string/ft_strnew \
	string/ft_strnstr \
	string/ft_strrchr \
	string/ft_strsplit \
	string/ft_strstr \
	string/ft_strsub \
	string/ft_strtrim \
	string/ft_strspn \
	string/ft_strcspn \
	string/ft_strnlen \
	\
	type/ft_isalnum \
	type/ft_isalpha \
	type/ft_isascii \
	type/ft_isdigit \
	type/ft_isxdigit \
	type/ft_islower \
	type/ft_isprint \
	type/ft_isspace \
	type/ft_isupper \
	type/ft_tolower \
	type/ft_toupper \
	\
	printf/buffer/ft_buffer \
	printf/buffer/ft_buffer_2 \
	printf/buffer/ft_fill_buffer \
	printf/buffer/ft_putbuffer \
	\
	printf/conv/ft_adr \
	printf/conv/ft_badconv \
	printf/conv/ft_bin \
	printf/conv/ft_char \
	printf/conv/ft_fillstr \
	printf/conv/ft_hex \
	printf/conv/ft_nbr \
	printf/conv/ft_octal \
	printf/conv/ft_unbr \
	\
	printf/fill/ft_backward \
	printf/fill/ft_filldimen \
	printf/fill/ft_fillforward \
	printf/fill/ft_fillforward_annexe \
	\
	printf/flags/ft_get_flags \
	printf/flags/ft_get_flags_2 \
	\
	printf/ft_printf \
	printf/ft_dprintf \
	printf/ft_eprintf \
	printf/ft_fprintf \
	printf/ft_sprintf \
	printf/ft_snprintf \
	printf/ft_asprintf \
	printf/ft_vdprintf \
	printf/ft_vfprintf \
	printf/ft_vasprintf \
	printf/ft_vsprintf \
	printf/ft_vsnprintf \
	\
	printf/utils/ft_get_int \
	printf/utils/ft_itoa \
	printf/utils/ft_unicode \
	printf/utils/ft_singleton \
	printf/utils/type \
	printf/utils/strlen_uni \
	printf/utils/string_copy \
	\
	getopt/getopt \
	getopt/getopt_long \
	getopt/getopt_long_only \
	getopt/getopt_long_r \
	getopt/getopt_long_only_r \
	\
	getopt/init/getopt_initialize \
	\
	getopt/utils/global \
	getopt/utils/exchange \
	getopt/utils/permute \
	getopt/utils/find_option \
	\
	getopt/internal/getopt_internal \
	getopt/internal/getopt_internal_r \
	getopt/internal/next_short \
	getopt/internal/process_long_option \
	\
	argp/argp_error \
	argp/argp_failure \
	argp/argp_help \
	argp/argp_parse \
	argp/argp_state_help \
	argp/argp_usage \
	\
	argp/help/bug_addr \
	argp/help/doc \
	argp/help/long \
	argp/help/see \
	argp/help/usage \
	\
	argp/help/internal \
	argp/help/internal/help_doc \
	argp/help/internal/help_option \
	argp/help/internal/help_option_alias \
	argp/help/internal/help_option_group \
	argp/help/internal/insert_argp_option \
	argp/help/internal/insert_opt_in_group \
	\
	argp/help/fmt/check_margin \
	argp/help/fmt/clean \
	argp/help/fmt/ensure \
	argp/help/fmt/flush \
	argp/help/fmt/init \
	argp/help/fmt/new_line \
	argp/help/fmt/nputsnl \
	argp/help/fmt/printf \
	argp/help/fmt/printfnl \
	argp/help/fmt/putc \
	argp/help/fmt/puts \
	argp/help/fmt/putsnl \
	argp/help/fmt/set_left_margin \
	argp/help/fmt/write \
	\
	argp/parser/parse_arg \
	argp/parser/parse_next \
	argp/parser/parse_opt \
	argp/parser/parser_finalize \
	argp/parser/parser_init \
	\
	argp/utils/calc_sizes \
	argp/utils/convert_option \
	argp/utils/find_long_option \
	argp/utils/global \
	argp/utils/group_parse \
	argp/utils/option_is_end \
	argp/utils/option_is_short \
	argp/utils/option_is_group_header \
	argp/help/long_utils \
	argp/parser/parser_init_struct \
	argp/help/internal/usage_utils_print \
	argp/help/internal/usage_util \
	argp/help/internal/usage_util_print \
	argp/help/long_util_print \

ISLIB := 1

ISDEV := 0
include $(MAKELIB_PATH)/core.mk
