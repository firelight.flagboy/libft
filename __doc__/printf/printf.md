# Printf

module that implement `printf` function

[header](../../incs/libft/printf.h)

## Function

- [ft_printf](func/ft_printf.md): print formated data
- [ft_dprintf](func/ft_dprintf.md): print formated data in a file descriptor
- [ft_eprintf](func/ft_eprintf.md): print formated data in **stderr**
- [ft_fprintf](func/ft_fprintf.md): print formated data in a `FILE*`
- [ft_asprintf](func/ft_asprintf.md): create a new string with formated data
- [ft_sprintf](func/ft_sprintf.md): fill a current string with formated data
- [ft_snprintf](func/ft_snprintf.md): fill a current string with formated data up to `n`
- [ft_vdprintf](func/ft_vdprintf.md): print formated data in a file descriptor using `va_list`
- [ft_vfprintf](func/ft_vfprintf.md): print formated data in a `FILE*` using `va_list`
- [ft_vasprintf](func/ft_vasprintf.md): create a new string with formated data using `va_list`
- [ft_vsprintf](func/ft_vsprintf.md): fill a currrent string with formated data using `va_list`
- [ft_vsnprintf](func/ft_vsnprintf.md): fill a currrent string with formated data up to `n` using `va_list`

## Format string

a format string is a string that contains *convertion element* that start with `%`
( e.g.: `%d` )

### Flag char

the char `%` is followed by one or more of the following char:

- `#`:
  - for the convertion `oO`, the output will be prefixed by `0`
  - for the convertion `xX`, the output will be prefixed by `0x` or `0X`
- `space`:
  - an space is added before an positif number or an empty string
- `+`:
  - force the sign display for number
  - `+` override `space` if both used
- `-`:
  - the field width is left padded, see [field width](#the-field-width)
- `0`:
  - for converstion `oOdDxXiuU` the value is left padded with `0`, see [field width](#the-field-width)

### The field width

An optional field, it can be a *number* or `*`

when the field is a *number*, the *number* will be used as width value

whe the field is `*`, the value of the next argument will be used as width value
a negative value, will set the flag `-` and the value converted to a positive

the width represent the minimal width of the convertion, it use *space* as padding char
the padding is make on the right side by default

### The precision field

An optional field, the field begin with `.` and **MAY** be followed by a *number* or `*`

when `.` is not preceded by a *number* or `*`, the precision will be **0**.

when `.` is preceded by a *number*, the precision will be that *number*.

when `.` is preceded by `*`, the precision will be the value of the next argument.
is the precision is negative, is value is clamp to **0**

### Length modifier

An optional field, used for integer convertion `duioxX` which can have one of the following value:

| prefix | corresponding type    |
| ------ | --------------------- |
| `hh`   | `char`                |
| `h`    | `short`               |
| `l`    | `long`                |
| `ll`   | `long long`           |
| `z`    | `size_t` or `ssize_t` |

### Convertion Type

standar flag:

- `d`, `i`: the argument is a *signed int*
  - the precision give the minimum of digits to appear
  - the default precision is **1** for **0** to be display on **NULL** value
  - if the precision is **0** and the value is **NULL** nothing is print
- `x`, `X`: the argument is a *unsigned int*
  - the default precision is **1**
- `o`: the argument is a *unsigned int*
  - the default precision is **1**
- `u`: the argument is a *unsigned int*
  - like the convertion `d` but with a *unsigned* value
- `s`: the argument is a *string*
  - if the lenght modifier is `l`, the argument is a *wide string*,
  this string is converted to a standar *string* that **MAY** be longer that the *wide string*,
  the transformed *string* is then print
  - when the *string* is **NULL**, it will use `(null)` as string
  - the precision indicate the number of char to print
  - if the precision is greater than the size of *string* it will use `space` as padding char
  - if a precision is present the *string* don't need to be terminated by `\0`
- `c`: the argument is a *char*
  - if the length modifer is `l`, the argument is a *wide char* and convert it to is multi char representation before print
  - print the argument
- `p`: the argument is an *addr*, the *addr* is printed using `%#lx`
- `%`: print an `%`
- `n`: the argument is an *int addr*
  - it will put the number of char written until the call to the referenced *int*


non standar flag:

- `C`: is like `%lc`
- `S`: is like `%ls`
- `D`: is like `%ld`
- `U`: is like `%lu`
- `O`: is like `%lo`
