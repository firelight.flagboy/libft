# [ft_asprintf](../../../srcs/printf/ft_asprintf.c)

like [ft_sprintf](ft_sprintf.md), but will alocated a string for that and put is addr
in `as`

```C
int ft_asprintf(char **as, char *format, ...);
```
