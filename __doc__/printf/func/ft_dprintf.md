# [ft_dprintf](../../../srcs/printf/ft_dprintf.c)

like [ft_printf](ft_printf.md), but the output are write in `fd` as file descriptor

```C
int ft_dprintf(int fd, const char **format, ...);
```
