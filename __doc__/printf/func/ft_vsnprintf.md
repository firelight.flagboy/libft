# [ft_vsnprintf](../../../srcs/printf/ft_vsnprintf.c)

like [ft_snprintf](ft_snprintf.md), but use the list given by `va_start()`

```C
int ft_vsnprintf(char *s, size_t n, char *format, va_list ap);
```
