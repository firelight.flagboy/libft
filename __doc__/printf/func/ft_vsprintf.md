# [ft_vsprintf](../../../srcs/printf/ft_vsprintf.c)

like [ft_vsprintf](ft_vsprintf.md), but use the list given by `va_start()`

```C
int ft_vsprintf(char *s, char *format, va_list ap);
```
