# [ft_snprintf](../../../srcs/printf/ft_snprintf.c)

like [ft_sprintf](ft_sprintf.md), but stop after `n` char

```C
int ft_snprintf(char *s, size_t n, char *format, ...);
```
