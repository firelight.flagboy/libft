# [ft_fprintf](../../../srcs/printf/ft_fprintf.c)

like [ft_dprintf](ft_dprintf.md), but use `FILE*` struct from `stdio`

```C
int ft_fprintf(FILE *stream, const char **format, ...);
```
