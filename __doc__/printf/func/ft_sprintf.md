# [ft_sprintf](../../../srcs/printf/ft_sprintf.c)

like [ft_printf], but instead of printing data to **stdout**, the data is put in `s`

```C
int ft_sprintf(char *s, char *format, ...);
```
