# [ft_printf](../../../srcs/printf/ft_printf.c)

print data to **stdout** using the format string `format`, and the argument that follow

```C
int ft_printf(const char **format, ...);
```
