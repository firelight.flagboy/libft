# [ft_eprintf](../../../srcs/printf/ft_eprintf.c)

like [ft_printf](ft_printf.md), but the output is print to **stderr**

```C
int ft_eprintf(const char **format, ...);
```
