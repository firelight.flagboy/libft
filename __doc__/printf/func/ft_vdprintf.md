# [ft_vdprintf](../../../srcs/printf/ft_vdprintf.c)

like [ft_dprintf](ft_dprintf.md), but use the list given by `va_start()`

```C
int ft_vdprintf(int fd, const char **format, va_list ap);
```
