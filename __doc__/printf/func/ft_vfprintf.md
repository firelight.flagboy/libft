# [ft_vfprintf](../../../srcs/printf/ft_vfprintf.c)

like [ft_fprintf](ft_fprintf.md), but use the list given by `va_start()`

```C
int ft_vfprintf(FILE *stream, const char **format, va_list ap);
```
