# [ft_vasprintf](../../../srcs/printf/ft_vasprintf.c)

like [ft_asprintf](ft_asprintf.md), but use the list given by `va_start()`

```C
int ft_vasprintf(char **as, char *format, va_list ap);
```
