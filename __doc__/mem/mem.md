# Mem

module used for byte array operation

[header](incs/../../../incs/libft/mem.h)

## Functions

- [ft_bzero](func/ft_bzero.md) set the byte array content's to *null*
- [ft_memalloc](func/ft_memalloc.md) create a byte array with is field to *null*
- [ft_memccpy](func/ft_memccpy.md) copy an array to another and stop at the end or when a byte if found
- [ft_memchr](func/ft_memchr.md) search for a byte in array
- [ft_memcmp](func/ft_memcmp.md) compare two array
- [ft_memcpy](func/ft_memcpy.md) copy an array to another
- [ft_memdel](func/ft_memdel.md) free an array, `cleanup` attribute compatible
- [ft_memmove](func/ft_memmove.md) move the content of an array to another location
- [ft_memset](func/ft_memset.md) set the content of an array
