# [ft_memccpy](../../../srcs/mem/ft_memccpy.c)

cpy an bytes array to another until the end or if bytes `c` is found

```C
void *ft_memccpy(void *dest, const void *src, int c, size_t n);
```
