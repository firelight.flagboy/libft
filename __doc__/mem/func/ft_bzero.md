# [ft_bzero](../../../srcs/mem/ft_bzero.c)

set all bytes of the bytes array to NULL

```C
void ft_bzero(void *s, size_t n);
```
