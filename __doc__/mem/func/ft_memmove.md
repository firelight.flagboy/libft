# [ft_memmove](../../../srcs/mem/ft_memmove.c)

copy an byte array into another in a non destructive way if the arrays overlap

```C
void *ft_memmove(void *dest, const void *src, size_t n);
```
