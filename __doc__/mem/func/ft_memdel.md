# [ft_memdel](../../../srcs/mem/ft_memdel.c)

free the address contains by `ap` then set it addr to NULL.
this function is compatible for the attribute **cleanup**

```C
void ft_memdel(void **ap);
```
