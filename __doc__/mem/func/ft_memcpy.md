# [ft_memcpy](../../../srcs/mem/ft_memccpy.c)

copy a byte array to another

```C
void *ft_memcpy(void *dest, const void *src, size_t n);
```
