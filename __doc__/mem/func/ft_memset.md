# [ft_memset](../../../srcs/mem/ft_memset.c)

set all the byte of an bytes array to the value `c`

```C
void *ft_memset(void *str, int c, size_t n);
```
