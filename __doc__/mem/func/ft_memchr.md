# [ft_memchr](../../../srcs/mem/ft_memchr.c)

search for a byte in a byte array.
return the addr where the byte is found or NULL

```C
void *ft_memchr(const void *s, int c, size_t n);
```
