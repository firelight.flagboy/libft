# [ft_memalloc](../../../srcs/mem/ft_memalloc.c)

alloc an byte arrays and set all of its bytes to NULL

```C
void *ft_memalloc(size_t size) __attribute__((alloc_size(1)));
```
