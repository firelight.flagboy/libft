# [ft_memcmp](../../../srcs/mem/ft_memcmp.c)

compare 2 byte arrays and return 0 if match or the difference between the 2 non match byte

```C
int ft_memcmp(const void *s1, const void *s2, size_t n);
```
