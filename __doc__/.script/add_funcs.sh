for file in $@; do
	dir=$(dirname $file)
	if [ ! -f $file ] || [ ! -d $dir/func ]; then
		continue
	fi
	funcs_dir="$dir/func"

	for func in $(ls $funcs_dir); do
		func_name=$(echo $func | cut -d. -f1)
		echo "- [$func_name](func/$func): " >> $file
	done
done
