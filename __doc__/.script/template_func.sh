for file in $@; do
	if [ ! -f $file ]; then
		continue
	fi
	func_name=$(echo $file | rev | cut -d/ -f1 | rev | cut -d. -f1)
	place=$(echo $func_name | rev | cut -d_ -f1 | rev)
echo "# [$func_name]($place)

\`\`\`C

\`\`\`" > $file

done
