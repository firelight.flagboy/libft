# [ft_lstnew](../../../srcs/list/ft_lstnew.c)

create a new `node`

```C
t_list *ft_lstnew(void *content, size_t content_size);
```
