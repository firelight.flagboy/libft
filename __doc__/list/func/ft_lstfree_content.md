# [ft_lstfree_content](../../../srcs/list/ft_lstfree_content.c)

use to free basic content element

```C
void ft_lstfree_content(void *content, size_t content_size);
```
