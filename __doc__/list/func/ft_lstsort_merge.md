# [ft_lstsort_merge](../../../srcs/list/ft_lstsort_merge.c)

sort the list using **Merge sort Algorithm** with the **callback** `cmp` as comparator

```C
void ft_lstsort_merge(t_list **head_ref, int (*cmp)(t_list *a, t_list *b));
```
