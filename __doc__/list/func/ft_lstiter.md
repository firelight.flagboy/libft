# [ft_lstiter](../../../srcs/list/ft_lstiter.c)

use the *callback* `f` on each node of `lst`

```C
void ft_lstiter(t_list *lst, void (*f)(t_list *elem));
```
