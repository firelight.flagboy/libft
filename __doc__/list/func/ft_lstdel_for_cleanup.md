# [ft_lstdel_for_cleanup](../../../srcs/list/ft_lstdel_for_cleanup.c)

used to delete an basic list, use [ft_lstfree_content](ft_lstfree_content.md)
to free the content.



```C
void ft_lstdel_for_cleanup(t_list **ptr_to_alst)
```

for more complex structure we suggest to use [ft_lstdel](ft_lstdel.md) with the apropriated `callback`
