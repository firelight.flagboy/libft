# [ft_lstmap](../../../srcs/list/ft_lstmap.c)

return a new list using the **callback** `f` on each node of the list

```C
t_list *ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem));
```
