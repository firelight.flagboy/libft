# [ft_lstadd](../../../srcs/list/ft_lstadd.c)

add a node to the begin to list

```C
void ft_lstadd(t_list **alst, t_list *new);
```

`ft_lstadd` is set a **DEPRECATED** use [ft_lstpush_front](ft_lstpush_front.md)
