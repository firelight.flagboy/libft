# [ft_lstdel](../../../srcs/list/ft_lstdel.c)

use to free an entire list. the `del` callback is used on the `content` element

`del` is the callback that will free the `content` element

```C
void ft_lstdel(t_list **alst, void (*del)(void *, size_t));
```
