# [ft_lstpush_front](../../../srcs/list/ft_lstpush_front.c)

add node to the begin of the list

```C
void ft_lstpush_front(t_list **head_ref, t_list *elem);
```
