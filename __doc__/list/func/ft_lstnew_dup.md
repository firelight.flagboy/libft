# [ft_lstnew_dup](../../../srcs/list/ft_lstnew_dup.c)

create a new `node` and duplicate `content` to the new `node`

```C
t_list *ft_lstnew_dup(void const *content, size_t content_size);
```
