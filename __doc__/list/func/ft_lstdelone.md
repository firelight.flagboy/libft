# [ft_lstdelone](../../../srcs/list/ft_lstdelone.c)

use to free the given node, `del` is used as `callback` to free `content` element

```C
void ft_lstdelone(t_list **alst, void (*del)(void *, size_t));
```
