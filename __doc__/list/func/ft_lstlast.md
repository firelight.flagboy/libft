# [ft_lstlast](../../../srcs/list/ft_lstlast.c)

return the last node of a list

```C
t_list *ft_lstlast(t_list *node);
```
