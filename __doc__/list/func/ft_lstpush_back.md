# [ft_lstpush_back](../../../srcs/list/ft_lstpush_back.c)

a node to the end of the list

```C
void ft_lstpush_back(t_list **head_ref, t_list *elem);
```
