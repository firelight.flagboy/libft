# List

module used for basic *linked list* operation
this module handle most of the stuff for basic *linked list*

[header](../../incs/libft/list.h)

## Functions

- [ft_lstadd](func/ft_lstadd.md) add node to list
- [ft_lstdel](func/ft_lstdel.md) free list
- [ft_lstdel_for_cleanup](func/ft_lstdel_for_cleanup.md) free list `cleanup` attribute compatible
- [ft_lstdelone](func/ft_lstdelone.md) remove the first node of the list
- [ft_lstfree_content](func/ft_lstfree_content.md) free the content of a node
- [ft_lstiter](func/ft_lstiter.md) iterate the list
- [ft_lstlast](func/ft_lstlast.md) get last node of the list
- [ft_lstlen](func/ft_lstlen.md) get the len of the list
- [ft_lstmap](func/ft_lstmap.md) map a list to another
- [ft_lstnew](func/ft_lstnew.md) create new node
- [ft_lstnew_dup](func/ft_lstnew_dup.md) duplicate node
- [ft_lstpush_back](func/ft_lstpush_back.md) push the node to the end of the list
- [ft_lstpush_front](func/ft_lstpush_front.md) push the node at the begin of the list
- [ft_lstsort_merge](func/ft_lstsort_merge.md) sort list using **merge sort**
