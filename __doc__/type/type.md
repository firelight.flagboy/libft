# Type

module to get the type of a char

[header](../../incs/libft/type.h)

## Functions

- [ft_isalnum](func/ft_isalnum.md) char match `[0-9a-zA-Z]`
- [ft_isalpha](func/ft_isalpha.md) char is in alphabet
- [ft_isascii](func/ft_isascii.md) char is valid ascii
- [ft_isdigit](func/ft_isdigit.md) char is digit ascii
- [ft_islower](func/ft_islower.md) char is lower letter
- [ft_isupper](func/ft_isupper.md) char is upper letter
- [ft_isprint](func/ft_isprint.md) char is printable
- [ft_isspace](func/ft_isspace.md) char is space char
- [ft_isxdigit](func/ft_isxdigit.md) char is hexdigit
- [ft_tolower](func/ft_tolower.md) convert char to lowercase
- [ft_toupper](func/ft_toupper.md) convert char to uppercase
