# [ft_isalnum](../../../srcs/type/ft_isalnum.c)

return `1` or `0` if the given char match with `[0-9a-zA-Z]`

```C
int ft_isalnum(int c);
```
