# [ft_islower](../../../srcs/type/ft_islower.c)

return `1` or `0` if the given char is lowercase letter

```C
int ft_islower(int c);
```
