# [ft_isalpha](../../../srcs/type/ft_isalpha.c)

return `1` or `0` if the given char is an alphabet letter in lower/upper case

```C
int ft_isalpha(int c);
```
