# [ft_tolower](../../../srcs/type/ft_tolower.c)

convert lowercase letter to uppercase letter, change nothing if the char is not an lowercase letter

```C
int ft_tolower(int c);
```
