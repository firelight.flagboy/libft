# [ft_isupper](../../../srcs/type/ft_isupper.c)

return `1` or `0` if the given char is a uppercase letter

```C
int ft_isupper(int c);
```
