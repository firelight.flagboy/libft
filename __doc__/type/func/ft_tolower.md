# [ft_tolower](../../../srcs/type/ft_tolower.c)

convert uppercase letter to lowercase letter, change nothing if the char is not an uppercase letter

```C
int ft_tolower(int c);
```
