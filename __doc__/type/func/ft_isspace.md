# [ft_isspace](../../../srcs/type/ft_isspace.c)

return `1` or `0` if the given char match with `\s`

```C
int ft_isspace(int c);
```
