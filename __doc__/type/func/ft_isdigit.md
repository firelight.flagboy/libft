# [ft_isdigit](../../../srcs/type/ft_isdigit.c)

return `1` or `0` if the given char is digit letter

```C
int ft_isdigit(int c);
```
