# [ft_isprint](../../../srcs/type/ft_isprint.c)

return `1` or `0` if the given char is printable

```C
int ft_isprint(int c);
```
