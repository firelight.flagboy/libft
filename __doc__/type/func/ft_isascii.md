# [ft_isascii](../../../srcs/type/ft_isascii.c)

return `1` or `0` if the given char is valid ascii value

```C
int ft_isascii(int c);
```
