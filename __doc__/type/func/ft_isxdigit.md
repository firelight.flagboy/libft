# [ft_isxdigit](../../../srcs/type/ft_isxdigit.c)

return `1` or `0` if the given char match with `[0-9a-fA-F]`

```C
int ft_isxdigit(int c);
```
