# Num

module to interact with integer

[header](../../incs/libft/num.h)

## Functions

- [ft_get_hex_letter](func/ft_get_hex_letter.md) get the hex letter for the char value
- [ft_len_int](func/ft_len_int.md) get the len of an `ssize_t` for a certain radix without `-`
- [ft_len_uint](func/ft_len_uint.md) get the len of an `size_t` for a certain radix
- [ft_lendigit](func/ft_lendigit.md) get the len of an ascii number
