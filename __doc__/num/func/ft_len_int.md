# [ft_len_int](../../../srcs/num/ft_len_int.c)

get the len of a signed int for its string representation

```C
ssize_t ft_len_int(ssize_t num, uint radix) __attribute__((const));
```
