# [ft_lendigit](../../../srcs/num/ft_lendigit.c)

get the len of a number contain in the string

```C
size_t ft_lendigit(const char *str);
```
