# [ft_get_hex_letter](../../../srcs/num/ft_get_hex_letter.c)

return the hex letter of the 4 lowest bits

```C
char ft_get_hex_letter(int val) __attribute__((const));
```
