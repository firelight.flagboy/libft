# [ft_len_uint](../../../srcs/num/ft_len_uint.c)

get the len of a unsigned int for its string representation

```C
ssize_t ft_len_uint(ssize_t num, uint radix) __attribute__((const));
```
