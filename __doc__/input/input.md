# Input

module for interacting with input

[header](../../incs/libft/input.h)

## Funcions

- [ft_read_file](func/ft_read_file.md) read the content of fd until **EOF**
