# [ft_read_file](../../../srcs/input/ft_read_file.c)

read the given **file descriptor** and return a `string`

```C
char *ft_read_file(int fd);
```

return **NULL** on error

not a suggested way to read a file
