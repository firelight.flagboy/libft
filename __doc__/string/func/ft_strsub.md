# [ft_strsub](../../../srcs/string/ft_strsub.c)

return a substring that start at the index `start` and stop at `start + len`

```C
char *ft_strsub(char const *s, unsigned int start, size_t len);
```
