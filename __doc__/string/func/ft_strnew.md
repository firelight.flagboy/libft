# [ft_strnew](../../../srcs/string/ft_strnew.c)

create a new string of size `size` with is memory area set to **0**

```C
char *ft_strnew(size_t size);
```
