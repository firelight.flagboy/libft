# [ft_strlcat](../../../srcs/string/ft_strlcat.c)

concat `src` to `dst` up to `size - 1`
`dst` will always be terminated by **0**

```C
size_t ft_strlcat(char *dst, const char *src, size_t size);
```
