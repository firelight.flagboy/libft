# [ft_strdel](../../../srcs/string/ft_strdel.c)

free the string pointed by `as` and set `as` to NULL

can be used with `__attribute__((cleanup))`

```C
void ft_strdel(char **as);
```
