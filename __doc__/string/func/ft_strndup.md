# [ft_strndup](../../../srcs/string/ft_strndup.c)

like [ft_strdup](ft_strdup.md), but stop after `n` bytes

```C
char *ft_strndup(char const *str, size_t n);
```
