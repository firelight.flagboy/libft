# [ft_strnlen](../../../srcs/string/ft_strnlen.c)

like [ft_strlen](ft_strlen.md), but stop after `n` bytes

```C
size_t ft_strnlen(const char *s, size_t n);
```
