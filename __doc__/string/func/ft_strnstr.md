# [ft_strnstr](../../../srcs/string/ft_strnstr.c)

like [ft_strstr](ft_strstr.md), but stop after `len` bytes

```C
char *ft_strnstr(const char *haystack, const char *needle, size_t len);
```
