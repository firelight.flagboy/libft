# [ft_strcspn](../../../srcs/string/ft_strcspn.c)

return the size until a char in `charset` is found in `s`

```C
size_t ft_strcspn(const char *s, const char *charset);
```
