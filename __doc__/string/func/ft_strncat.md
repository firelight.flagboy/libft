# [ft_strncat](../../../srcs/string/ft_strncat.c)

like [ft_strcat](ft_strcat.md), but stop after `n` bytes

```C
char *ft_strncat(char *s1, char const *s2, size_t n);
```
