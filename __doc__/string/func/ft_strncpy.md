# [ft_strncpy](../../../srcs/string/ft_strncpy.c)

like [ft_strcpy](ft_strcpy.md), but stop after `len` bytes

```C
char *ft_strncpy(char *dst, const char *src, size_t len);
```
