# [ft_strchr](../../../srcs/string/ft_strchr.c)

return the address where `c` was found in `s` or `NULL`

```C
char *ft_strchr(const char *s, int c);
```
