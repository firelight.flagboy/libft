# [ft_strequ](../../../srcs/string/ft_strequ.c)

compare `s1` to `s2` return `1` if equals or `0`

```C
int ft_strequ(char const *s1, char const *s2);
```
