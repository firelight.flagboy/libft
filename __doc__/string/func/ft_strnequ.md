# [ft_strnequ](../../../srcs/string/ft_strnequ.c)

like [ft_strequ](ft_strequ.md), but stop after `n` bytes

```C
int ft_strnequ(char const *s1, char const *s2, size_t n);
```
