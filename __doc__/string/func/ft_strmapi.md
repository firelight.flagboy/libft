# [ft_strmapi](../../../srcs/string/ft_strmapi.c)

create a new string that is bytes is the value returned by `f()`
for the byte at the same index in `s` and the given index

```C
char *ft_strmapi(char const *s, char (*f)(unsigned int, char));
```
