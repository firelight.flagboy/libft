# [ft_strlen](../../../srcs/string/ft_strlen.c)

return the size of `s`

```C
size_t ft_strlen(const char *s);
```
