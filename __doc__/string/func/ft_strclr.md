# [ft_strclr](../../../srcs/string/ft_strclr.c)

set all the byte in `s` to `nul`

```C
void ft_strclr(char *s);
```
