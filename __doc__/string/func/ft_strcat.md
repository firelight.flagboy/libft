# [ft_strcat](../../../srcs/string/ft_strcat.c)

add `s2` to the end of `s1` return `s1`

```C
char *ft_strcat(char *s1, const char *s2);
```
