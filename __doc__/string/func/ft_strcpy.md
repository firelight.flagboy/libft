# [ft_strcpy](../../../srcs/string/ft_strcpy.c)

copy `src` in `dst`

return the addr of `dst`

```C
char *ft_strcpy(char *dst, const char *src);
```
