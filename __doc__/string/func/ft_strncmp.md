# [ft_strncmp](../../../srcs/string/ft_strncmp.c)

like [ft_strcmp](ft_strcmp.md), but stop after `n` byte or at the end of one string

```C
int ft_strncmp(const char *s1, const char *s2, size_t n);
```
