# [ft_strjoin](../../../srcs/string/ft_strjoin.c)

return a new string that is the concatenation of `s1` and `s2`

```C
char *ft_strjoin(char const *s1, char const *s2);
```
