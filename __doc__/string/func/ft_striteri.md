# [ft_striteri](../../../srcs/string/ft_striteri.c)

call `f()` on each bytes of `s` with the index of the current bytes in the string

```C
void ft_striteri(char *s, void (*f)(unsigned int, char *));
```
