# [ft_strspn](../../../srcs/string/ft_strspn.c)

return the size until the currect bytes in `s` is not in `charset`

```C
size_t ft_strspn(const char *s, const char *charset);
```
