# [ft_count_word](../../../srcs/string/ft_count_word.c)

count the number of *word* in a string

```C
size_t ft_count_word(char *str);
```
