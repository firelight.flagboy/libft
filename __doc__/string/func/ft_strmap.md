# [ft_strmap](../../../srcs/string/ft_strmap.c)

create a new string that is bytes is the value returned by `f()`
for the byte at the same index in `s`

```C
char *ft_strmap(char const *s, char (*f)(char));
```
