# [ft_strcmp](../../../srcs/string/ft_strcmp.c)

compare 2 strings return **0** if the strings match eachother
or the difference between the 2 different bytes

```C
int ft_strcmp(const char *s1, const char *s2);
```
