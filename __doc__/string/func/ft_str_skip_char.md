# [ft_str_skip_char](../../../srcs/string/ft_str_skip_char.c)

skip byte in `s` until one is not equals to `to_skip`

return the first byte who is not equals to `to_skip`

```C
char *ft_str_skip_char(const char *s, int to_skip);
```
