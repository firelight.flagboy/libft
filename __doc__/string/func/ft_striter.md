# [ft_striter](../../../srcs/string/ft_striter.c)

call `f()` on each bytes of `s`

```C
void ft_striter(char *s, void (*f)(char *));
```
