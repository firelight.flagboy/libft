# [ft_strstr](../../../srcs/string/ft_strstr.c)

search a substring into another, return the begin where `needle` was found in `haystack`
or **NULL**

```C
char *ft_strstr(const char *haystack, const char *needle);
```
