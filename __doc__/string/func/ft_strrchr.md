# [ft_strrchr](../../../srcs/string/ft_strrchr.c)

like [ft_strchr](ft_strchr.md), but it will return the last match

```C
char *ft_strrchr(const char *s, int c);
```
