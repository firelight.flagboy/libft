# [ft_strtrim](../../../srcs/string/ft_strtrim.c)

return a new string that is the trimmed version of `s` without *spaces* at the beginning or the end

```C
char *ft_strtrim(char const *s);
```
