# [ft_str_skip_char_f](../../../srcs/string/ft_str_skip_char_f.c)

skip the byte until the end of `s` of `f()` return **0**

the return ptr is the end of `s` or the first byte where `f()` return **0**

```C
char *ft_str_skip_char_f(const char *s, int (*f)(int));
```
