# [ft_realloc_str](../../../srcs/string/ft_realloc_str.c)

resize the string referenced by `astr` to size `len`, after the copy it will free
the referenced string and modified the value `astr` to point to the new memory space

The address referenced by `astr` **SHOULD** be a valid address for `free()`

```C
char *ft_realloc_str(char **astr, size_t len);
```

I don't recommend to use this function
