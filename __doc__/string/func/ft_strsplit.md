# [ft_strsplit](../../../srcs/string/ft_strsplit.c)

split the string `s` on `c` bytes

```C
char **ft_strsplit(char const *s, char c);
```
