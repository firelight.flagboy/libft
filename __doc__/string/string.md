# String

module for string operation

[header](../../incs/libft/string.h)

## Functions

- [ft_count_word](func/ft_count_word.md) count word in string
- [ft_realloc_str](func/ft_realloc_str.md) realloc a string
- [ft_str_skip_char](func/ft_str_skip_char.md) skip the given char
- [ft_str_skip_char_f](func/ft_str_skip_char_f.md) skip until the callback say stop
- [ft_strcspn](func/ft_strcspn.md) get the index when the charset is found
- [ft_strspn](func/ft_strspn.md) get the index until the charset not match
- [ft_strdel](func/ft_strdel.md) free string, `cleanup` attribut compatible
- [ft_strdup](func/ft_strdup.md) duplicate a string
- [ft_strndup](func/ft_strndup.md) duplicate a string
- [ft_strcmp](func/ft_strcmp.md) compare 2 string
- [ft_strequ](func/ft_strequ.md) compare 2 string
- [ft_strncmp](func/ft_strncmp.md) compare 2 string
- [ft_strnequ](func/ft_strnequ.md) compare 2 string
- [ft_striter](func/ft_striter.md) iterate a string
- [ft_striteri](func/ft_striteri.md) iterate a string
- [ft_strjoin](func/ft_strjoin.md) join 2 string together
- [ft_strcat](func/ft_strcat.md) concat a string to another
- [ft_strlcat](func/ft_strlcat.md) concat a string to another
- [ft_strncat](func/ft_strncat.md) concat a string to another
- [ft_strmap](func/ft_strmap.md) map a string
- [ft_strmapi](func/ft_strmapi.md) map a string
- [ft_strcpy](func/ft_strcpy.md) copy a string to another
- [ft_strncpy](func/ft_strncpy.md) copy a string to another
- [ft_strnew](func/ft_strnew.md) create an empty string
- [ft_strclr](func/ft_strclr.md) empty a string
- [ft_strlen](func/ft_strlen.md) get the len of a string
- [ft_strnlen](func/ft_strnlen.md) get the len of a string
- [ft_strnstr](func/ft_strnstr.md) search a string inside another
- [ft_strstr](func/ft_strstr.md) search a string inside another
- [ft_strchr](func/ft_strchr.md) search a char in a string
- [ft_strrchr](func/ft_strrchr.md) reverse `strchr`
- [ft_strsplit](func/ft_strsplit.md) split a string into substring
- [ft_strsub](func/ft_strsub.md) get a sub string
- [ft_strtrim](func/ft_strtrim.md) trim a string
