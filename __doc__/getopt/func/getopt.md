# [getopt](../../../srcs/getopt/getopt.c)

`ft_getopt` will scan *option* in `argv`, it will return **-1** on failure,

`options` is difined in [getopt optstring](../getopt.md#optstring)

to know the current *option* being parsed see [getopt global](../getopt.md#global)

```C
int ft_getopt(int argc, char *const *argv, const char *options);
```
