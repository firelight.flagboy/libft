# [getopt_long_only](../../../srcs/getopt/getopt_long_only.c)

like [ft_getopt_long](getopt_long.md), but will allow long option to be prefixed with only `-`

```C
int ft_getopt_long_only(t_arg *arg, const char *shortopts, t_option *longopts, int *indexptr);
```
