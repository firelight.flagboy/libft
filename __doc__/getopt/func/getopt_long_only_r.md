# [getopt_long_only_r](../../../srcs/getopt/getopt_long_only_r.c)

like [ft_getopt_long_only](getopt_long_only.md), but use `t_getopt_data` struct instead of global

```C
int ft_getopt_long_only_r(t_getopt_data *data);
```
