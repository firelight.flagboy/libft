# [getopt_long_r](../../../srcs/getopt/getopt_long_r.c)

like [ft_getopt_long](getopt_long.md), but use `t_getopt_data` struct instead of global

```C
int ft_getopt_long_r(t_getopt_data *data);
```
