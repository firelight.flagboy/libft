# [getopt_long](../../../srcs/getopt/getopt_long_only.c)

like [ft_getopt](getopt.md), but will also parse long options defined in `longopts`
the long option needed to be prefixed by `--`

```C
int ft_getopt_long(t_arg *arg, const char *shortopts, t_option *longopts, int *indexptr);
```
