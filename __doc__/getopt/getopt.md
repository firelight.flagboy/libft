# Getopt

module that implement `getopt` functions

## Functions

- [getopt](func/getopt.md): for parsing short option
- [getopt_long](func/getopt_long.md): parsing using short/long option
- [getopt_long_only](func/getopt_long_only.md): parsing allowing `-` for long option
- [getopt_long_only_r](func/getopt_long_only_r.md): like [getopt_long_only](func/getopt_long_only.md), but usign `data` struct
- [getopt_long_r](func/getopt_long_r.md): like [getopt_long](func/getopt_long.md), but using `data` struct

## Optstring

`optstring` is the string that represent the short options.
when a short option is followed by `:` it **require** an argument
when a short option is followd by `::` it have an **optionnal** argument
otherwise the option don't need an argument

when `optstring` start with `+`, `argv` need to be in **ORDER**
when `optstring` start with `-` it will return `argv` in **ORDER**
otherwise it will **PERMUTE** element in `argv`
after `+`/`-` if the next char is `:`, it will not print error ( like `g_opterr = 0` )

## Global

using `getopt`, you have access to 4 global value:

```C
int g_opterr = 1;
int g_optopt = '?';
int g_optind = 1;
char *g_optarg = NULL;
```

`g_opterr` if set to not **0** will print error ( e.g.: missing argument, unknow option, ...) to `stderr`
`g_optopt` the current option `?`/`:` indicate an error in parsing
`g_optind` current index in `argv`
`g_optarg` the current argument given to the option or the option that raise an error

## Struct getopt_data

```C
typedef struct s_getopt_data t_getopt_data;

struct s_getopt_data
{
  // option given to getopt_long_r
  int             argc;
  char *const     *argv;
  int             *longind;
  int             long_only;
  const char      *optstring;
  const t_option  *longopts;

  // global element
  int             optind;
  int             opterr;
  int             optopt;
  char            *optarg;

  // internal element
  int             is_initialized;
  char            *next;
  t_ord           ordering;
  int             first_nonopt;
  int             last_nonopt;
  int             print_error;
  int             err_opt;
  const char      *modified_optstring;
};
```

`longind` is the index where the long option is found
