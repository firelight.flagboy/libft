# Output

module used when you need to output data

[header](../../incs/libft/output.h)

## Functions

- [ft_putchar](func/ft_putchar.md) print `char`
- [ft_putchar_fd](func/ft_putchar_fd.md) print `char` to fd
- [ft_putendl](func/ft_putendl.md) print `string` followed by NL
- [ft_putendl_fd](func/ft_putendl_fd.md) printf `string` to fd followed by NL
- [ft_putnbr](func/ft_putnbr.md) print `num`
- [ft_putnbr_fd](func/ft_putnbr_fd.md) print `num` to fd
- [ft_putstr](func/ft_putstr.md) print `string`
- [ft_putstr_fd](func/ft_putstr_fd.md) print `string` to fd
