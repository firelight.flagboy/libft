# [ft_putendl_fd](../../../srcs/output/ft_putendl_fd.c)

write the string `s` to the file descriptor `fd` and append a NL

```C
void ft_putendl_fd(char const *s, int fd);
```
