# [ft_putchar_fd](../../../srcs/output/ft_putchar_fd.c)

write the char `c` to the file descriptor `fd`

```C
void ft_putchar_fd(char c, int fd);
```
