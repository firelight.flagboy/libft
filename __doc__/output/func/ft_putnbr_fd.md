# [ft_putnbr_fd](../../../srcs/output/ft_putnbr_fd.c)

write the num `n` to the file descriptor `fd`

```C
void ft_putnbr_fd(int n, int fd);
```
