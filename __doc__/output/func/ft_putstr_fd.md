# [ft_putstr_fd](../../../srcs/output/ft_putstr_fd.c)

write the string `s` to the file descriptor `fd`

```C
void ft_putstr_fd(char const *s, int fd);
```
