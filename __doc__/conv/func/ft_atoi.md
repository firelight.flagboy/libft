# [ft_atoi](../../../srcs/conv/ft_atoi.c)

convert a given `string` to an `int`

```C
int ft_atoi(const char *s);
```

## String format

the `string` **MAY** be prefixed with `space` char.

the `string` **MAY** be prefixed with `sign` char after the `spaces`.

the `string` **MUST** have digit char.

## Info

`ft_atoi` by default return `0`, be carefull on bad formated `string`.

`ft_atoi` is not protected against **integer overflow**.

If the `string` have a `-` before the *digit part*, it will return a *negative number*
