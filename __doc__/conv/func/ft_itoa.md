# [ft_itoa](../../../srcs/conv/ft_itoa.c)

convert a given `int` to an `string`

```C
char *ft_itoa(int n);
```

## Info

the `string` will be prefixed with `-` if negative
