# [ft_atoll](../../../srcs/conv/ft_atoll.c)

convert a given `string` to an `long long`

```C
long long ft_atoll(const char *s);
```

## String format

the `string` **MAY** be prefixed with `space` char.

the `string` **MAY** be prefixed with `sign` char after the `spaces`.

the `string` **MUST** have digit char.

## Info

`ft_atoll` by default return `0`, be carefull on bad formated `string`.

`ft_atoll` is not protected against **integer overflow**.

If the `string` have a `-` before the *digit part*, it will return a *negative number*
