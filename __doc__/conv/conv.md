# Convertion Operation

module used for convertion between type

[header](../../incs/libft/conv.h)

## Functions

- [ft_atoi](func/ft_atoi.md) convert `string` to `int`
- [ft_atol](func/ft_atol.md) convert `string` to `long`
- [ft_atoll](func/ft_atoll.md) convert `string` to `long long`
- [ft_itoa](func/ft_itoa.md) convert `int` to `string`
