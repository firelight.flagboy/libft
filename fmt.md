# FMT

## init

init the `t_fmt` *struct* with `*stream` and `settings`

set all field to **0**
set `->right_margin` to `settings->right_margin`
alloc `->buffer` with a size of **FMT_BUFF_SIZE**
set `->p` to the start of `->buffer`
set `->end` to the end of `->buffer`

return `->buffer` if success

## clean

clean `fmt`

dump `->buffer` on last time to `->stream`
free `->buffer`

## flush

flush the data in `fmt`

dump `->buffer` on `->stream`
reset `->p`

## putc

add *char* `c` to `->buffer`

check available size on the current line with `fmt_ensure()`
if `c` is a *newline* set `->cursor` to **0**

return `c`

## write

add *void* `s` to `->buffer` up to *size_t* `len`

while `len` > 0

1. get the minimum between `len` and `->end - ->p`
2. copy `s` into `->buffer` up to the minimum value
3. if the minimum value is `->end - ->p` flush with `flush()`
4. update `s`, `len`, `->cursor` and `->p`

return the number bytes put in `->buffer`

## puts

add *string* `s` to `->buffer`

put the len of `s` in *size_t* `len`
set left margin if need using `check_margin()`
while `len` > 0

1. get the max size with `ensure(len)` in `max`
2. put `s` to `->buffer` with `write(s, max)`
3. update `s` and `len`

return the number of bytes put in `->buffer`

## nputs

like `puts` but stop after `n` bytes

## printf

create a formated *string* using `snprintf()` and put it to `->buffer` using `fmt_puts()`

return the number of bytes put in `->buffer`

## set_left_margin

set `->left_margin` to *unsigned* `left_margin`

if `left_margin` > `->right_margin` set `->left_margin` to **0**

return the old value of `->left_margin`

## new_line

create a *newline* with the left margin

1. add *newline* to `->buffer`
2. add `->left_margin` *spaces* to buffer
3. update `->cursor` to the new location

## ensure

ensure that the current provided `size` can be put on the *line*
and return the max size of byte that can be put on the current *line*

if `->cursor` >= `->right_margin`:

1. create a new line with `new_line()`

else if `->cursor + size` >= `->right_margin` and `->right_margin - ->left_margin` > `size`

1. create a new line with `new_line()`

return the minimun between `->right_margin - ->cursor` and `size`
