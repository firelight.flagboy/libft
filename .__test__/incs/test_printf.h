#ifndef TEST_PRINTF_H
# define TEST_PRINTF_H

#include <libft/printf.h>
#include <libunit.h>
#include <stdio.h>
#include <string.h>

extern t_test snprintf_suite[];
extern t_test sprintf_suite[];
extern t_test asprintf_suite[];

// extern t_test printf_suite[];
// extern t_test eprintf_suite[];
// extern t_test dprintf_suite[];
// extern t_test fprintf_suite[];

#endif
