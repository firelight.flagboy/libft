#ifndef TEST_STRING_H
# define TEST_STRING_H

#include <string.h>
#include "libunit.h"
#include "libft/string.h"
#include <ctype.h>

extern t_test strlen_suite[];

extern t_test strcmp_suite[];

extern t_test count_word_suite[];

extern t_test type_suite[];

extern t_test skip_char_suite[];

extern t_test string_suite[];

#endif
