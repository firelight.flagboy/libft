#ifndef TEST_FLAG_H
# define TEST_FLAG_H

#include <libft/flag.h>
#include <libft/flag/utils.h>
#include <libunit.h>

extern t_test flag_utils_suite[];

#endif
