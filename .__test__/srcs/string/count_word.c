#include "libft.h"
#include "libunit.h"
#include "test_string.h"

static int template(char *s, size_t nb_word) {
    if (ft_count_word(s) == nb_word)
        return TEST_SUCCESS;
    return TEST_FAILED;
}

int count_word_empty_string() {
    return template("", 0);
}

int count_word_1_word() {
    return template("bonjour", 1);
}

int count_word_multiple_word() {
    return template("bonjour je suis la", 4);
}

int count_word_multiple_word_space() {
    return template("bonjour\nje\rsuis\vla\tici parla", 6);
}

t_test count_word_suite[] = {
    TEST(count_word_empty_string),
    TEST(count_word_1_word),
    TEST(count_word_multiple_word),
    TEST(count_word_multiple_word_space),
    NULL_TEST
};
