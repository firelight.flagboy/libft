#include "test_string.h"
#include "libft/type.h"
#ifdef __linux__
#include <bsd/string.h>
#else
#include <malloc/malloc.h>
#endif

#define S1 "bonjour je"
#define S2 " suis la!"

int test_strdup() {
	const char s[] = "bonjour je suis la!";
	char *dup __attribute__((cleanup(ft_strdel)));

	dup = ft_strdup(s);
	ASSERT(strcmp(dup, s) == 0);
	return TEST_SUCCESS;
}

int test_strndup() {
	int ret = TEST_SUCCESS;
	const char s[] = "bonjour je suis la!";
	char *dup;

	dup = ft_strndup(s, 0);
	ASSERT(dup[0] == 0);
	free(dup);

	dup = ft_strndup(s, sizeof(s) / 2);
	ASSERT(strncmp(dup, s, sizeof(s) / 2) == 0);
	free(dup);

	return ret;
}

int test_strcpy() {
	const char s[] = "bonjour je suis la!";
	const char s1[] = "";
	char dup[256];

	ft_strcpy(dup, s1);
	ASSERT(strcmp(dup, s1) == 0);
	ft_strcpy(dup, s);
	ASSERT(strcmp(dup, s) == 0);
	return TEST_SUCCESS;
}

int test_strncpy() {
	const char s[] = "bonjour je suis la!";
	char dup[256];
	char dup1[256];
	int ret = TEST_SUCCESS;

	memset(dup, 42, sizeof(dup));
	memset(dup1, 42, sizeof(dup1));

	ft_strncpy(dup, s, 0);
	strncpy(dup1, s, 0);
	DUMP_MEM(dup, sizeof(dup));
	DUMP_MEM(dup1, sizeof(dup1));
	ASSERT_RET(memcmp(dup, dup1, sizeof(dup)) == 0, &ret);

	ft_strncpy(dup, s, 5);
	strncpy(dup1, s, 5);
	ASSERT_RET(memcmp(dup, dup1, sizeof(dup)) == 0, &ret);

	ft_strncpy(dup, s, 42);
	strncpy(dup1, s, 42);
	ASSERT_RET(memcmp(dup, dup1, sizeof(dup)) == 0, &ret);

	return ret;
}

int test_strcat() {
	const char s[] = "bonjour je suis la!";
	char dup[256];
	char dup1[256];

	memset(dup, 0, sizeof(dup));
	memset(dup1, 0, sizeof(dup1));

	memcpy(dup, s, sizeof(s));
	memcpy(dup1, s, sizeof(s));

	ft_strcat(dup, s);
	ft_strcat(dup1, s);
	ASSERT(strcmp(dup, dup1) == 0);

	dup[5] = 0;
	dup1[5] = 0;
	ft_strcat(dup, s);
	ft_strcat(dup1, s);
	ASSERT(strcmp(dup, dup1) == 0);

	return TEST_SUCCESS;
}

int test_strncat() {
	const char s[] = "bonjour je suis la!";
	char dup[256];
	char dup1[256];

	memset(dup, 0, sizeof(dup));
	memset(dup1, 0, sizeof(dup1));

	memcpy(dup, s, sizeof(s));
	memcpy(dup1, s, sizeof(s));

	ft_strncat(dup, s, 10);
	strncat(dup1, s, 10);
	ASSERT(strcmp(dup, dup1) == 0);

	memset(dup + 5, 42, sizeof(dup) - 5);
	memset(dup1 + 5, 42, sizeof(dup1) - 5);

	dup[5] = 0;
	dup1[5] = 0;
	ft_strncat(dup, s, 128);
	strncat(dup1, s, 128);
	ASSERT(memcmp(dup, dup1, sizeof(dup)) == 0);

	memset(dup + 5, 42, sizeof(dup) - 5);
	memset(dup1 + 5, 42, sizeof(dup1) - 5);

	dup[5] = 0;
	dup1[5] = 0;
	ft_strncat(dup, s, 4);
	strncat(dup1, s, 4);
	ASSERT(memcmp(dup, dup1, sizeof(dup)) == 0);

	return TEST_SUCCESS;
}

#ifdef __FreeBSD__

int test_strlcat() {
	const char s[] = "bonjour je suis la!";
	char buf[128];
	char buf1[128];
	int ret = TEST_SUCCESS;
	size_t r1, r2;

	memset(buf, 0, sizeof(buf));
	memset(buf1, 0, sizeof(buf1));

	strcpy(buf, s);
	strcpy(buf1, s);
	ASSERT_RET(strlcat(buf, s, sizeof(buf)) == ft_strlcat(buf1, s, sizeof(buf1)), &ret);
	DUMP_MEM(buf, sizeof(buf));
	DUMP_MEM(buf1, sizeof(buf));
	ASSERT_RET(memcmp(buf, buf1, sizeof(buf)) == 0, &ret);

	strcpy(buf, s);
	strcpy(buf1, s);
	r1 = strlcat(buf, s, (sizeof(s) + sizeof(s) / 2));
	r2 = ft_strlcat(buf1, s, (sizeof(s) + sizeof(s) / 2));
	DUMP_MEM(buf, sizeof(buf));
	DUMP_MEM(buf1, sizeof(buf));
	logf("r1: %zu, r2: %zu\n", r1, r2);
	ASSERT_RET(r1 == r2, &ret);
	ASSERT_RET(memcmp(buf, buf1, sizeof(buf)) == 0, &ret);

	strcpy(buf, "un deux trois");
	strcpy(buf1, "un deux trois");
	r1 = strlcat(buf, s, 5);
	r2 = ft_strlcat(buf1, s, 5);
	logf("r1: %zu, r2: %zu\n", r1, r2);
	ASSERT_RET(r1 == r2, &ret);
	ASSERT_RET(memcmp(buf, buf1, sizeof(buf)) == 0, &ret);

	strcpy(buf, s);
	strcpy(buf1, s);
	ASSERT_RET(strlcat(buf, s, 0) == ft_strlcat(buf1, s, 0), &ret);
	ASSERT_RET(memcmp(buf, buf1, sizeof(buf)) == 0, &ret);

	return ret;
}

int test_strnstr() {
	const char s[] = "bonjour je suis la!";
	int ret = TEST_SUCCESS;

	ASSERT_RET(ft_strnstr(s, "bonjour", sizeof(s)) == strnstr(s, "bonjour", sizeof(s)), &ret);
	ASSERT_RET(ft_strnstr(s, "je", sizeof(s)) == strnstr(s, "je", sizeof(s)), &ret);
	ASSERT_RET(ft_strnstr(s, "la!", sizeof(s)) == strnstr(s, "la!", sizeof(s)), &ret);
	ASSERT_RET(ft_strnstr(s, "?", sizeof(s)) == strnstr(s, "?", sizeof(s)), &ret);

	ASSERT_RET(ft_strnstr(s, "bonjour", 5) == strnstr(s, "bonjour", 5), &ret);
	ASSERT_RET(ft_strnstr(s, "je", 5) == strnstr(s, "je", 5), &ret);
	ASSERT_RET(ft_strnstr(s, "la!", 5) == strnstr(s, "la!", 5), &ret);
	ASSERT_RET(ft_strnstr(s, "?", 5) == strnstr(s, "?", 5), &ret);

	ASSERT_RET(ft_strnstr(s, "bonjour", 0) == strnstr(s, "bonjour", 0), &ret);
	ASSERT_RET(ft_strnstr(s, "je", 0) == strnstr(s, "je", 0), &ret);
	ASSERT_RET(ft_strnstr(s, "la!", 0) == strnstr(s, "la!", 0), &ret);
	ASSERT_RET(ft_strnstr(s, "?", 0) == strnstr(s, "?", 0), &ret);

	return ret;
}

#endif

int test_strchr() {
	const char s[] = "bonjour je suis la!";
	int ret = TEST_SUCCESS;

	ASSERT_RET(ft_strchr(s, 'b') == strchr(s, 'b'), &ret);
	ASSERT_RET(ft_strchr(s, 'j') == strchr(s, 'j'), &ret);
	ASSERT_RET(ft_strchr(s, '!') == strchr(s, '!'), &ret);
	ASSERT_RET(ft_strchr(s, '?') == strchr(s, '?'), &ret);

	return ret;
}

int test_strrchr() {
	const char s[] = "bonjour je suis la!";
	int ret = TEST_SUCCESS;

	ASSERT_RET(ft_strrchr(s, 'b') == strrchr(s, 'b'), &ret);
	ASSERT_RET(ft_strrchr(s, 'j') == strrchr(s, 'j'), &ret);
	ASSERT_RET(ft_strrchr(s, '!') == strrchr(s, '!'), &ret);
	ASSERT_RET(ft_strrchr(s, '?') == strrchr(s, '?'), &ret);

	return ret;
}

int test_strstr() {
	const char s[] = "bonjour je suis la!";
	int ret = TEST_SUCCESS;

	ASSERT_RET(ft_strstr(s, "bonjour") == strstr(s, "bonjour"), &ret);
	ASSERT_RET(ft_strstr(s, "je") == strstr(s, "je"), &ret);
	ASSERT_RET(ft_strstr(s, "la!") == strstr(s, "la!"), &ret);
	ASSERT_RET(ft_strstr(s, "?") == strstr(s, "?"), &ret);

	return ret;
}

int test_strncmp() {
	char s1[] = "bonjour je suis la!";
	char s2[] = "bonjour je suis la!";
	char s3[] = "bonjour je suis la?";
	int ret = TEST_SUCCESS;

	ASSERT_RET(ft_strncmp(s1, s1, sizeof(s1)) == strncmp(s1, s1, sizeof(s1)), &ret);
	ASSERT_RET(ft_strncmp(s1, s2, sizeof(s1)) == strncmp(s1, s2, sizeof(s1)), &ret);
	ASSERT_RET(ft_strncmp(s1, s3, sizeof(s1)) == strncmp(s1, s3, sizeof(s1)), &ret);

	ASSERT_RET(ft_strncmp(s1, s1 + 5, sizeof(s1)) == strncmp(s1, s1 + 5, sizeof(s1)), &ret);
	ASSERT_RET(ft_strncmp(s1, s2 + 5, sizeof(s1)) == strncmp(s1, s2 + 5, sizeof(s1)), &ret);
	ASSERT_RET(ft_strncmp(s1, s3 + 5, sizeof(s1)) == strncmp(s1, s3 + 5, sizeof(s1)), &ret);

	ASSERT_RET(ft_strncmp(s1, s1, 5) == strncmp(s1, s1, 5), &ret);
	ASSERT_RET(ft_strncmp(s1, s2, 5) == strncmp(s1, s2, 5), &ret);
	ASSERT_RET(ft_strncmp(s1, s3, 5) == strncmp(s1, s3, 5), &ret);

	ASSERT_RET(ft_strncmp(s1, s1 + 1, 0) == strncmp(s1, s1 + 1, 0), &ret);
	ASSERT_RET(ft_strncmp(s1, s2 + 1, 0) == strncmp(s1, s2 + 1, 0), &ret);
	ASSERT_RET(ft_strncmp(s1, s3 + 1, 0) == strncmp(s1, s3 + 1, 0), &ret);

	return ret;
}

int test_strnew() {
	char	*s = NULL;
	int ret = TEST_SUCCESS;

	s = ft_strnew(0);
	ASSERT_RET(s != NULL, &ret);
	ASSERT_RET(s[0] == 0, &ret);

	free(s);
	s = ft_strnew(42);
	ASSERT_RET(s != NULL, &ret);
	for(size_t i = 0; i < 43; i++) {
		if (s[i] != 0)
			ret = TEST_FAILED;
	}

	return ret;
}

int test_strclr() {
	char buf[128];

	memset(buf, 42, 128);
	ASSERT(memchr(buf, 42, 128));

	return TEST_SUCCESS;
}

static void iter_toupper(char *c) {
	*c = toupper(*c);
}

static void iteri_toupper(unsigned int i, char *c) {
	if (i % 2 == 0)
		return ;
	*c = toupper(*c);
}

static char mapi_toupper(unsigned int i, char c) {
	if (i % 2 == 0)
		return c;
	return toupper(c);
}

int test_striter() {
	char buf[] = "bonjour je suis la!";

	ft_striter(buf, iter_toupper);
	ASSERT(strcmp(buf, "BONJOUR JE SUIS LA!") == 0);
	return TEST_SUCCESS;
}

int test_striteri() {
	char buf[] = "bonjour je suis la!";

	ft_striteri(buf, iteri_toupper);
	ASSERT(strcmp(buf, "bOnJoUr jE SuIs lA!") == 0);
	return TEST_SUCCESS;
}

int test_strmap() {
	char buf[] = "bonjour je suis la!";
	char	*s;

	s = ft_strmap(buf, (void*)ft_toupper);
	ASSERT(strcmp(s, "BONJOUR JE SUIS LA!") == 0);
	return TEST_SUCCESS;
}

int test_strmapi() {
	char buf[] = "bonjour je suis la!";
	char	*s;

	s = ft_strmapi(buf, mapi_toupper);
	ASSERT(strcmp(s, "bOnJoUr jE SuIs lA!") == 0);
	return TEST_SUCCESS;
}

int test_strequ() {
	int ret = TEST_SUCCESS;

	ASSERT_RET(ft_strequ(S1, S1) == 1, &ret);
	ASSERT_RET(ft_strequ(S1, S2) == 0, &ret);
	ASSERT_RET(ft_strequ(S2, S1) == 0, &ret);
	ASSERT_RET(ft_strequ(S2, S2) == 1, &ret);

	ASSERT_RET(ft_strequ(S2, "\0") == 0, &ret);
	ASSERT_RET(ft_strequ("\0", "\0") == 1, &ret);
	ASSERT_RET(ft_strequ("\0", S2) == 0, &ret);

	ASSERT_RET(ft_strequ(S1 S2, S1 S2) == 1, &ret);
	return ret;
}

int test_strnequ() {
	int ret = TEST_SUCCESS;

	ASSERT_RET(ft_strnequ(S1, S1, sizeof(S1)) == 1, &ret);
	ASSERT_RET(ft_strnequ(S1, S2, sizeof(S2)) == 0, &ret);
	ASSERT_RET(ft_strnequ(S1, S2, 0) == 1, &ret);
	ASSERT_RET(ft_strnequ(S2, S1, sizeof(S2)) == 0, &ret);
	ASSERT_RET(ft_strnequ(S2, S2, sizeof(S2)) == 1, &ret);

	ASSERT_RET(ft_strnequ(S2, "\0", sizeof(S2)) == 0, &ret);
	ASSERT_RET(ft_strnequ(S2, "\0", 0) == 1, &ret);
	ASSERT_RET(ft_strnequ("\0", "\0", 1) == 1, &ret);
	ASSERT_RET(ft_strnequ("\0", S2, sizeof(S2)) == 0, &ret);

	ASSERT_RET(ft_strnequ(S1 S2, S1 S2, sizeof(S1 S2)) == 1, &ret);

	return ret;
}

int test_strsub() {
	const char t[] = "bonjour je suis la!";
	char *s;
	int ret = TEST_SUCCESS;

	ASSERT((s = ft_strsub(t, 5, 5)) != NULL);
	ASSERT_RET(strncmp(s, t + 5, 5) == 0, &ret);
	free(s);

	ASSERT((s = ft_strsub(t, 0, 0)) != NULL);
	ASSERT_RET(s[0] == 0, &ret);
	free(s);

	ASSERT((s = ft_strsub(t, 0, sizeof(t))) != NULL);
	ASSERT_RET(strcmp(s, t) == 0, &ret);
	free(s);
	return ret;
}

int test_strjoin() {
	char *s;
	int ret = TEST_SUCCESS;

	ASSERT((s = ft_strjoin(S1, S2)) != NULL);
	ASSERT_RET(strcmp(s, S1 S2) == 0, &ret);
	free(s);

	ASSERT((s = ft_strjoin(S2, S1)) != NULL);
	ASSERT_RET(strcmp(s, S2 S1) == 0, &ret);
	free(s);

	return ret;
}

int test_strtrim() {
	char 	*s;
	char 	*r;
	int ret = TEST_SUCCESS;

	s = "bonjour je suis la!";
	r = ft_strtrim(s);
#ifdef __FreeBSD__
	DUMP_MEM(r, malloc_size(r));
#endif
	DUMP_MEM(s, 20);
	ASSERT(strcmp(r, s) == 0);

	s = "  bonjour je suis la!  ";
	free(r);
	r = ft_strtrim(s);
	ASSERT(strcmp(r, "bonjour je suis la!") == 0);

	free(r);
	return ret;
}

int test_strsplit() {
	const char s[] = "bonjour je suis la!";
	char	*expect[] = { "bonjour", "je", "suis", "la!", NULL };
	char	**r;
	size_t i = 0;

	r = ft_strsplit(s, ' ');
	for (; expect[i] != NULL; i++) {
		ASSERT(r[i] != NULL);
		ASSERT(strcmp(expect[i], r[i]) == 0);
	}
	ASSERT(r[i] == NULL);
	return TEST_SUCCESS;
}

int test_strspn() {
	const char s[] = "bonjour je suis la!";
	int ret = TEST_SUCCESS;

	ASSERT_RET(ft_strspn(s, "b") == strspn(s, "b"), &ret);
	ASSERT_RET(ft_strspn(s, "j") == strspn(s, "j"), &ret);
	ASSERT_RET(ft_strspn(s, "!") == strspn(s, "!"), &ret);
	ASSERT_RET(ft_strspn(s, "?") == strspn(s, "?"), &ret);

	return ret;
}

int test_strcspn() {
	const char s[] = "bonjour je suis la!";
	int ret = TEST_SUCCESS;

	ASSERT_RET(ft_strcspn(s, "b") == strcspn(s, "b"), &ret);
	ASSERT_RET(ft_strcspn(s, "j") == strcspn(s, "j"), &ret);
	ASSERT_RET(ft_strcspn(s, "!") == strcspn(s, "!"), &ret);
	ASSERT_RET(ft_strcspn(s, "?") == strcspn(s, "?"), &ret);

	return ret;
}


t_test string_suite[] = {
	TEST(test_strdup),
	TEST(test_strndup),
	TEST(test_strcpy),
	TEST(test_strncpy),
	TEST(test_strcat),
	TEST(test_strncat),
#ifdef __FreeBSD__
	TEST(test_strlcat),
	TEST(test_strnstr),
#endif
	TEST(test_strchr),
	TEST(test_strrchr),
	TEST(test_strstr),
	TEST(test_strncmp),
	TEST(test_strnew),
	TEST(test_strclr),
	TEST(test_striter),
	TEST(test_striteri),
	TEST(test_strmap),
	TEST(test_strmapi),
	TEST(test_strequ),
	TEST(test_strnequ),
	TEST(test_strsub),
	TEST(test_strjoin),
	TEST(test_strtrim),
	TEST(test_strsplit),
	TEST(test_strspn),
	TEST(test_strcspn),
	NULL_TEST
};
