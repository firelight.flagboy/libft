/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   strcmp.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/23 16:40:58 by fbenneto          #+#    #+#             */
/*   Updated: 2019/10/24 10:11:57 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libunit.h"

#include <string.h>
#include "libft/string.h"
#include "test_string.h"

static int template(const char *s1, const char *s2) {
    if (strcmp(s1, s2) == ft_strcmp(s1, s2))
        return TEST_SUCCESS;
    return TEST_FAILED;
}

int strcmp_test_empty_string() {
    return template("", "");
}

int strcmp_test_equal() {
    return template("bonjour je suis la", "bonjour je suis la");
}

int strcmp_test_equal_s1() {
    return template("bonjour je suis", "bonjour je suis la");
}

int strcmp_test_diff_s2() {
    return template("bonjour je suis la", "bonjour je suis");
}

t_test strcmp_suite[] = {
    TEST(strcmp_test_empty_string),
    TEST(strcmp_test_equal),
    TEST(strcmp_test_equal_s1),
    TEST(strcmp_test_diff_s2),
    NULL_TEST
};
