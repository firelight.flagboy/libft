#include "libft/string.h"
#include "libunit.h"
#include "test_string.h"

#define STRING "bonjour je suis une string"

int strlen_test_empty_string() {
    if (ft_strlen("") == 0)
        return TEST_SUCCESS;
    return TEST_FAILED;
}

int strlen_test_normal_string() {
    if (ft_strlen(STRING) == sizeof(STRING) - 1)
        return TEST_SUCCESS;
    return TEST_FAILED;
}

t_test strlen_suite[] = {
    TEST(strlen_test_empty_string),
    TEST(strlen_test_normal_string),
    NULL_TEST
};
