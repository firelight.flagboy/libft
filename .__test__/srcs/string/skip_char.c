#include "test_string.h"

static int test_str_skip_char_f_template(const char *input, void *skip, off_t want, int *return_value) {
	const char *got = ft_str_skip_char_f(input, skip);

	if (got == input + want)
		return TEST_SUCCESS;
	logf("got '%1$s'(%1$p) expected '%2$s'(+%3$lx) for '%4$s'(%4$p)\n", got, input + want, want, input);
	*return_value = TEST_FAILED;
	return TEST_FAILED;
}

static int test_str_skip_char_template(const char *input, int skip, off_t want, int *return_value) {
	const char *got = ft_str_skip_char(input, skip);

	if (got == input + want)
		return TEST_SUCCESS;
	logf("got '%1$s'(%1$p) expected '%2$s'(+%3$lx) for '%4$s'(%4$p)\n", got, input + want, want, input);
	*return_value = TEST_FAILED;
	return TEST_FAILED;
}

int	test_str_skip_char_f() {
	int res = TEST_SUCCESS;

	test_str_skip_char_f_template("", isspace, 0, &res);
	test_str_skip_char_f_template("bonjour", isspace, 0, &res);
	test_str_skip_char_f_template("    bonjour", isspace, 4, &res);

	return res;
}

int	test_str_skip_char() {
	int res = TEST_SUCCESS;

	test_str_skip_char_template("", ' ', 0, &res);
	test_str_skip_char_template("bonjour", ' ', 0, &res);
	test_str_skip_char_template("    bonjour", ' ', 4, &res);

	return res;
}

t_test skip_char_suite[] = {
	TEST(test_str_skip_char_f),
	TEST(test_str_skip_char),
	NULL_TEST,
};
