#include <test_mem.h>
#include <libft/string.h>
#include <string.h>

int test_bzero() {
	char *buff __attribute__((cleanup(ft_strdel)));

	buff = malloc(4096);
	memset(buff, 42, 4096);

	ft_bzero(buff, 0);
	ASSERT(memchr(buff, 0, 4096) == NULL);

	ft_bzero(buff, 4096);
	ASSERT(memchr(buff, 42, 4096) == NULL);

	return TEST_SUCCESS;
}

int test_memalloc() {
	char *buff __attribute__((cleanup(ft_strdel)));

	buff = ft_memalloc(4096);
	for (size_t i = 0; i < 4096; i++) {
		if (buff[i] != 0)
			return TEST_FAILED;
	}
	return TEST_SUCCESS;
}

int test_memccpy() {
	char	src[] = "bonjour je suis la!";
	char	dest1[1024];
	char	dest2[1024];
	void	*value;
	int		ret = TEST_SUCCESS;

	memset(dest1, 0, 1024);
	memset(dest2, 0, 1024);
	memccpy(dest1, src, 'b', sizeof(src));
	value = ft_memccpy(dest2, src, 'b', sizeof(src));
	ASSERT_RET(value == dest2 + strcspn(src, "b") + 1, &ret);
	ASSERT_RET(memcmp(dest1, dest2, sizeof(src)) == 0, &ret);

	memccpy(dest1, src, 'j', sizeof(src));
	value = ft_memccpy(dest2, src, 'j', sizeof(src));
	ASSERT_RET(value == dest2 + strcspn(src, "j") + 1, &ret);
	ASSERT_RET(memcmp(dest1, dest2, sizeof(src)) == 0, &ret);

	memset(dest1, 0, 1024);
	memset(dest2, 0, 1024);
	memccpy(dest1, src, '?', sizeof(src));
	value = ft_memccpy(dest2, src, '?', sizeof(src));
	ASSERT_RET(value == NULL, &ret);
	ASSERT_RET(memcmp(dest1, dest2, sizeof(src)) == 0, &ret);

	return ret;
}

int test_memchr() {
	char	s[] = "bonjour je suis la!";
	int		ret = TEST_SUCCESS;

	ASSERT_RET(ft_memchr(s, 'b', sizeof(s)) == memchr(s, 'b', sizeof(s)), &ret);
	ASSERT_RET(ft_memchr(s, 'j', sizeof(s)) == memchr(s, 'j', sizeof(s)), &ret);
	ASSERT_RET(ft_memchr(s, '!', sizeof(s)) == memchr(s, '!', sizeof(s)), &ret);
	ASSERT_RET(ft_memchr(s, '?', sizeof(s)) == memchr(s, '?', sizeof(s)), &ret);

	return ret;
}

int test_memcmp() {
	char s1[] = "bonjour je suis la!";
	char s2[] = "bonjour je suis la!";
	char s3[] = "bonjour je suis la?";
	int ret = TEST_SUCCESS;

	ASSERT_RET(ft_memcmp(s1, s2, sizeof(s1)) == memcmp(s1, s2, sizeof(s1)), &ret);
	ASSERT_RET(ft_memcmp(s2, s2, sizeof(s2)) == memcmp(s2, s2, sizeof(s2)), &ret);
	ASSERT_RET(ft_memcmp(s3, s2, sizeof(s2)) == memcmp(s3, s2, sizeof(s2)), &ret);
	ASSERT_RET(ft_memcmp(s3, s2, 0) == memcmp(s3, s2, 0), &ret);

	return ret;
}

int test_memcpy() {
	char	src[] = "bonjour je suis la!";
	char	dest1[256];
	char	dest2[256];
	void	*value;
	int		ret = TEST_SUCCESS;

	memset(dest1, 0, 256);
	memset(dest2, 0, 256);
	memcpy(dest1, src, sizeof(src));
	value = ft_memcpy(dest2, src, sizeof(src));
	ASSERT_RET(value == dest2, &ret);
	ASSERT_RET(memcmp(dest1, dest2, sizeof(src)) == 0, &ret);

	memset(dest1, 0, 256);
	memset(dest2, 0, 256);
	memcpy(dest1, src, 0);
	value = ft_memcpy(dest2, src, 0);
	ASSERT_RET(value == dest2, &ret);
	ASSERT_RET(memcmp(dest1, dest2, sizeof(src)) == 0, &ret);

	return ret;
}

int test_memmove() {
	const char	test[] = "bonjour je suis la!";
	char		buff[1024];
	char		buff1[1024];
	int			ret = TEST_SUCCESS;

	memcpy(buff, test, sizeof(test));
	memcpy(buff1, test, sizeof(test));

	ASSERT_RET(memcmp(
			ft_memmove(buff, buff, sizeof(test)),
			memmove(buff1, buff1, sizeof(test)), sizeof(test)) == 0,
		&ret);
	ASSERT_RET(memcmp(
			ft_memmove(buff + 42, buff, sizeof(test)),
			memmove(buff1 + 42, buff1, sizeof(test)), sizeof(test)) == 0,
		&ret);
	ASSERT_RET(memcmp(
			ft_memmove(buff + 21, buff + 42, sizeof(test)),
			memmove(buff1 + 21, buff1 + 42, sizeof(test)), sizeof(test)) == 0,
		&ret);
	ASSERT_RET(memcmp(
			ft_memmove(buff + 40, buff + 42, sizeof(test)),
			memmove(buff1 + 40, buff1 + 42, sizeof(test)), sizeof(test)) == 0,
		&ret);
	ASSERT_RET(memcmp(
			ft_memmove(buff + 2, buff, sizeof(test)),
			memmove(buff1 + 2, buff1, sizeof(test)), sizeof(test)) == 0,
		&ret);

	return ret;
}

int test_memset() {
	char *buff __attribute__((cleanup(ft_strdel)));

	buff = malloc(4096);
	memset(buff, 0, 4096);

	ft_memset(buff, 42, 0);
	ASSERT(memchr(buff, 42, 4096) == NULL);

	ft_memset(buff, 42, 4096);
	ASSERT(memchr(buff, 0, 4096) == NULL);

	return TEST_SUCCESS;
}

t_test mem_suite[] = {
	TEST(test_bzero),
	TEST(test_memalloc),
	TEST(test_memccpy),
	TEST(test_memchr),
	TEST(test_memcmp),
	TEST(test_memcpy),
	TEST(test_memmove),
	TEST(test_memset),
	NULL_TEST,
};
