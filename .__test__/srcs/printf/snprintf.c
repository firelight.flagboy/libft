#include "test_printf.h"
#include <limits.h>

int test_d() {
	char *format;
	char buff[512];
	char buff1[512];
	int ret = TEST_SUCCESS;
	int num;

	format = "%d";
	num = 42;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MAX;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MIN;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);

	format = "%0d";
	num = 42;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MAX;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MIN;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);

	format = "%.0d";
	num = 42;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MAX;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MIN;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);

	format = "%-d";
	num = 42;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MAX;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MIN;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);

	format = "%5d";
	num = 42;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MAX;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MIN;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);

	format = "%.5d";
	num = 42;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MAX;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MIN;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);

	format = "%-5d";
	num = 42;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MAX;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MIN;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);

	return ret;
}

int test_u() {
	char *format;
	char buff[512];
	char buff1[512];
	int ret = TEST_SUCCESS;
	int num;

	format = "%u";
	num = 42;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MAX;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MIN;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);

	format = "%0u";
	num = 42;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MAX;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MIN;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);

	format = "%.0u";
	num = 42;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MAX;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MIN;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);

	format = "%-u";
	num = 42;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MAX;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MIN;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);

	format = "%5u";
	num = 42;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MAX;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MIN;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);

	format = "%.5u";
	num = 42;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MAX;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MIN;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);

	format = "%-5u";
	num = 42;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MAX;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MIN;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);

	return ret;
}

int test_x() {
	char *format;
	char buff[512];
	char buff1[512];
	int ret = TEST_SUCCESS;
	int num;

	format = "%x";
	num = 42;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MAX;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MIN;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);

	format = "%0x";
	num = 42;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MAX;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MIN;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);

	format = "%.0x";
	num = 42;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MAX;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MIN;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);

	format = "%-x";
	num = 42;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MAX;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MIN;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);

	format = "%5x";
	num = 42;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MAX;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MIN;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);

	format = "%.5x";
	num = 42;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MAX;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MIN;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);

	format = "%-5x";
	num = 42;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MAX;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MIN;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);

	return ret;
}

int test_o() {
	char *format;
	char buff[512];
	char buff1[512];
	int ret = TEST_SUCCESS;
	int num;

	format = "%o";
	num = 42;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MAX;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MIN;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);

	format = "%0o";
	num = 42;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MAX;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MIN;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);

	format = "%.0o";
	num = 42;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MAX;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MIN;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);

	format = "%-o";
	num = 42;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MAX;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MIN;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);

	format = "%5o";
	num = 42;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MAX;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MIN;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);

	format = "%.5o";
	num = 42;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MAX;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MIN;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);

	format = "%-5o";
	num = 42;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MAX;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MIN;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);

	return ret;
}

int test_i() {
	char *format;
	char buff[512];
	char buff1[512];
	int ret = TEST_SUCCESS;
	int num;

	format = "%i";
	num = 42;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MAX;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MIN;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);

	format = "%0i";
	num = 42;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MAX;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MIN;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);

	format = "%.0i";
	num = 42;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MAX;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MIN;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);

	format = "%-i";
	num = 42;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MAX;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MIN;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);

	format = "%5i";
	num = 42;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MAX;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MIN;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);

	format = "%.5i";
	num = 42;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MAX;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MIN;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);

	format = "%-5i";
	num = 42;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MAX;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MIN;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);

	return ret;
}

int test_X() {
	char *format;
	char buff[512];
	char buff1[512];
	int ret = TEST_SUCCESS;
	int num;

	format = "%X";
	num = 42;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MAX;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MIN;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);

	format = "%0X";
	num = 42;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MAX;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MIN;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);

	format = "%.0X";
	num = 42;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MAX;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MIN;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);

	format = "%-X";
	num = 42;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MAX;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MIN;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);

	format = "%5X";
	num = 42;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MAX;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MIN;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);

	format = "%.5X";
	num = 42;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MAX;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MIN;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);

	format = "%-5X";
	num = 42;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MAX;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MIN;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);

	return ret;
}

int test_D() {
	char *format;
	char buff[512];
	char buff1[512];
	int ret = TEST_SUCCESS;
	int num;

	format = "%D";
	num = 42;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MAX;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MIN;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);

	format = "%0D";
	num = 42;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MAX;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MIN;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);

	format = "%.0D";
	num = 42;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MAX;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MIN;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);

	format = "%-D";
	num = 42;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MAX;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MIN;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);

	format = "%5D";
	num = 42;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MAX;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MIN;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);

	format = "%.5D";
	num = 42;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MAX;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MIN;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);

	format = "%-5D";
	num = 42;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MAX;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);
	num = INT_MIN;
	ASSERT_RET(snprintf(buff, 512, format, num) == ft_snprintf(buff1, 512, format, num), &ret);
	ASSERT_RET(strcmp(buff, buff1) == 0, &ret);

	return ret;
}

int test_c() {
	char *format;
	char c;
	char buff[256];
	char buff1[256];
	int ret = TEST_SUCCESS;

	memset(buff, 0, 256);
	memset(buff1, 0, 256);

	format = "%c";
	c = 'a';
	ASSERT_RET(snprintf(buff, 256, format, c) == ft_snprintf(buff1, 256, format, c), &ret);
	ASSERT_RET(memcmp(buff, buff1, 256) == 0, &ret);
	c = '\0';
	ASSERT_RET(snprintf(buff, 256, format, c) == ft_snprintf(buff1, 256, format, c), &ret);
	ASSERT_RET(memcmp(buff, buff1, 256) == 0, &ret);
	c = '\n';
	ASSERT_RET(snprintf(buff, 256, format, c) == ft_snprintf(buff1, 256, format, c), &ret);
	ASSERT_RET(memcmp(buff, buff1, 256) == 0, &ret);
	c = '\t';
	ASSERT_RET(snprintf(buff, 256, format, c) == ft_snprintf(buff1, 256, format, c), &ret);
	ASSERT_RET(memcmp(buff, buff1, 256) == 0, &ret);

	format = "%5c";
	c = 'a';
	ASSERT_RET(snprintf(buff, 256, format, c) == ft_snprintf(buff1, 256, format, c), &ret);
	ASSERT_RET(memcmp(buff, buff1, 256) == 0, &ret);
	c = '\0';
	ASSERT_RET(snprintf(buff, 256, format, c) == ft_snprintf(buff1, 256, format, c), &ret);
	ASSERT_RET(memcmp(buff, buff1, 256) == 0, &ret);
	c = '\n';
	ASSERT_RET(snprintf(buff, 256, format, c) == ft_snprintf(buff1, 256, format, c), &ret);
	ASSERT_RET(memcmp(buff, buff1, 256) == 0, &ret);
	c = '\t';
	ASSERT_RET(snprintf(buff, 256, format, c) == ft_snprintf(buff1, 256, format, c), &ret);
	ASSERT_RET(memcmp(buff, buff1, 256) == 0, &ret);

	format = "%-5c";
	c = 'a';
	ASSERT_RET(snprintf(buff, 256, format, c) == ft_snprintf(buff1, 256, format, c), &ret);
	ASSERT_RET(memcmp(buff, buff1, 256) == 0, &ret);
	c = '\0';
	ASSERT_RET(snprintf(buff, 256, format, c) == ft_snprintf(buff1, 256, format, c), &ret);
	ASSERT_RET(memcmp(buff, buff1, 256) == 0, &ret);
	c = '\n';
	ASSERT_RET(snprintf(buff, 256, format, c) == ft_snprintf(buff1, 256, format, c), &ret);
	ASSERT_RET(memcmp(buff, buff1, 256) == 0, &ret);
	c = '\t';
	ASSERT_RET(snprintf(buff, 256, format, c) == ft_snprintf(buff1, 256, format, c), &ret);
	ASSERT_RET(memcmp(buff, buff1, 256) == 0, &ret);

	format = "%05c";
	c = 'a';
	ASSERT_RET(snprintf(buff, 256, format, c) == ft_snprintf(buff1, 256, format, c), &ret);
	ASSERT_RET(memcmp(buff, buff1, 256) == 0, &ret);
	c = '\0';
	ASSERT_RET(snprintf(buff, 256, format, c) == ft_snprintf(buff1, 256, format, c), &ret);
	ASSERT_RET(memcmp(buff, buff1, 256) == 0, &ret);
	c = '\n';
	ASSERT_RET(snprintf(buff, 256, format, c) == ft_snprintf(buff1, 256, format, c), &ret);
	ASSERT_RET(memcmp(buff, buff1, 256) == 0, &ret);
	c = '\t';
	ASSERT_RET(snprintf(buff, 256, format, c) == ft_snprintf(buff1, 256, format, c), &ret);
	ASSERT_RET(memcmp(buff, buff1, 256) == 0, &ret);

	format = "%.5c";
	c = 'a';
	ASSERT_RET(snprintf(buff, 256, format, c) == ft_snprintf(buff1, 256, format, c), &ret);
	ASSERT_RET(memcmp(buff, buff1, 256) == 0, &ret);
	c = '\0';
	ASSERT_RET(snprintf(buff, 256, format, c) == ft_snprintf(buff1, 256, format, c), &ret);
	ASSERT_RET(memcmp(buff, buff1, 256) == 0, &ret);
	c = '\n';
	ASSERT_RET(snprintf(buff, 256, format, c) == ft_snprintf(buff1, 256, format, c), &ret);
	ASSERT_RET(memcmp(buff, buff1, 256) == 0, &ret);
	c = '\t';
	ASSERT_RET(snprintf(buff, 256, format, c) == ft_snprintf(buff1, 256, format, c), &ret);
	ASSERT_RET(memcmp(buff, buff1, 256) == 0, &ret);

	return ret;
}

int test_s() {
	char *format;
	char *s;
	char buff[256];
	char buff1[256];
	int ret = TEST_SUCCESS;

	memset(buff, 0, 256);
	memset(buff1, 0, 256);

	format = "%s";
	s = "bonjour";
	ASSERT_RET(snprintf(buff, 256, format, s) == ft_snprintf(buff1, 256, format, s), &ret);
	ASSERT_RET(memcmp(buff, buff1, 256) == 0, &ret);
	s = "bonjour je suis la!";
	ASSERT_RET(snprintf(buff, 256, format, s) == ft_snprintf(buff1, 256, format, s), &ret);
	ASSERT_RET(memcmp(buff, buff1, 256) == 0, &ret);
	s = "bonjour je\0 suis la!";
	ASSERT_RET(snprintf(buff, 256, format, s) == ft_snprintf(buff1, 256, format, s), &ret);
	ASSERT_RET(memcmp(buff, buff1, 256) == 0, &ret);
	s = NULL;
	ASSERT_RET(snprintf(buff, 256, format, s) == ft_snprintf(buff1, 256, format, s), &ret);
	ASSERT_RET(memcmp(buff, buff1, 256) == 0, &ret);

	format = "%50s";
	s = "bonjour";
	ASSERT_RET(snprintf(buff, 256, format, s) == ft_snprintf(buff1, 256, format, s), &ret);
	ASSERT_RET(memcmp(buff, buff1, 256) == 0, &ret);
	s = "bonjour je suis la!";
	ASSERT_RET(snprintf(buff, 256, format, s) == ft_snprintf(buff1, 256, format, s), &ret);
	ASSERT_RET(memcmp(buff, buff1, 256) == 0, &ret);
	s = "bonjour je\0 suis la!";
	ASSERT_RET(snprintf(buff, 256, format, s) == ft_snprintf(buff1, 256, format, s), &ret);
	ASSERT_RET(memcmp(buff, buff1, 256) == 0, &ret);
	s = NULL;
	ASSERT_RET(snprintf(buff, 256, format, s) == ft_snprintf(buff1, 256, format, s), &ret);
	ASSERT_RET(memcmp(buff, buff1, 256) == 0, &ret);

	format = "%-50s";
	s = "bonjour";
	ASSERT_RET(snprintf(buff, 256, format, s) == ft_snprintf(buff1, 256, format, s), &ret);
	ASSERT_RET(memcmp(buff, buff1, 256) == 0, &ret);
	s = "bonjour je suis la!";
	ASSERT_RET(snprintf(buff, 256, format, s) == ft_snprintf(buff1, 256, format, s), &ret);
	ASSERT_RET(memcmp(buff, buff1, 256) == 0, &ret);
	s = "bonjour je\0 suis la!";
	ASSERT_RET(snprintf(buff, 256, format, s) == ft_snprintf(buff1, 256, format, s), &ret);
	ASSERT_RET(memcmp(buff, buff1, 256) == 0, &ret);
	s = NULL;
	ASSERT_RET(snprintf(buff, 256, format, s) == ft_snprintf(buff1, 256, format, s), &ret);
	ASSERT_RET(memcmp(buff, buff1, 256) == 0, &ret);

	format = "%.50s";
	s = "bonjour";
	ASSERT_RET(snprintf(buff, 256, format, s) == ft_snprintf(buff1, 256, format, s), &ret);
	ASSERT_RET(memcmp(buff, buff1, 256) == 0, &ret);
	s = "bonjour je suis la!";
	ASSERT_RET(snprintf(buff, 256, format, s) == ft_snprintf(buff1, 256, format, s), &ret);
	ASSERT_RET(memcmp(buff, buff1, 256) == 0, &ret);
	s = "bonjour je\0 suis la!";
	ASSERT_RET(snprintf(buff, 256, format, s) == ft_snprintf(buff1, 256, format, s), &ret);
	ASSERT_RET(memcmp(buff, buff1, 256) == 0, &ret);
	s = NULL;
	ASSERT_RET(snprintf(buff, 256, format, s) == ft_snprintf(buff1, 256, format, s), &ret);
	ASSERT_RET(memcmp(buff, buff1, 256) == 0, &ret);

	format = "%.0s";
	s = "bonjour";
	ASSERT_RET(snprintf(buff, 256, format, s) == ft_snprintf(buff1, 256, format, s), &ret);
	ASSERT_RET(memcmp(buff, buff1, 256) == 0, &ret);
	s = "bonjour je suis la!";
	ASSERT_RET(snprintf(buff, 256, format, s) == ft_snprintf(buff1, 256, format, s), &ret);
	ASSERT_RET(memcmp(buff, buff1, 256) == 0, &ret);
	s = "bonjour je\0 suis la!";
	ASSERT_RET(snprintf(buff, 256, format, s) == ft_snprintf(buff1, 256, format, s), &ret);
	ASSERT_RET(memcmp(buff, buff1, 256) == 0, &ret);
	s = NULL;
	ASSERT_RET(snprintf(buff, 256, format, s) == ft_snprintf(buff1, 256, format, s), &ret);
	ASSERT_RET(memcmp(buff, buff1, 256) == 0, &ret);

	format = "%.2s";
	s = "bonjour";
	ASSERT_RET(snprintf(buff, 256, format, s) == ft_snprintf(buff1, 256, format, s), &ret);
	ASSERT_RET(memcmp(buff, buff1, 256) == 0, &ret);
	s = "bonjour je suis la!";
	ASSERT_RET(snprintf(buff, 256, format, s) == ft_snprintf(buff1, 256, format, s), &ret);
	ASSERT_RET(memcmp(buff, buff1, 256) == 0, &ret);
	s = "bonjour je\0 suis la!";
	ASSERT_RET(snprintf(buff, 256, format, s) == ft_snprintf(buff1, 256, format, s), &ret);
	ASSERT_RET(memcmp(buff, buff1, 256) == 0, &ret);
	s = NULL;
	ASSERT_RET(snprintf(buff, 256, format, s) == ft_snprintf(buff1, 256, format, s), &ret);
	ASSERT_RET(memcmp(buff, buff1, 256) == 0, &ret);

	return ret;
}

t_test snprintf_suite[] = {
	TEST(test_d),
	TEST(test_u),
	TEST(test_x),
	TEST(test_o),
	TEST(test_i),
	TEST(test_X),
	TEST(test_D),
	TEST(test_c),
	TEST(test_s),
	NULL_TEST
};
