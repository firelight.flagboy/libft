#include "test_conv.h"

static int atoi_template(const char *s, int *return_value) {
	int got = ft_atoi(s);
	int want = atoi(s);

	if (got == want)
		return TEST_SUCCESS;
	logf("got %d expected %d for %s\n", got, want, s);
	*return_value = TEST_FAILED;
	return TEST_FAILED;
}

static int atol_template(const char *s, int *return_value) {
	long got = ft_atol(s);
	long want = atol(s);

	if (got == want)
		return TEST_SUCCESS;
	logf("got %ld expected %ld for %s\n", got, want, s);
	*return_value = TEST_FAILED;
	return TEST_FAILED;
}

static int atoll_template(const char *s, int *return_value) {
	long long got = ft_atoll(s);
	long long want = atoll(s);

	if (got == want)
		return TEST_SUCCESS;
	logf("got %lld expected %lld for %s\n", got, want, s);
	*return_value = TEST_FAILED;
	return TEST_FAILED;
}

int test_atoi() {
	int return_val = TEST_SUCCESS;

	atoi_template("", &return_val);
	atoi_template("0", &return_val);
	atoi_template("1", &return_val);

	atoi_template("-1", &return_val);
	atoi_template("+1", &return_val);
	atoi_template(" 1", &return_val);

	atoi_template("-10", &return_val);
	atoi_template("10", &return_val);

	atoi_template("123456789", &return_val);
	atoi_template("-123456789", &return_val);

	atoi_template("2147483647", &return_val);
	atoi_template("-2147483648", &return_val);

	return return_val;
}

int test_atol() {
	int return_val = TEST_SUCCESS;

	atol_template("", &return_val);
	atol_template("0", &return_val);
	atol_template("1", &return_val);

	atol_template("-1", &return_val);
	atol_template("+1", &return_val);
	atol_template(" 1", &return_val);

	atol_template("-10", &return_val);
	atol_template("10", &return_val);

	atol_template("123456789", &return_val);
	atol_template("-123456789", &return_val);

	atol_template("2147483647", &return_val);
	atol_template("-2147483648", &return_val);

	atol_template("9223372036854775807", &return_val);
	atol_template("-9223372036854775808", &return_val);

	return return_val;
}

int test_atoll() {
	int return_val = TEST_SUCCESS;

	atoll_template("", &return_val);
	atoll_template("0", &return_val);
	atoll_template("1", &return_val);

	atoll_template("-1", &return_val);
	atoll_template("+1", &return_val);
	atoll_template(" 1", &return_val);

	atoll_template("-10", &return_val);
	atoll_template("10", &return_val);

	atoll_template("123456789", &return_val);
	atoll_template("-123456789", &return_val);

	atoll_template("2147483647", &return_val);
	atoll_template("-2147483648", &return_val);

	atoll_template("9223372036854775807", &return_val);
	atoll_template("-9223372036854775808", &return_val);

	return return_val;
}

static int itoa_template(int input, int *return_value) {
	char	*got = ft_itoa(input);
	char	want[64];
	int			diff;

	snprintf(want, sizeof(want), "%d", input);
	diff = strcmp(got, want);
	free(got);
	if (diff == 0)
		return TEST_SUCCESS;
	logf("got '%s' expected '%s' for %d\n", got, want, input);
	*return_value = TEST_FAILED;
	return TEST_FAILED;
}

int	test_itoa() {
	int res = TEST_SUCCESS;

	itoa_template(0, &res);

	itoa_template(-1, &res);
	itoa_template(1, &res);

	itoa_template(10, &res);
	itoa_template(-10, &res);

	itoa_template(42, &res);
	itoa_template(-42, &res);

	itoa_template(123456789, &res);
	itoa_template(-123456789, &res);

	itoa_template(2147483647, &res);
	itoa_template(-2147483648, &res);

	return res;
}

t_test conv_suite[] = {
	TEST(test_atoi),
	TEST(test_atol),
	TEST(test_atoll),
	TEST(test_itoa),
	NULL_TEST,
};
