#include "test_flag.h"

static int test_option_len() {
	t_flag_option options[] = {
		{ "verbose", 'v', 0, 0, "Produce verbose output", 0, 0},
		{ "quiet", 'q', 0, 0, "Don't produce any output", 0, 0},
		{ "silent", 's', 0, FLAG_OPT_ALIAS, 0, 0, 0},
		{ "ouput", 'o', "FILE", 0, "output file", 0, 0},
		{ 0, 0, 0, 0, "The following options should be grouped together:", 0, 0 },
		{ "repeat", 'r', "COUNT", FLAG_OPT_OPTIONAL, "Repeat the ouput COUNT times", 0, 0 },
		{"TEST_NAME",    1, "NAME", FLAG_OPT_DOC, "try option doc", 0, 0},
		{ "abort", 1, 0, 0, "Abort the programme", 0, 0 },
		{ 0 },
	};

	ASSERT(flag_options_len(options) == 8);
	ASSERT(flag_options_len((t_flag_option[]){ {0} }) == 0);

	return TEST_SUCCESS;
}

static int test_check_options() {
	int ret = TEST_SUCCESS;
	t_flag_option *options;

	options = (t_flag_option[]){
		{ "verbose", 'v', 0, 0, 0, 0, 0},
		{ "verbose", 'v', 0, 0, 0, 0, 0},
		{ 0 },
	};
	ASSERT_RET(flag_check_duplicates(options) == true, &ret);

	options = (t_flag_option[]){
		{ "verbose", 'v', 0, 0, 0, 0, 0},
		{ "verbose", 'v', 0, FLAG_OPT_DOC, 0, 0, 0},
		{ 0 },
	};
	ASSERT_RET(flag_check_duplicates(options) == false, &ret);

	options = (t_flag_option[]){
		{ "verbose", 'v', 0, 0, 0, 0, 0},
		{ "verbose", 'c', 0, 0, 0, 0, 0},
		{ 0 },
	};
	ASSERT_RET(flag_check_duplicates(options) == true, &ret);

	options = (t_flag_option[]){
		{ "verbose", 'v', 0, 0, 0, 0, 0},
		{ "count", 'c', 0, 0, 0, 0, 0},
		{ 0 },
	};
	ASSERT_RET(flag_check_duplicates(options) == false, &ret);

	options = (t_flag_option[]){
		{ "verbose", 'v', 0, 0, 0, 0, 0},
		{ "test", 'v', 0, 0, 0, 0, 0},
		{ 0 },
	};
	ASSERT_RET(flag_check_duplicates(options) == true, &ret);


	return ret;
}

static int test_option_is_null() {
	int ret = TEST_SUCCESS;

	ASSERT_RET(flag_option_is_null(&(t_flag_option){ 0 }) == 1, &ret);

	ASSERT_RET(flag_option_is_null(&(t_flag_option){ "bonjour", 0, 0, 0, 0, 0, 0 }) == 0, &ret);
	ASSERT_RET(flag_option_is_null(&(t_flag_option){ 0, 'b', 0, 0, 0, 0, 0 }) == 0, &ret);
	ASSERT_RET(flag_option_is_null(&(t_flag_option){ 0, 0, "meta", 0, 0, 0, 0 }) == 0, &ret);
	ASSERT_RET(flag_option_is_null(&(t_flag_option){ 0, 0, 0, FLAG_OPT_ALIAS, 0, 0, 0 }) == 0, &ret);
	ASSERT_RET(flag_option_is_null(&(t_flag_option){ 0, 0, 0, 0, "doc", 0, 0 }) == 0, &ret);
	ASSERT_RET(flag_option_is_null(&(t_flag_option){ 0, 0, 0, 0, 0, 1, 0 }) == 0, &ret);
	ASSERT_RET(flag_option_is_null(&(t_flag_option){ 0, 0, 0, 0, 0, 0, (void*)1 }) == 0, &ret);

	return ret;
}

t_test flag_utils_suite[] = {
	TEST(test_option_is_null),
	TEST(test_option_len),
	TEST(test_check_options),
	NULL_TEST,
};
