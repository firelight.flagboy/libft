#include <ctype.h>
#include "test_type.h"
#include "libft/type.h"
#include "libunit.h"
#include <stdio.h>

static int template(int (*my_func)(int), int (*off)(int)) {
	int res = TEST_SUCCESS;

	for (int i = 0; i <= 0xff; i++) {
		if (my_func(i) != off(i)) {
			logf("got %d expected %d for %c(%#x)\n", my_func(i), off(i), i, i);
			res = TEST_FAILED;
		}
	}
	return res;
}

int type_test_isalnum() {
	return template(ft_isalnum, isalnum);
}

int type_test_isalpha() {
	return template(ft_isalpha, isalpha);
}

int type_test_isascii() {
	return template(ft_isascii, isascii);
}

int type_test_isdigit() {
	return template(ft_isdigit, isdigit);
}

int type_test_islower() {
	return template(ft_islower, islower);
}

int type_test_isprint() {
	return template(ft_isprint, isprint);
}

int type_test_isspace() {
	return template(ft_isspace, isspace);
}

int type_test_isupper() {
	return template(ft_isupper, isupper);
}

int type_test_tolower() {
	return template(ft_tolower, tolower);
}

int type_test_toupper() {
	return template(ft_toupper, toupper);
}

int type_test_isxdigit() {
	return template(ft_isxdigit, isxdigit);
}


t_test type_suite[] = {
	TEST(type_test_isalnum),
	TEST(type_test_isalpha),
	TEST(type_test_isascii),
	TEST(type_test_isdigit),
	TEST(type_test_islower),
	TEST(type_test_isprint),
	TEST(type_test_isspace),
	TEST(type_test_isupper),
	TEST(type_test_isxdigit),
	TEST(type_test_tolower),
	TEST(type_test_toupper),
	NULL_TEST,
};
