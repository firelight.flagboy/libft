/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/23 14:04:42 by fbenneto          #+#    #+#             */
/*   Updated: 2019/11/19 11:22:35 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libunit.h"
#include "test_string.h"
#include "test_type.h"
#include "test_num.h"
#include "test_conv.h"
#include "test_mem.h"
#include "test_printf.h"
#include "test_flag.h"

t_test_suite g_suite_arr[] = {
    (t_test_suite){ "strlen", strlen_suite },
    (t_test_suite){ "strcmp", strcmp_suite },
    (t_test_suite){ "count_word", count_word_suite },
    (t_test_suite){ "ctype", type_suite},
    (t_test_suite){ "num", num_suite},
    (t_test_suite){ "conv", conv_suite},
    (t_test_suite){ "skip_char", skip_char_suite},
    (t_test_suite){ "mem", mem_suite},
    (t_test_suite){ "string", string_suite},
	(t_test_suite){ "snprintf", snprintf_suite},
	(t_test_suite){ "flag_utils", flag_utils_suite},
    NULL_TEST_SUITE,
};

int	main(void) {
    setup_test_config(NULL);
    run_multi_suite(g_suite_arr);
    return show_score();
}
