#include "test_num.h"

int test_get_hex_letter() {
	const char	set[] = "0123456789abcdef";
	int			return_val = TEST_SUCCESS;
	int			res;

	for (size_t i = 0; i < sizeof(set) - 1; i++) {
		res = ft_get_hex_letter((int)i);
		if (res != set[i]) {
			logf("got %c expected %c for %zu\n", res, set[i], i);
			return_val = TEST_FAILED;
		}
	}
	return return_val;
}

static int test_lendigit_template(const char *s, size_t want, int *res) {
	size_t	got;

	got = ft_lendigit(s);
	if (got == want)
		return TEST_SUCCESS;
	logf("got %zu expected %zu for '%s'\n", got, want, s);
	*res = TEST_FAILED;
	return TEST_FAILED;
}

int	test_lendigit() {
	int	return_value = TEST_SUCCESS;

	test_lendigit_template("", 0, &return_value);
	test_lendigit_template("1", 1, &return_value);
	test_lendigit_template("123456789", 9, &return_value);

	return return_value;
}

static int test_len_uint_template(size_t num, uint radix, ssize_t want, int *res) {
	ssize_t got = ft_len_uint(num, radix);

	if (got == want)
		return TEST_SUCCESS;
	logf("got %zd expected %zd for (%zd, %u)\n", got, want, num, radix);
	*res = TEST_FAILED;
	return TEST_FAILED;
}

int	test_len_uint() {
	int return_value = TEST_SUCCESS;

	test_len_uint_template(0, 0, 1, &return_value);
	test_len_uint_template(2, 2, 2, &return_value);
	test_len_uint_template(42, 10, 2, &return_value);
	test_len_uint_template(15, 16, 1, &return_value);

	test_len_uint_template(2147483647, 16, 8, &return_value);
	test_len_uint_template(2147483647, 10, 10, &return_value);
	test_len_uint_template(9223372036854775807LL, 16, 16, &return_value);
	test_len_uint_template(9223372036854775807LL, 10, 19, &return_value);

	test_len_uint_template(42, 0, -1, &return_value);

	return return_value;
}

static int test_len_int_template(ssize_t num, uint radix, ssize_t want, int *res) {
	ssize_t got = ft_len_int(num, radix);

	if (got == want)
		return TEST_SUCCESS;
	logf("got %zd expected %zd for (%zd, %u)\n", got, want, num, radix);
	*res = TEST_FAILED;
	return TEST_FAILED;
}

int	test_len_int() {
	int return_value = TEST_SUCCESS;

	test_len_int_template(0, 0, 1, &return_value);
	test_len_int_template(2, 2, 2, &return_value);
	test_len_int_template(42, 10, 2, &return_value);
	test_len_int_template(15, 16, 1, &return_value);

	test_len_int_template(2147483647, 16, 8, &return_value);
	test_len_int_template(-2147483647, 16, 8, &return_value);
	test_len_int_template(2147483647, 10, 10, &return_value);
	test_len_int_template(-2147483648, 10, 10, &return_value);
	test_len_int_template(9223372036854775807LL, 16, 16, &return_value);
	test_len_int_template(-9223372036854775807LL, 16, 16, &return_value);
	test_len_int_template(9223372036854775807LL, 10, 19, &return_value);
	test_len_int_template(-9223372036854775807LL, 10, 19, &return_value);

	test_len_int_template(42, 0, -1, &return_value);

	return return_value;
}

t_test num_suite[] = {
	TEST(test_get_hex_letter),
	TEST(test_lendigit),
	TEST(test_len_uint),
	TEST(test_len_int),
	NULL_TEST,
};
