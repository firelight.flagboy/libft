/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/21 20:06:54 by fbenneto          #+#    #+#             */
/*   Updated: 2019/11/25 12:43:36 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_GETOPT_UTILS_H
# define LIBFT_GETOPT_UTILS_H

# include "libft/getopt/struct.h"
# include <sys/types.h>

void			ft_getopt_exchange(char **argv, t_getopt_data *d);
const t_option	*find_long_option(t_getopt_data *d, size_t option_len);
void			ft_getopt_permute(t_getopt_data *d);

#endif
