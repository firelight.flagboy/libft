/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   global.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/20 10:25:59 by fbenneto          #+#    #+#             */
/*   Updated: 2019/11/20 10:26:23 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_GETOPT_GLOBAL_H
# define LIBFT_GETOPT_GLOBAL_H

extern int g_opterr;
extern int g_optopt;
extern int g_optind;
extern char *g_optarg;

#endif
