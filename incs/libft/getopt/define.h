/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   define.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <f.benneto@student.42.fr>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/16 17:27:49 by fbenneto          #+#    #+#             */
/*   Updated: 2019/11/16 17:29:34 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_GETOPT_DEFINE_H
# define LIBFT_GETOPT_DEFINE_H

# define ERR_MISS_ARG "%s: option requires an argument -- '%c'\n"
# define ERR_INV_ARG "%s: invalid option -- '%c'\n"

#endif
