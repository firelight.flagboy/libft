/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/20 13:01:06 by fbenneto          #+#    #+#             */
/*   Updated: 2019/11/25 13:23:08 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_GETOPT_INIT_H
# define LIBFT_GETOPT_INIT_H

# include "libft/getopt/struct.h"

const char	*ft_getopt_initialize(const char *optstring, t_getopt_data *data);

#endif
