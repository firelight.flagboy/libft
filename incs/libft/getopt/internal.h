/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   internal.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/20 11:17:20 by fbenneto          #+#    #+#             */
/*   Updated: 2019/11/25 13:27:31 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_GETOPT_INTERNAL_H
# define LIBFT_GETOPT_INTERNAL_H

# include "libft/getopt/struct.h"

# define NEXT_ARG_OK 0x1000000
# define GETOPT_ERR_MISS_ARG "%s: option requires an argument -- '%c'\n"

int	ft_getopt_internal(t_arg *arg, t_getopt_option *option);
int	ft_getopt_internal_r(t_getopt_data *d);
int	process_long_option(t_getopt_data *d, const char *prefix);
int next_short_option(const char *optstring, t_getopt_data *d);

#endif
