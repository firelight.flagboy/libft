/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   struct.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/20 10:26:47 by fbenneto          #+#    #+#             */
/*   Updated: 2019/11/25 13:27:45 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_GETOPT_STRUCT_H
# define LIBFT_GETOPT_STRUCT_H

typedef struct s_option	t_option;
typedef struct s_arg	t_arg;
typedef struct s_getopt_data	t_getopt_data;
typedef enum e_ord	t_ord;
typedef struct s_getopt_option	t_getopt_option;

enum				e_option_flag
{
	no_argument,
	required_argument,
	optional_argument,
};

enum				e_ord
{
	REQUIRE_ORDER, PERMUTE, RETURN_IN_ORDER
};

struct				s_option
{
	const char		*name;
	int				has_arg;
	int				*flag;
	int				val;
};

struct				s_arg
{
	int				argc;
	char *const		*argv;
};

struct				s_getopt_data
{
	int				argc;
	char *const		*argv;
	int				*longind;
	int				long_only;
	const char		*optstring;
	const t_option	*longopts;
	int				optind;
	int				opterr;
	int				optopt;
	char			*optarg;
	int				is_initialized;
	char			*next;
	t_ord			ordering;
	int				first_nonopt;
	int				last_nonopt;
	int				print_error;
	int				err_opt;
	const char		*modified_optstring;
};

struct				s_getopt_option
{
	const char		*optstring;
	const t_option	*longopts;
	int				*longind;
	int				long_only;
};

#endif
