/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   list.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/27 10:56:05 by fbenneto          #+#    #+#             */
/*   Updated: 2019/06/11 15:23:55 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIST_H
# define LIST_H

# include <unistd.h>
# include "libft/mem.h"

typedef struct	s_list
{
	void			*content;
	size_t			content_size;
	struct s_list	*next;
}				t_list;

/*
** Create
*/
t_list			*ft_lstnew(void *content, size_t content_size);
t_list			*ft_lstnew_dup(void const *content, size_t content_size);

/*
** Delete
*/
void			ft_lstdelone(t_list **alst, void (*del)(void *, size_t));
void			ft_lstdel(t_list **alst, void (*del)(void *, size_t));
void			ft_lstdel_for_cleanup(t_list **ptr_to_alst);

/*
** Append
*/
# define DEPRECATED_LSTADD "use ft_lstpush_(front/back)"

void			ft_lstadd(
	t_list **alst, t_list *new) __attribute__((deprecated(DEPRECATED_LSTADD)));
void			ft_lstpush_front(t_list **head_ref, t_list *elem);
void			ft_lstpush_back(t_list **head_ref, t_list *elem);

/*
** Iter
*/
void			ft_lstiter(t_list *lst, void (*f)(t_list *elem));
t_list			*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem));

/*
** Utils
*/
size_t			ft_lstlen(t_list *lst);
t_list			*ft_lstlast(t_list *head);
void			ft_lstfree_content(void *content, size_t content_size);

/*
** Sort
*/
void			ft_lstsort_merge(
	t_list **head_ref, int (*cmp)(t_list *a, t_list *b));

#endif
