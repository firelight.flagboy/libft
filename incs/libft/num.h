/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   num.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/06 10:41:41 by fbenneto          #+#    #+#             */
/*   Updated: 2019/10/24 11:25:11 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef NUM_H
# define NUM_H

# include <stdint.h>
# include <sys/types.h>

# define BIN_CHARSET "01"
# define OCT_CHARSET "01234567"
# define DEC_CHARSET "0123456789"
# define HEX_CHARSET "0123456789abcdef"
# define HEX_CHARSET_U "0123456789ABCDEF"

size_t	ft_lendigit(const char *str);
ssize_t	ft_len_int(ssize_t num, uint radix) __attribute__((const));
ssize_t	ft_len_uint(size_t num, uint radix) __attribute__((const));
char	ft_get_hex_letter(int val) __attribute__((const));

#endif
