/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   type.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/17 08:49:36 by fbenneto          #+#    #+#             */
/*   Updated: 2019/10/24 10:23:18 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef TYPE_H
# define TYPE_H

int		ft_isalpha(int c) __attribute__((const));
int		ft_isdigit(int c) __attribute__((const));
int		ft_isalnum(int c) __attribute__((const));
int		ft_isascii(int c) __attribute__((const));
int		ft_isprint(int c) __attribute__((const));
int		ft_toupper(int c) __attribute__((const));
int		ft_tolower(int c) __attribute__((const));
int		ft_isspace(int c) __attribute__((const));
int		ft_islower(int c) __attribute__((const));
int		ft_isupper(int c) __attribute__((const));
int		ft_isxdigit(int c) __attribute__((const));

#endif
