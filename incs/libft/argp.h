/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   argp.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/17 12:42:12 by fbenneto          #+#    #+#             */
/*   Updated: 2019/11/29 15:06:33 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_ARGP_H
# define LIBFT_ARGP_H

# include "libft/getopt.h"

# include "libft/argp/struct.h"
# include "libft/argp/define.h"
# include "libft/argp/global.h"

void	ft_argp_error(const t_argp_state *state, const char *fmt, ...);
void	ft_argp_failure(
	const t_argp_state *state, int status, int errnum, const char *fmt, ...);
void	ft_argp_state_help(
	const t_argp_state *state, FILE *stream, unsigned flags);
void	ft_argp_help(
	const t_argp *argp, FILE *stream, unsigned flags, char *name);
void	ft_argp_usage(const t_argp_state *state);

int		ft_argp_parse(
	const t_argp *argp, t_arg *arg, unsigned flags, t_argp_data *data);

#endif
