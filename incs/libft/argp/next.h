/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   next.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/27 10:34:28 by fbenneto          #+#    #+#             */
/*   Updated: 2019/11/28 13:25:35 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_ARGP_NEXT_H
# define LIBFT_ARGP_NEXT_H

# include "libft/argp/internal/struct.h"

int		ft_argp_parse_next(t_parser *parser, int *arg_badkey);
int		ft_argp_parser_parse_arg(t_parser *parser, const char *arg);
int		ft_argp_parser_parse_opt(t_parser *parser, int opt, const char *arg);

#endif
