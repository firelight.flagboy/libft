/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   struct.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/17 16:32:47 by fbenneto          #+#    #+#             */
/*   Updated: 2019/11/29 16:02:34 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_ARGP_STRUCT_H
# define LIBFT_ARGP_STRUCT_H

# include <stdio.h>

typedef struct s_argp_option	t_argp_option;
typedef struct s_argp_state		t_argp_state;
typedef struct s_argp			t_argp;
typedef struct s_argp_child		t_argp_child;
typedef struct s_argp_data		t_argp_data;

typedef int		(*t_argp_parser)(int key, const char *arg, t_argp_state *state);
typedef void	(*t_program_version_hook)(FILE *stream, t_argp_state *state);

struct	s_argp_option
{
	const char	*name;
	int			key;
	const char	*metavar;
	unsigned	flags;
	const char	*doc;
	int			group;
};

struct	s_argp_state
{
	const t_argp	*root_argp;
	int				argc;
	char *const		*argv;
	int				next;
	unsigned		flags;
	unsigned		arg_num;
	int				quoted;
	void			*input;
	void			**child_inputs;
	void			*hook;
	const char		*name;
	FILE			*err_stream;
	FILE			*out_stream;
	void			*pstate;
};

struct	s_argp
{
	const t_argp_option	*options;
	t_argp_parser		parser;
	const char			*args_doc;
	const char			*doc;
	t_argp_child		*children;
	char				*(*help_filter)(int key, const char *text, void *input);
};

struct	s_argp_child
{
	const t_argp	*argp;
	unsigned		flags;
	const char		*header;
	int				group;
};

struct	s_argp_data
{
	int		*arg_index;
	void	*input;
};

#endif
