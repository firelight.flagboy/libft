/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   global.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/18 16:00:38 by fbenneto          #+#    #+#             */
/*   Updated: 2019/11/29 15:58:45 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_ARGP_GLOBAL_H
# define LIBFT_ARGP_GLOBAL_H

# include <stdio.h>
# include "libft/argp/struct.h"

extern int						g_argp_err_exit_status;
extern const char				*g_argp_program_version;
extern const char				*g_argp_program_bug_address;
extern t_program_version_hook	g_argp_program_version_hook;
extern const char				*g_argp_program_invocation_name;

#endif
