/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/27 10:31:23 by fbenneto          #+#    #+#             */
/*   Updated: 2020/03/10 09:31:12 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_ARGP_INIT_H
# define LIBFT_ARGP_INIT_H

# include "libft/getopt.h"
# include "libft/argp/struct.h"
# include "libft/argp/internal/struct.h"

int		ft_argp_parser_init(
	t_parser *parser, const t_argp *argp, t_arg *arg, t_parser_data *data);
void	*parser_init_struct(
	const t_argp *argp, t_parser_data *data, t_parser *parser);

#endif
