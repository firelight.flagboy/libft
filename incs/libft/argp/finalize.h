/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   finalize.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/27 10:36:10 by fbenneto          #+#    #+#             */
/*   Updated: 2019/11/27 10:37:37 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_ARGP_FINALIZE_H
# define LIBFT_ARGP_FINALIZE_H

# include "libft/argp/internal/struct.h"

int		ft_argp_parser_finalize(
	t_parser *parser, int err, int arg_badkey, int *arg_index);

#endif
