/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   help_format.h                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/04 10:41:23 by fbenneto          #+#    #+#             */
/*   Updated: 2020/03/10 09:32:12 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_ARGP_INTERNAL_HELP_FORMAT_H
# define LIBFT_ARGP_INTERNAL_HELP_FORMAT_H

# include <sys/types.h>
# include <stdio.h>

# define DUP_METAVAR 0
# define DUP_METAVAR_NOTE 1
# define SHORT_OPT_COL 2
# define LONG_OPT_COL 6
# define DOC_OPT_COL 2
# define OPT_DOC_COL 29
# define GROUP_HEADER 1
# define RIGHT_MARGIN 79
# define USAGE_OPT_COL 11

# define FMT_BUFF_SIZE 4096

typedef struct s_params_fmt	t_params_fmt;
typedef struct s_fmt		t_fmt;

struct						s_params_fmt
{
	unsigned	dup_metavar;
	unsigned	dup_metavar_note;
	unsigned	short_opt_col;
	unsigned	long_opt_col;
	unsigned	doc_opt_col;
	unsigned	opt_doc_col;
	unsigned	group_header;
};

struct						s_fmt
{
	size_t		cursor;
	unsigned	left_margin;
	unsigned	right_margin;
	char		*end;
	char		*p;
	char		*buffer;
	FILE		*stream;
};

int							fmt_ensure(t_fmt *fmt, size_t size);
void						fmt_flush(t_fmt *fmt);
char						*fmt_init(t_fmt *fmt, unsigned right_marging,
	FILE *stream);
void						fmt_clean(t_fmt *fmt);

int							fmt_putc(t_fmt *fmt, char c);
int							fmt_puts(t_fmt *fmt, const char *s);
int							fmt_nputs(t_fmt *fmt, const char *s, size_t n);
int							fmt_printf(t_fmt *fmt, const char *format, ...
) __attribute__((format(printf,2,3)));
int							fmt_write(t_fmt *fmt, const void *s, size_t len);

unsigned					fmt_set_left_margin(t_fmt *fmt,
	unsigned left_margin);
void						fmt_new_line(t_fmt *fmt);
int							fmt_check_margin(t_fmt *fmt);

int							fmt_putsnl(t_fmt *fmt, const char *s);
int							fmt_nputsnl(t_fmt *fmt, const char *s, size_t n);
int							fmt_printfnl(t_fmt *fmt, const char *format, ...
) __attribute__((format(printf,2,3)));

#endif
