/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   help_long_utils.h                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/10 09:04:08 by fbenneto          #+#    #+#             */
/*   Updated: 2020/03/10 10:03:13 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef HELP_LONG_UTILS_H
# define HELP_LONG_UTILS_H

# include "libft/argp/help.h"

int	print_docs(t_fmt *fmt, t_help_doc *docs);
int	print_header(t_fmt *fmt, const char *header);
int	print_metavar(t_fmt *fmt, const char *metavar,
	unsigned flags, int is_short);
int	next_alias_is_print(const t_help_option_alias *a, int is_short);
int	print_alias_short_option(t_fmt *fmt,
	const t_help_option_alias *alias, int print_long_opt,
	const t_help_option *option);
int	print_alias_long_option(t_fmt *fmt,
	const t_help_option_alias *alias,
	const t_help_option *option, int long_only);

#endif
