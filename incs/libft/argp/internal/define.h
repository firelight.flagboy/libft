/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   define.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/29 08:59:43 by fbenneto          #+#    #+#             */
/*   Updated: 2019/11/29 09:00:26 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_ARGP_INTERNAL_DEFINE_H
# define LIBFT_ARGP_INTERNAL_DEFINE_H

# include "libft/argp/define.h"

# define EBADKEY FT_ARGP_ERR_UNKNOWN

#endif
