/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   usage_util.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/10 09:16:52 by fbenneto          #+#    #+#             */
/*   Updated: 2020/03/10 10:11:17 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef USAGE_UTIL_H
# define USAGE_UTIL_H

# include "libft/argp/help.h"
# include "libft/argp/define.h"
# include "libft/argp/struct.h"

typedef struct	s_help_usage_internal
{
	int			first_pattern;
	int			long_usage;
	size_t		level;
	const char	*name;
	size_t		current_level;
}				t_help_usage_internal;

size_t			usage_nb_level(const t_argp *argp);
char			*get_next_args_doc(const t_argp *argp, char **saved_arg_doc,
	size_t *curr_level, size_t level);
int				should_print_long_usage(const t_help_option_alias *alias);
int				group_have_option(const t_help_option_group *group);

int				print_long_usage_no_arg(t_fmt *fmt,
	const t_help_option_alias *alias, char *buff, int *idx);
int				print_options_long_usage_no_arg(t_help_option *options,
	t_fmt *fmt, char *buff, int *arr);
void			print_long_usage_no_arg_loop(t_fmt *fmt, t_help_state *state);
void			print_long_usage_short(t_fmt *fmt,
	const t_help_option_alias *alias, unsigned flags, const char *metavar);
void			print_long_usage_short_opt_loop(t_fmt *fmt,
	t_help_state *state);

void			print_start_usage(t_fmt *fmt, const char *name,
	int first_pattern);
char			*print_doc(const char *s, size_t *curr_level, size_t level,
	char **saved_arg_doc);

#endif
