/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   help_internal.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <f.benneto@student.42.fr>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/02 14:13:50 by fbenneto          #+#    #+#             */
/*   Updated: 2019/12/05 08:32:13 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_ARGP_HELP_INTERNAL_H
# define LIBFT_ARGP_HELP_INTERNAL_H

# include "libft/argp/internal/help_format.h"
# include "libft/argp/struct.h"

typedef struct s_help_state			t_help_state;
typedef struct s_help_option_group	t_help_option_group;
typedef struct s_help_option		t_help_option;
typedef struct s_help_option_alias	t_help_option_alias;
typedef struct s_help_doc			t_help_doc;
typedef struct s_help_data			t_help_data;

struct								s_help_data
{
	unsigned	flags;
	const char	*name;
};

struct								s_help_doc
{
	const char	*name;
	const char	*content;
	t_help_doc	*next;
};

struct								s_help_option_alias
{
	int					key;
	const char			*name;
	unsigned			flags;
	t_help_option_alias	*next;
};

struct								s_help_option
{
	t_help_option_alias	*alias;
	const char			*metavar;
	const char			*doc;
	unsigned			flags;
	t_help_option		*next;
};

struct								s_help_option_group
{
	const char			*header;
	int					id;

	t_help_option		*options;
	t_help_doc			*docs;

	t_help_option_group	*parent;
	t_help_option_group	*next;
};

struct								s_help_state
{
	t_help_option_group	*group_head;
	t_help_doc			*docs;
	const t_argp		*argp;
	const t_argp_state	*state;
	unsigned			as_print;
	t_fmt				fmt;
	const char			*name;
	unsigned			flags;
	void				*hook;
};

void								insert_argp_opt(const t_argp_option *opt,
	t_help_state *state, int *current_group);

void								insert_help_doc(t_help_doc **head,
	t_help_doc *to_insert);
t_help_doc							*new_help_doc(const t_argp_option *doc);

t_help_option						*get_last_options(t_help_option *list);
t_help_option						*new_option_group(
	t_help_option_group *group, const t_argp_option *option);
t_help_option						*insert_opt_in_group(
	t_help_option_group *group, const t_argp_option *option);

void								insert_help_option(t_help_option **head,
	t_help_option *to_insert);

t_help_option_alias					*new_option_alias(const t_argp_option *opt);
void								insert_help_option_alias(
	t_help_option_alias **head, t_help_option_alias *to_insert);

t_help_option_group					*new_help_group(t_help_state *state,
	int group);
void								insert_new_group(t_help_option_group **head,
	t_help_option_group *to_insert);
t_help_option_group					*find_help_group(t_help_option_group *list,
	int id);

#endif
