/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   struct.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/27 10:09:54 by fbenneto          #+#    #+#             */
/*   Updated: 2019/11/29 15:54:37 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_ARGP_INTERNAL_STRUCT_H
# define LIBFT_ARGP_INTERNAL_STRUCT_H

# include "libft/getopt.h"
# include "libft/argp/struct.h"

typedef struct s_group					t_group;
typedef struct s_parser					t_parser;
typedef struct s_parser_sizes			t_parser_sizes;
typedef struct s_parser_data			t_parser_data;
typedef struct s_parser_convert_state	t_parser_convert_state;

struct	s_group
{
	t_argp_parser	parser;
	const t_argp	*argp;
	char			*short_end;
	unsigned		args_processed;
	t_group			*parent;
	unsigned		parent_index;
	void			*input;
	void			**child_inputs;
	void			*hook;
};

struct	s_parser
{
	const t_argp	*argp;
	char			*short_opts;
	const t_option	*long_opts;
	t_getopt_data	opt_data;
	t_group			*groups;
	t_group			*end_group;
	void			**child_inputs;
	int				try_getopt;
	t_argp_state	state;
	void			*storage;
};

struct	s_parser_convert_state
{
	t_parser	*parser;
	char		*short_end;
	t_option	*long_end;
	void		**child_inputs_end;
	unsigned	parent_index;
};

struct	s_parser_sizes
{
	size_t	short_opt_len;
	size_t	long_opt_len;
	size_t	num_groups;
	size_t	num_child_inputs;
};

struct	s_parser_data
{
	unsigned	flags;
	int			*arg_index;
	void		*input;
};

#endif
