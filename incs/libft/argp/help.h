/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   help.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/18 15:40:08 by fbenneto          #+#    #+#             */
/*   Updated: 2019/12/04 15:06:24 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_ARGP_HELP_H
# define LIBFT_ARGP_HELP_H

# include "libft/argp/struct.h"
# include "libft/argp/internal/help_internal.h"
# include "libft/argp/internal/help_format.h"

int		ft_argp_help_usage(t_fmt *fmt, t_help_state *state, const char *name,
	int long_usage);
int		ft_argp_help_see(t_fmt *fmt, const char *name);
int		ft_argp_help_doc(t_fmt *fmt, const t_argp *argp, t_help_state *state,
	int *values);
int		ft_argp_help_long(t_fmt *fmt, t_help_state *state, int long_only);
int		ft_argp_help_bug_addr(t_fmt *fmt, t_help_state *state);
void	ft_argp_help_internal(const t_argp *argp, const t_argp_state *state,
	FILE *stream, t_help_data *data);

#endif
