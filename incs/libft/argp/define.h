/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   define.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/18 08:38:03 by fbenneto          #+#    #+#             */
/*   Updated: 2019/11/29 15:06:09 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_ARGP_DEFINE_H
# define LIBFT_ARGP_DEFINE_H

enum	e_argp_flags
{
	FT_ARGP_PARSE_ARGV0 = 0x1,
	FT_ARGP_NO_ERRS = 0x2,
	FT_ARGP_NO_ARGS = 0x4,
	FT_ARGP_IN_ORDER = 0x8,
	FT_ARGP_NO_HELP = 0x10,
	FT_ARGP_NO_EXIT = 0x20,
	FT_ARGP_LONG_ONLY = 0x40,
	FT_ARGP_SILENT = (FT_ARGP_NO_ERRS | FT_ARGP_NO_EXIT | FT_ARGP_NO_HELP),
};

/*
** For option
*/
enum	e_argp_option
{
	FT_ARGP_OPTION_ARG_OPTIONAL = 0x1,
	FT_ARGP_OPTION_HIDDEN = 0x2,
	FT_ARGP_OPTION_ALIAS = 0x4,
	FT_ARGP_OPTION_DOC = 0x8,
	FT_ARGP_OPTION_NO_USAGE = 0x10,
	FT_ARGP_KEY_HELP_PRE_DOC = 0x2000001,
	FT_ARGP_KEY_HELP_POST_DOC = 0x2000002,
	FT_ARGP_KEY_HELP_HEADER = 0x2000003,
	FT_ARGP_KEY_HELP_EXTRA = 0x2000004,
	FT_ARGP_KEY_HELP_DUP_ARGS_NOTE = 0x2000005,
	FT_ARGP_KEY_HELP_ARGS_DOC = 0x2000006,
};

/*
** The Following flags are for `ft_argp_help` and `ft_argp_state_help`
*/
enum	e_argp_help_flags
{
	FT_ARGP_HELP_USAGE = 0x1,
	FT_ARGP_HELP_SHORT_USAGE = 0x2,
	FT_ARGP_HELP_SEE = 0x4,
	FT_ARGP_HELP_LONG = 0x8,
	FT_ARGP_HELP_PRE_DOC = 0x10,
	FT_ARGP_HELP_POST_DOC = 0x20,
	FT_ARGP_HELP_DOC = (FT_ARGP_HELP_PRE_DOC | FT_ARGP_HELP_POST_DOC),
	FT_ARGP_HELP_BUG_ADDR = 0x40,
	FT_ARGP_HELP_LONG_ONLY = 0x80,
};

/*
** The Following flags are only understood by `ft_argp_state_help`
*/
enum	e_argp_state_help
{
	FT_ARGP_HELP_EXIT_ERR = 0x100,
	FT_ARGP_HELP_EXIT_OK = 0x200,
	FT_ARGP_HELP_STD_ERR = (FT_ARGP_HELP_SEE | FT_ARGP_HELP_EXIT_ERR),
	FT_ARGP_HELP_STD_USAGE = (FT_ARGP_HELP_SHORT_USAGE | FT_ARGP_HELP_SEE
		| FT_ARGP_HELP_EXIT_ERR),
	FT_ARGP_HELP_STD_HELP = (FT_ARGP_HELP_SHORT_USAGE | FT_ARGP_HELP_LONG
		| FT_ARGP_HELP_DOC | FT_ARGP_HELP_EXIT_OK | FT_ARGP_HELP_BUG_ADDR),
};

enum	e_argp_special_key
{
	FT_ARGP_KEY_ARG = 0,
	FT_ARGP_KEY_END = 0x1000001,
	FT_ARGP_KEY_NO_ARGS = 0x1000002,
	FT_ARGP_KEY_INIT = 0x1000003,
	FT_ARGP_KEY_SUCCESS = 0x1000004,
	FT_ARGP_KEY_ERROR = 0x1000005,
	FT_ARGP_KEY_ARGS = 0x1000006,
	FT_ARGP_KEY_FINI = 0x1000007,
};

# define FT_ARGP_ERR_UNKNOWN 0x42424242
# define FT_ARGP_MISS_INVOCATION_NAME "(missing name)"

#endif
