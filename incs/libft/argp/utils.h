/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/17 16:28:56 by fbenneto          #+#    #+#             */
/*   Updated: 2019/12/02 13:29:13 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_ARGP_UTILS_H
# define LIBFT_ARGP_UTILS_H

# include "libft/argp/struct.h"
# include "libft/argp/internal/struct.h"

int		option_is_end(const t_argp_option *opt);
int		option_is_short(const t_argp_option *opt);
int		option_is_group_header(const t_argp_option *opt);

void	ft_argp_calc_sizes(const t_argp *argp, t_parser_sizes *sizes);
int		argp_find_long_option(const t_option *options, const char *name);
t_group	*convert_options(
	const t_argp *argp, t_group *parent, t_group *group,
	t_parser_convert_state *state);
int		ft_argp_group_parse(
	t_group *group, t_argp_state *state, int key, const char *arg);

#endif
