/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   global.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/17 12:15:29 by fbenneto          #+#    #+#             */
/*   Updated: 2019/11/26 10:19:52 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_PRINTF_GLOBAL_H
# define LIBFT_PRINTF_GLOBAL_H

# include <stdarg.h>
# include "libft/printf/struct.h"
# include "libft/printf/conv.h"

# undef PRINTF_SIZE_HASH
# define PRINTF_SIZE_HASH 57

# define DEFAULT ft_putgen_type

int	(*const g_conv[PRINTF_SIZE_HASH])(va_list*, t_flags*) =
{
	ft_call_fillhex,
	DEFAULT,
	DEFAULT,
	ft_call_fillunbr,
	DEFAULT,
	ft_call_fillstr,
	DEFAULT,
	DEFAULT,
	ft_call_filladr,
	ft_call_filloctal,
	ft_get_index,
	DEFAULT,
	DEFAULT,
	DEFAULT,
	DEFAULT,
	ft_call_fillnbr,
	DEFAULT,
	DEFAULT,
	DEFAULT,
	DEFAULT,
	ft_call_fillnbr,
	ft_call_fillchar,
	ft_call_fillbin,
	DEFAULT,
	DEFAULT,
	DEFAULT,
	DEFAULT,
	DEFAULT,
	DEFAULT,
	DEFAULT,
	DEFAULT,
	DEFAULT,
	ft_call_fillhex,
	DEFAULT,
	DEFAULT,
	ft_call_fillunbr,
	DEFAULT,
	ft_call_fill_longstr,
	DEFAULT,
	DEFAULT,
	DEFAULT,
	ft_call_filloctal,
	DEFAULT,
	DEFAULT,
	DEFAULT,
	DEFAULT,
	DEFAULT,
	DEFAULT,
	DEFAULT,
	DEFAULT,
	DEFAULT,
	DEFAULT,
	ft_call_fillnbr,
	ft_call_fill_longchar,
	ft_call_fillbin,
	DEFAULT,
	DEFAULT,
};

#endif
