/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/17 13:41:18 by fbenneto          #+#    #+#             */
/*   Updated: 2019/11/25 13:24:28 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_PRINTF_UTILS_H
# define LIBFT_PRINTF_UTILS_H

# include <sys/types.h>
# include <wchar.h>
# include <stdint.h>
# include "libft/printf/struct.h"

/*
** Putbuffer
*/
void		ft_putbuffer(t_buff *buff);
void		ft_putbuffer_s(t_buff *buff);
void		ft_putbuffer_sn(t_buff *buff);
void		ft_putbuffer_as(t_buff *buff);

/*
** Buffer fc
*/
int			ft_fill_buffer(char const *s, va_list ap);
int			ft_fill_buffer_color(char const *s, va_list ap);
int			ft_add_char_to_buff(int c);
int			ft_add_nchar_to_buff(int c, size_t n);
int			ft_add_str_to_buff(char const *s);
int			ft_add_nstr_to_buff(char const *s, size_t n);
int			ft_fill_ox(char type);

/*
** Type
*/
int			printf_islen_flags(int c);
int			printf_isattr_flags(int c);

/*
** Getter
*/
t_buff		*get_buff(void);
t_flags		*ft_get_flags(char **astr, va_list *ap, t_flags *f);
t_flags		*ft_get_att(char **astr, t_flags *f);
t_flags		*ft_get_len_flags(char **astr, t_flags *f);
int			ft_get_buff_size(char **astr, t_flags *f, va_list *ap);
int			ft_get_precision(char **astr, t_flags *f, va_list *ap);
intmax_t	ft_get_int(va_list *ap, t_flags *f);
uintmax_t	ft_get_uint(va_list *ap, t_flags *f);

/*
** T_type
*/
void		ft_tab_charset(void *data_type);
void		ft_tab_fc(void *data_type);
char		*ft_call_fc_g(char const *s, va_list *ap);

/*
** Filler
*/
int			ft_fillforward(t_flags *f, char isneg, int len);
int			ft_fillforward_hex(t_flags *f, int len);
int			ft_fillforward_oct(t_flags *f, int len);
int			ft_fillforward_uin(t_flags *f, int len);
int			ft_fillbackward(t_flags *f, char isneg, int len);
int			ft_filldimen(t_flags *f, char isneg, int len);
int			ft_filldimen_hex(t_flags *f, int len);
int			ft_filldimen_oct(t_flags *f, int len);
int			ft_filldimen_uin(t_flags *f, int len);
int			ft_fill_char_sign(t_flags *f, char isneg);
int			ft_get_len_forward(t_flags *f, int flen, char neg);

/*
** Lib string
*/
char		*printf_string_cpy(char *dest, char const *s, size_t n, size_t l);
int			printf_itoa(uintmax_t n);
int			printf_itoa_base(uintmax_t n, char *base);

/*
** Unicode
*/
int			printf_len_uni_char(wchar_t c);
int			printf_len_uni_str(wchar_t *s, t_flags *f);
size_t		ft_len_longstr(wchar_t const *s);
int			ft_unicode(wchar_t c);

#endif
