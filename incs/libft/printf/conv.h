/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   conv.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/17 13:05:16 by fbenneto          #+#    #+#             */
/*   Updated: 2019/11/15 08:46:30 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_PRINTF_CONV_H
# define LIBFT_PRINTF_CONV_H

# include <stdarg.h>
# include "libft/printf/struct.h"

/*
** Decimal conv
*/
int		ft_call_fillnbr(va_list *ap, t_flags *f);

/*
** Unsigned conv
*/
int		ft_call_fillunbr(va_list *ap, t_flags *f);

/*
** Hexadecimal conv
*/
int		ft_call_fillhex(va_list *ap, t_flags *f);
int		ft_call_filladr(va_list *ap, t_flags *f);

/*
** Octal conv
*/
int		ft_call_filloctal(va_list *ap, t_flags *f);

/*
** Binary conv
*/
int		ft_call_fillbin(va_list *ap, t_flags *f);

/*
** Char conv
*/
int		ft_call_fillchar(va_list *ap, t_flags *f);
int		ft_call_fill_longchar(va_list *ap, t_flags *f);

/*
** String conv
*/
int		ft_call_fillstr(va_list *ap, t_flags *f);
int		ft_call_fill_longstr(va_list *ap, t_flags *f);

/*
** %n
*/
int		ft_get_index(va_list *ap, t_flags *f);

/*
** Float
*/
int		ft_call_fillfloat(va_list *ap, t_flags *f);

/*
** Other
*/
int		ft_putgen_type(va_list *ap, t_flags *f);

#endif
