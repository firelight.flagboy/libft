/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   struct.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/17 13:06:58 by fbenneto          #+#    #+#             */
/*   Updated: 2019/11/25 13:26:22 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_PRINTF_STRUCT_H
# define LIBFT_PRINTF_STRUCT_H

# include <sys/types.h>
# include <stdarg.h>
# include "libft/printf/define.h"

typedef struct	s_ite
{
	size_t		i;
	size_t		nbyte;
	size_t		len;
	size_t		max_len;
	int			res;
	int			index;
}				t_ite;

typedef struct	s_flags
{
	unsigned	flags:10;
	char		type;
	char		len_flags[4];
	int			precision;
	int			buff_size;
}				t_flags;

typedef struct	s_type
{
	char		charset;
	int			(*f)(va_list*, t_flags);
}				t_type;

typedef struct	s_buff
{
	char		buffer[BUFF_SIZE_PRINTF];
	size_t		index;
	size_t		res;
	int			fd;
	size_t		max;
	char		*s;
	void		(*put)(struct s_buff *buff);
}				t_buff;

#endif
