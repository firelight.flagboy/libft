/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   printf.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/17 12:02:50 by fbenneto          #+#    #+#             */
/*   Updated: 2019/11/29 15:19:40 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_PRINTF_H
# define LIBFT_PRINTF_H

# include <stdarg.h>
# include <stdio.h>

/*
** Printf display
*/
int	ft_printf(const char *format, ...) __attribute__((format(printf,1,2)));
int	ft_eprintf(const char *format, ...) __attribute__((format(printf,1,2)));
int	ft_dprintf(int fd, const char *f, ...) __attribute__((format(printf,2,3)));
int	ft_fprintf(FILE *s, const char *f, ...) __attribute__((format(printf,2,3)));
/*
** Printf string
*/
int	ft_sprintf(char *s, const char *format, ...
) __attribute__((format(printf,2,3)));
int	ft_snprintf(char *s, size_t n, const char *f, ...
) __attribute__((format(printf,3,4)));
int	ft_asprintf(char **as, const char *f, ...
) __attribute__((format(printf,2,3)));
/*
** vprintf display
*/
int	ft_vfprintf(FILE *stream, const char *format, va_list ap);
int	ft_vdprintf(int fd, const char *format, va_list ap);
/*
** vprintf string
*/
int	ft_vsprintf(char *s, const char *format, va_list ap);
int	ft_vsnprintf(char *s, size_t n, const char *format, va_list ap);
int	ft_vasprintf(char **as, const char *format, va_list ap);

#endif
