/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   getopt.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/16 16:47:52 by fbenneto          #+#    #+#             */
/*   Updated: 2019/11/27 13:03:03 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_GETOPT_H
# define LIBFT_GETOPT_H

# include "libft/getopt/global.h"
# include "libft/getopt/struct.h"

int				ft_getopt(int argc, char *const *argv, const char *options);
int				ft_getopt_long(t_arg *arg, const char *shortopts,
	const t_option *longopts, int *indexptr);
int				ft_getopt_long_only(t_arg *arg, const char *shortopts,
	const t_option *longopts, int *indexptr);

int				ft_getopt_long_r(t_getopt_data *data);
int				ft_getopt_long_only_r(t_getopt_data *data);

#endif
