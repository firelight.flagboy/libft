/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/08 11:41:38 by fbenneto          #+#    #+#             */
/*   Updated: 2019/11/15 08:45:01 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_H
# define LIBFT_H

# include "libft/conv.h"
# include "libft/type.h"
# include "libft/input.h"
# include "libft/list.h"
# include "libft/mem.h"
# include "libft/output.h"
# include "libft/string.h"
# include "libft/printf.h"

#endif
